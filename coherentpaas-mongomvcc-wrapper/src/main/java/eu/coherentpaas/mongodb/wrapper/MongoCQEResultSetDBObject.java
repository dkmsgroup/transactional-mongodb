package eu.coherentpaas.mongodb.wrapper;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.wrapper.exceptions.ResultSetClosedException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author idezol
 */
public class MongoCQEResultSetDBObject extends MongoCQEResultSet {
    private final DBObject dbObject;

    public MongoCQEResultSetDBObject(DBObject bObject, boolean includeIDInResult, Type [] signature, String [] columnNames) {
        super(null, includeIDInResult, signature, columnNames);
        this.dbObject = bObject;
    }
    
    @Override
    public int getRowCount() throws CQEException {
        if(dbObject!=null)
            return 1;
        else
            return 0;
    }
    
    @Override
    public Object[][] next() throws CQEException {
        synchronized(this) {
            if(!this.open.get()) {
                ResultSetClosedException ex = new ResultSetClosedException("Cannot retrieve more data. ResultSet is closed");
                throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
            }
        }
        
        List<Object[]> rows = new ArrayList<>();
        if(this.dbObject!=null) 
            rows.add(transformRow((BasicDBObject) dbObject));
        
        return rows.toArray(new Object[rows.size()][]);
    }
    
    @Override
    public Object[][] next(int max) throws CQEException {
        return next();
    }
}
