package eu.coherentpaas.mongodb.wrapper.sqlgen;

import com.mongodb.BasicDBObject;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.plan.Aggregate;
import eu.coherentpaas.cqe.plan.ColRef;
import eu.coherentpaas.cqe.plan.Expression;
import eu.coherentpaas.cqe.plan.Func;
import eu.coherentpaas.cqe.plan.Limit;
import eu.coherentpaas.cqe.plan.NamedExpr;
import eu.coherentpaas.cqe.plan.Operation;
import eu.coherentpaas.cqe.plan.Project;
import eu.coherentpaas.cqe.plan.Select;
import eu.coherentpaas.cqe.plan.Sort;
import eu.coherentpaas.cqe.plan.SortColumn;
import eu.coherentpaas.cqe.plan.SortOrder;
import eu.coherentpaas.cqe.plan.TableRef;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import eu.coherentpaas.mongodb.wrapper.MongoOperationType;
import eu.coherentpaas.mongodb.wrapper.exceptions.MalformedQueryException;
import eu.coherentpaas.mongodb.wrapper.exceptions.MongoDBQueryMalformedException;
import eu.coherentpaas.mongodb.wrapper.exceptions.MultipleCollectionsQueryException;
import eu.coherentpaas.mongodb.wrapper.exceptions.NoCollectionQueryException;
import eu.coherentpaas.mongodb.wrapper.exceptions.UnknownOperationException;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQueryAggregationFactory;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQuerySelectFactory;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBSorterParam;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBSorterType;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class TransformQuerySelectBuilder {
    private static final Logger Log = LoggerFactory.getLogger(TransformQuerySelectBuilder.class);
    private final BasicDBObject projectionDbObject = new BasicDBObject();
    private BasicDBObject queryDbObject = new BasicDBObject();
    private final MongoDBSorterParam mongoDBSorterParam = new MongoDBSorterParam();
    private MongoOperationType operationType;
    private Integer limit;
    private String collectionName;
    private boolean includeIDInResult = false;
    List<NamedTableExpression> namedTables = null;
    List<GroupByElement> groupByElements = null;
    
    public MongoDBQuerySelectFactory build(Operation operation) throws CQEException {
        
        if(operation instanceof Limit)
            parseLimit((Limit) operation);
        
        if(operation instanceof Project)
            parseProjection((Project) operation);
        
        
        
        MongoDBQuerySelectFactory factory;
        if(operationType==MongoOperationType.AGGREGATION) {
            factory = new MongoDBQueryAggregationFactory(groupByElements, collectionName, operationType, queryDbObject, projectionDbObject, mongoDBSorterParam, limit, includeIDInResult, namedTables);
        } else {
            factory = new MongoDBQuerySelectFactory(
                collectionName, operationType, queryDbObject, 
                projectionDbObject, mongoDBSorterParam, limit, includeIDInResult, namedTables);
        }
        
        
        
        return factory;
    }
    
    private void parseLimit(Limit limitOperation) throws CQEException {
            
        //((MongoDBQuerySelect)this.mongoDBQueryParam).setLimit(limitOperation.getLimit());
        this.limit = limitOperation.getLimit();

        Operation [] limitOperations = limitOperation.getOperands();
        if(limitOperations==null) {
            MongoDBQueryMalformedException ex = new MongoDBQueryMalformedException("Limit operation does not have valid operands");
            throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
        }

        for(Operation operationElem : Arrays.asList(limitOperations)) {
            build(operationElem);
        }        
    }
    
   
    
    
    private void parseProjection(Project projection) throws CQEException {
        
        
        if(projection.isDistinct())
            this.operationType = MongoOperationType.DISTINCT;
        else
            this.operationType = MongoOperationType.SELECT;
        
        
        NamedExpr[] columns = projection.getColumns();
        projectionDbObject.put(MongoMVCCFields._ID, 0);
        projectionDbObject.put(MongoMVCCFields.DATA_ID, 1);
        projectionDbObject.put(MongoMVCCFields.TID, 1);
        for (NamedExpr column : columns) {
            if((column.getName()!=null)&&(column.getName().equalsIgnoreCase(MongoMVCCFields._ID)))
                this.includeIDInResult = true;
            else {
                if(column.getName()!=null)
                    projectionDbObject.put(column.getName(), 1);
                else {
                    if(column.getValue() instanceof ColRef) {
                        ColRef colRef = (ColRef) column.getValue();
                        projectionDbObject.put(colRef.getColref()[0], 1);
                    } else {
                        MalformedQueryException ex = new MalformedQueryException("Projection query is malformed. Cannot find colref to project");
                        CQEException cqeEx = new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                        throw cqeEx;
                    }
                }
            }
        }
        
          
        
        Operation operation = projection.getOperands()[0];
        
        parseOperation(projection, operation);
        
    } 
    
    private void parseOperation(Project projection, Operation operation) throws CQEException {
        
        if(operation instanceof TableRef)
            parseOperation(projection, (TableRef) operation);
        
        else if(operation instanceof Select)
            parseOperation(projection, (Select) operation);
        
        else if(operation instanceof Sort)
            parseOperation(projection, (Sort) operation);
        
        else if(operation instanceof Aggregate) {
            //reset to aggretation operation type
            this.operationType = MongoOperationType.AGGREGATION;
            
            //remove projectin object, it will be re-constructed for the aggregation special case
            projectionDbObject.clear();
            
            parseOperation(projection, (Aggregate) operation);
        } 
        
        else {
            UnknownOperationException ex = new UnknownOperationException("Query projects onto an unknown operation type: " + operation.getClass().getName());
            CQEException cqeEx = new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
            throw cqeEx;
        }
    }
    
    private void parseOperation(Project projection, TableRef operation) {
        if(operation instanceof TableRef) { 
            this.collectionName = ((TableRef) operation).getName();
        }
    }
    
    private void parseOperation(Project projection, Aggregate operation) throws CQEException {
        if(operation.getOperands().length!=1) {
            MalformedQueryException ex = new MalformedQueryException("Aggregation operation must have exactly one operand. Now it has " + operation.getOperands().length);
            CQEException cqeEx = new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
            throw cqeEx;
        }
        
        if(!((operation.getOperands()[0] instanceof Select ) || (operation.getOperands()[0] instanceof TableRef ))) {
            MalformedQueryException ex = new MalformedQueryException("Aggregation operation must have exactly one SELECT or TABLEREF operand. Now it has type" + operation.getOperands()[0].getClass().getName());
            CQEException cqeEx = new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
            throw cqeEx;
        }
        
        parseOperation(projection, operation.getOperands()[0]);
        
        
        //GET THE GROUP-BY FIELDS
        groupByElements = parseGroupByElements(operation.getGroupBy());    
        
        //GET THE AGGREGATION FUNCTIONS
        groupByElements = parseAggregationElements(groupByElements, operation.getAggregates());
        
        //re-set and add new projections for the aggragetion special case
        parseAggregationProjection(projectionDbObject, projection.getColumns());
        
    }
    
    private void parseAggregationProjection(BasicDBObject projection, NamedExpr[] columns) throws CQEException {
        for (NamedExpr column : columns) {
            if(column.getValue() instanceof ColRef) {
                ColRef colRef = (ColRef) column.getValue();
                projectionDbObject.put(colRef.getColref()[0], 1);
            } else {
                MalformedQueryException ex = new MalformedQueryException("Projection query is malformed. Cannot find colref to project");
                CQEException cqeEx = new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                throw cqeEx;
            }
        }
    }
    
    private List<GroupByElement> parseGroupByElements(NamedExpr [] namedExpressions) throws CQEException {
        List<GroupByElement> groupByElements = new LinkedList<>();
        if(namedExpressions==null)
            return groupByElements;
        
        for(NamedExpr groupByExpression : namedExpressions) {
            if(!(groupByExpression.getValue() instanceof ColRef)) {
                MalformedQueryException ex = new MalformedQueryException("Group by expressions must be type of 'ColRef'. The '" + groupByExpression.getName() + "' is of type: " + groupByExpression.getValue().getClass().getName());
                CQEException cqeEx = new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                throw cqeEx;
            }
            
            groupByElements.add(new GroupByElement(groupByExpression.getName(), ((ColRef) groupByExpression.getValue()).getColref()[0]));
        }
        
        return groupByElements;
    }
    
    private List<GroupByElement> parseAggregationElements(List<GroupByElement> groupByElements, NamedExpr [] namedExpressions) throws CQEException {
        for(NamedExpr aggregationExpression : namedExpressions) {
            if(!(aggregationExpression.getValue() instanceof Func)) {
                MalformedQueryException ex = new MalformedQueryException("Aggregation expressions must be type of 'Func'. The '" + aggregationExpression.getName() + "' is of type: " + aggregationExpression.getValue().getClass().getName());
                CQEException cqeEx = new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                throw cqeEx;
            }
            
            Func aggregationFunction = (Func) aggregationExpression.getValue();
            AggregationElement.checkValidAggregationOperation(aggregationFunction.getFunction());
            
            if(aggregationFunction.getFunction().equalsIgnoreCase("count"))  {
                //if count, then one element is of interest, get it and return
                groupByElements.add(new CountElement(aggregationFunction.getFunction(), aggregationExpression.getName(), null));
                return groupByElements;
            }
            
            if(aggregationFunction.getOperands().length!=1) {
                MalformedQueryException ex = new MalformedQueryException("Aggregation function must have exactly one operand. this has: " + aggregationFunction.getOperands().length);
                CQEException cqeEx = new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                throw cqeEx;
            }
            
            if(!(aggregationFunction.getOperands()[0] instanceof ColRef)) {
                MalformedQueryException ex = new MalformedQueryException("Aggregation function operand must be of type ColRef. The '" + aggregationExpression.getName() + "' is of type: " + aggregationFunction.getOperands()[0].getClass().getName());
                CQEException cqeEx = new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                throw cqeEx;
            }
            
            groupByElements.add(new AggregationElement(aggregationFunction.getFunction(), aggregationExpression.getName(), ((ColRef) aggregationFunction.getOperands()[0]).getColref()[0]));
        }
        
        return groupByElements;
        
    }
    
    private void parseOperation(Project projection, Select operation) throws CQEException {
        if(operation instanceof Select) {
            Select selectOp = operation;

            if(selectOp.getOperands().length==0) {
                NoCollectionQueryException ex = new NoCollectionQueryException("Cannot perform query in a non existing collection");
                Log.error("{}", ex.getMessage());
                throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
            }

            if(selectOp.getOperands().length>1) {
                MultipleCollectionsQueryException ex = new MultipleCollectionsQueryException("Cannot perform query in more than one collection");
                Log.error("{}", ex.getMessage());
                throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
            }

            this.collectionName = ((TableRef) selectOp.getOperands()[0]).getName();
            Expression expression = selectOp.getFilter();

            MongoDBExpressionVisitor mongoDBExpressionVisitor = new MongoDBExpressionVisitor();
            this.queryDbObject = (BasicDBObject) mongoDBExpressionVisitor.interpret(expression);
            this.namedTables = mongoDBExpressionVisitor.getNamedTables();
        }
    }
    
    private void parseOperation(Project projection, Sort operation) throws CQEException {
        if(operation instanceof Sort) {
            Sort sortOp = (Sort) projection.getOperands()[0];
            
            //get SORT elements
            for(SortColumn sortColumn : Arrays.asList(sortOp.getColumns())) {
                Expression expression = sortColumn.getColumn();
                if(expression instanceof ColRef) {
                    ColRef colRef = (ColRef) expression;
                    String [] colRefs = colRef.getColref();
                    if(colRefs==null) {
                        MongoDBQueryMalformedException ex = new MongoDBQueryMalformedException("Sort operation does not have a valid ColRef expression");
                        Log.error("{}", ex.getMessage());
                        throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                    }
                    
                    String colRefName = colRefs[0];
                    if(sortColumn.getOrder()==SortOrder.asc)
                        this.mongoDBSorterParam.addNext(colRefName, MongoDBSorterType.ASC);
                        //((MongoDBQuerySelect)this.mongoDBQueryParam).getMongoDBSorterParam().addNext(colRefName, MongoDBSorterType.ASC);
                    if(sortColumn.getOrder()==SortOrder.desc)
                        this.mongoDBSorterParam.addNext(colRefName, MongoDBSorterType.DESC);
                        ///((MongoDBQuerySelect)this.mongoDBQueryParam).getMongoDBSorterParam().addNext(colRefName, MongoDBSorterType.DESC);
                } else {
                    MongoDBQueryMalformedException ex = new MongoDBQueryMalformedException("Sort operation does not have a ColRef expression");
                    Log.error("{}", ex.getMessage());
                    throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                }
               
            }
            
            //check for other operations (i.e WHERE clause etc)
            for(Operation _operation : Arrays.asList(sortOp.getOperands()))
                parseOperation(projection, _operation);
            
        }
        
    }
    
}
