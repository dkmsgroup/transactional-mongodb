package eu.coherentpaas.mongodb.wrapper.sqlgen.query;

import com.mongodb.BasicDBObject;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.wrapper.MongoOperationType;
import eu.coherentpaas.mongodb.wrapper.sqlgen.NamedTableExpression;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author idezol
 */
public class MongoDBQueryInsertFactory implements MongoDBQueryFactory {
    private final BasicDBObject insertObject = new BasicDBObject();
    private String collectionName;
    private List<MongoDBParam> parameters = new LinkedList<>();
    private List<NamedTableExpression> namedTables = new LinkedList<>();
    private Type[] signature;
    protected String [] projectionColumnNames;

    @Override
    public MongoDBTransformationQuery createInstance() throws CQEException {
        MongoDBQueryInsert insertQuery = new MongoDBQueryInsert(insertObject, collectionName, MongoOperationType.INSERT, signature, projectionColumnNames, parameters, namedTables);
        return insertQuery;
    }

    @Override
    public void setSignature(Type[] signature) {
        this.signature = signature;
    }

    @Override
    public void setProjectionColumnNames(String[] projectionColumnNames) {
        this.projectionColumnNames = projectionColumnNames;
    }
    
    @Override
    public void addParam(MongoDBParam parameter) {
        this.parameters = MongoDBQueryFactoryUtils.addUniqueParam(parameters, parameter);
    }
    
    @Override
    public void addNamedTable(NamedTableExpression namedTable) {
        this.namedTables = MongoDBQueryFactoryUtils.addUniqueNamedTableExpr(namedTables, namedTable);
    }

    @Override
    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public BasicDBObject getInsertObject() {
        return insertObject;
    }
    
}
