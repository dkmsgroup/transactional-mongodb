package eu.coherentpaas.mongodb.wrapper.sqlgen.query;

import com.mongodb.BasicDBObject;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.wrapper.MongoOperationType;
import eu.coherentpaas.mongodb.wrapper.sqlgen.NamedTableExpression;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author idezol
 */
public class MongoDBQueryDelete extends MongoDBTransformationQuery {
    protected final BasicDBObject deleteQueryDbObject;

    public MongoDBQueryDelete(String collectionName, MongoOperationType operationType, Type[] signature, String [] columnNames, List<MongoDBParam> parameters, List<NamedTableExpression> namedTables, BasicDBObject deleteQuery) {
        super(collectionName, operationType, signature, columnNames, parameters, namedTables);
        this.deleteQueryDbObject = deleteQuery;
    }

    public BasicDBObject getDeleteQueryDbObject() {
        return deleteQueryDbObject;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.deleteQueryDbObject);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MongoDBQueryDelete other = (MongoDBQueryDelete) obj;
        return Objects.equals(this.deleteQueryDbObject, other.deleteQueryDbObject);
    }

    @Override
    public String toString() {
        return "MongoDBQueryDelete{" + "deleteQueryDbObject=" + deleteQueryDbObject + '}';
    }
    
    
    
    
}
