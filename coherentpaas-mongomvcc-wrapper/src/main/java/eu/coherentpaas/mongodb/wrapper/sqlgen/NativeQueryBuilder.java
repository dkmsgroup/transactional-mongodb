package eu.coherentpaas.mongodb.wrapper.sqlgen;


import com.mongodb.BasicDBObject;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.NativeQueryPlan;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import eu.coherentpaas.mongodb.wrapper.exceptions.MalformedQueryException;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBNativeQueryFactory;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQueryFactory;import eu.coherentpaas.mongodb.wrapper.sqlgen.utils.ParsingUtils;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map.Entry;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class NativeQueryBuilder {
    private static final Logger Log = LoggerFactory.getLogger(NativeQueryBuilder.class);
    private static final String eol = System.getProperty("line.separator");
    private static final char space = new Character(" ".charAt(0)).charValue();
    private static final char bracketRight = new Character("]".charAt(0)).charValue();
    private static final char coma = new Character(",".charAt(0)).charValue();
    private static final char braceLeft = new Character("{".charAt(0)).charValue();
    private static final char braceRight = new Character("}".charAt(0)).charValue();
    private static final char parenthesisLeft = new Character("(".charAt(0)).charValue();
    private static final char parenthesisRight = new Character(")".charAt(0)).charValue();
    private static final char charDollar = new Character("$".charAt(0)).charValue();
    private static final char doubleQuote = new Character("\"".charAt(0)).charValue();
    private static final char doubleDot = new Character(":".charAt(0)).charValue();    
    private static final char dot = new Character(".".charAt(0)).charValue();
    private LinkedList<String> arguments;
    private String collectionName;
    private String operationName;
    StringBuilder sb;
    
    
    
    public MongoDBQueryFactory build(NativeQueryPlan plan) throws CQEException {
        MongoDBNativeQueryFactory factory = (MongoDBNativeQueryFactory) build(plan.getCode());
        if(factory.getOperationName().equalsIgnoreCase("find")) {
            if(factory.getArguments().size()==2) {
                //second argument is the projection fields, check if id is excluded
                factory.setIncludeIDInResult(isIDIncludedInProjection(ParsingUtils.getDBObjectFromString(factory.getArguments().get(1))));
            }
        } else if(factory.getOperationName().equalsIgnoreCase("findOne")) {
            if(factory.getArguments().size()>1) 
                //second argument is the projection fields, check if id is excluded
                factory.setIncludeIDInResult(isIDIncludedInProjection(ParsingUtils.getDBObjectFromString(factory.getArguments().get(1))));
        }
        return factory;
    }
    
    protected MongoDBQueryFactory build(String code) throws CQEException {
        Log.debug("Native code: {}", code);
        sb = new StringBuilder();
        arguments = new LinkedList<>();
        //get rid of the first chars and position to collection
        int index = code.indexOf("db.", 0)+3;
        Log.debug("Remaining string: {}", code.substring(index));
        
        
        //get collection name
        if(!StringUtils.isAlphanumeric(code.substring(index, index+1))) 
            throwException("Collection must start with an alphanumeric char");
        
        
        while(code.charAt(index)!=dot) {
            sb.append(code.charAt(index));
            index++;
        } //collection founed        
        
        this.collectionName = sb.toString();
        sb = new StringBuilder();
        index++;
        
        //get operation name
        while(code.charAt(index)!=parenthesisLeft) {
            sb.append(code.charAt(index));
            index++;
        } //operation found
        this.operationName = sb.toString();
        
        
        Log.debug("Remaining code after get operation name: {}", code.substring(index));
        //skip chars until u reach the first "("
        index = getNextCharPosition(code, index, parenthesisLeft)+1;
        if(index>=code.length()) 
            throwException("no left parenthesis found after the operation");
        
        
        while(index<code.length()) {
            //skip chars until u reach a valid '{' or ")" that means the operation has no args
            index = getNextCharPosition(code, index, braceLeft, parenthesisRight);
            if(code.charAt(index)==parenthesisRight) //end of the arguments section
                return _build();
            else if(code.charAt(index)==braceLeft) {
                //get argumemt
                index = getArgument(code, index)+1;                
            }
        }
        
        return null;
    }
    
    private MongoDBNativeQueryFactory _build() {
        MongoDBNativeQueryFactory factory = new MongoDBNativeQueryFactory();
        factory.setCollectionName(collectionName);
        factory.setOperationName(operationName);
        factory = fixArgs(factory);
        
        //fix arguments
        if(operationName.equalsIgnoreCase("aggregate"))
            factory = fixAggregateFunc(factory);
        
        return factory;
    }
    
    private MongoDBNativeQueryFactory fixAggregateFunc(MongoDBNativeQueryFactory factory) {
        
        StringBuilder sb = new StringBuilder("[");
        boolean isFirst = true;
        for(String parameter : factory.getArguments()) {
            if(isFirst) 
                isFirst = false;
            else
                sb.append(",");
            
            //sb.append(removeDouBleQuotesFromReservedWords(parameter));
            sb.append(parameter);
        }
        
        
        sb.append("]");
        
        //clear multiple parameters and reset to all-in-one list of parameters
        factory.getArguments().clear();
        factory.getArguments().add(sb.toString());
        return factory;
    }
    
    private String removeDouBleQuotesFromReservedWords(String str) {
        
        if(str.contains("$$match"))
            str = str.replace("$$match", "$match");
        
        if(str.contains("$$group"))
            str = str.replace("$$group", "$group");
        
        if(str.contains("$$order"))
            str = str.replace("$$order", "$order");
        
        if(str.contains("$$limit"))
            str = str.replace("$$limit", "$limit");
        
        if(str.contains("$$avg"))
            str = str.replace("$$avg", "$avg");
        
        if(str.contains("$$sum"))
            str = str.replace("$$sum", "$sum");
        
        return str;
    }
    
    private MongoDBNativeQueryFactory fixArgs(MongoDBNativeQueryFactory factory ) {
        ListIterator<String> it = this.arguments.listIterator();
        while(it.hasNext()) {
            String arg = it.next();
            arg = fixArg(arg);
            factory.addArguments(arg);
        }
        return factory;
    }
    
    private String fixArg(String str) {
        StringBuilder _sb = new StringBuilder();
        boolean foundParameter = false;
        for(int i=0; i<str.length(); i++) {
            char character = str.charAt(i);
          
            if((character==space)||(character==doubleDot)) {
                _sb.append(space);
                _sb.append(character);
                if(foundParameter)
                    foundParameter = false;
                continue;
            }
            
            if(character==charDollar) {
                //candidate for parameter
                if(str.charAt(i-1)!=doubleQuote) {
                    //no mongo reserved word, then it is a parameter, mark as parameter with double dollards
                    _sb.append(space);
                    _sb.append(charDollar);
                    _sb.append(character);
                    foundParameter=true;
                    continue;
                }
            }
            
            if(((character==braceRight)||(character==bracketRight)||(character==coma))
                    && (foundParameter)) {
                //parameter ended, add a space after to distinguish later
                _sb.append(space);
                _sb.append(character);
                foundParameter=false;
                continue;
            }
            
            //nothing interesting, just put the char into the string
            _sb.append(str.charAt(i));
        }
        return _sb.toString();
    }
    
    private int getArgument(String str, int index) {
        this.sb = new StringBuilder();
        int count = 1;
        this.sb.append(str.charAt(index));
        //parse until the bracket closes --this means that left--right so count will be 0
        while(count>0) {
            index++;
            if(str.charAt(index)==braceLeft) {
                count++;
            } else if (str.charAt(index)==braceRight) {
                count--;
            }
            this.sb.append(str.charAt(index));
        }
        this.arguments.add(this.sb.toString());
        return index;
    }
    
    private int getNextCharPosition(String str, int index, char character) {
        while(index<str.length()) {
            if(str.charAt(index)==character) {
                return index;
            } else
                index++;
        }
        return -1;
    }
    
    
    private int getNextCharPosition(String str, int index, char character1, char character2) {
        while(index<str.length()) {
            if((str.charAt(index)==character1)||(str.charAt(index)==character2)) {
                return index;
            } else {
                index++;
            }
        }
        return -1;
    }
    
    
    private void throwException(String message) throws CQEException {
        MalformedQueryException ex = new MalformedQueryException(message);
        throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
    }
    
    private boolean isIDIncludedInProjection(BasicDBObject dBObject) throws CQEException {
        boolean toInclude = true;
        Iterator<Entry<String, Object>> it = dBObject.entrySet().iterator();
        while(it.hasNext()) {
            Entry<String, Object> entry = it.next();
            if(entry.getKey().equalsIgnoreCase(MongoMVCCFields._ID)) {
                try {
                    Integer value = (Integer) entry.getValue();
                    if(value==0) 
                        toInclude = false;
                } catch(Exception ex) {
                    throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                }
                
                break;
            }
        }
        return toInclude;
    }
}
