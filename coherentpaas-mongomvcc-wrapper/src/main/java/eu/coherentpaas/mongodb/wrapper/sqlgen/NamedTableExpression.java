package eu.coherentpaas.mongodb.wrapper.sqlgen;

import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.ResultSet;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.cqe.datastore.Parameterized;
import eu.coherentpaas.mongodb.wrapper.exceptions.ParameterNotSetException;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class NamedTableExpression {
    private static final Logger Log = LoggerFactory.getLogger(NamedTableExpression.class);
    private final String name;
    private final List<MongoDBParam> params = new LinkedList<>();
    private Type[] signature;
    private Parameterized preparedStatement;

    public NamedTableExpression(String name) {
        this.name = name;
    }

    public void setSignature(Type[] signature) {
        this.signature = signature;
    }

    public void setPreparedStatement(Parameterized preparedStatement) {
        this.preparedStatement = preparedStatement;
    }

    public String getName() {
        return name;
    }

    public List<MongoDBParam> getParams() {
        return params;
    }
    
    public Type[] getSignature() {
        return signature;
    }

    public Parameterized getPreparedStatement() {
        return preparedStatement;
    }
    
    public void addParam(MongoDBParam param) {
        this.params.add(param);
    }
    
    public ResultSet executeExpression(TxnCtx txnCtx) throws CQEException, InterruptedException {
        _checkPreparedStatementIsSet();
        _assignParameters();        
        
        ResultSet resultSet = this.preparedStatement.execute(txnCtx);
        return resultSet;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NamedTableExpression other = (NamedTableExpression) obj;
        return Objects.equals(this.name, other.name);
    }

    @Override
    public String toString() {
        return "NamedTableExpression{" + "name=" + name + '}';
    }
    
    private void _checkPreparedStatementIsSet() throws CQEException {
        if(this.preparedStatement==null) {
            ParameterNotSetException ex = new ParameterNotSetException("Statement " + this.name + " has not been set yet");
            throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
        }
    }
    
    private void _assignParameters() throws CQEException {
        for(MongoDBParam param : this.params) {
            switch(param.getDataType()) {
                case ARRAY:
                    this.preparedStatement.setArray(param.getName(), (ArrayList<?>) param.getValue());
                    break;
                case BINARY:
                    this.preparedStatement.setBinary(param.getName(), (Blob) param.getValue());
                    break;
                case BOOL:
                    Log.warn("Cannot assign boolean value");
                    break;
                case DICTIONARY:
                    this.preparedStatement.setDictionary(param.getName(), (HashMap<?, ?>) param.getValue());
                    break;
                case DOUBLE:
                    this.preparedStatement.setDouble(param.getName(), (Double) param.getValue());
                    break;
                case FLOAT:
                    this.preparedStatement.setFloat(param.getName(), (Float) param.getValue());
                    break;
                case INT:
                    this.preparedStatement.setInt(param.getName(), (Integer) param.getValue());
                    break;
                case LONG:
                    this.preparedStatement.setLong(param.getName(), (Long) param.getValue());
                    break;
                case STRING:
                    this.preparedStatement.setString(param.getName(), (String) param.getValue());
                    break;
                case TIMESTAMP:
                    this.preparedStatement.setTimestamp(param.getName(), (Timestamp) param.getValue());
                    break;
                case UNKNOWN:
                    Log.warn("Unknown type for parameter " + param.getName());
                    break;
                default:
                    break;
            }
        }
    }
    
}
