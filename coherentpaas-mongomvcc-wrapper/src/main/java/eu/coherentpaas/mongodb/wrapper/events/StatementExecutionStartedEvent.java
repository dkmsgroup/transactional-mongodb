package eu.coherentpaas.mongodb.wrapper.events;

import java.util.EventObject;

/**
 * This event will be fired when a statement starts executing a statement, 
 * so that other open statements to be informed and close their result sets
 * 
 * @author idezol
 */
public class StatementExecutionStartedEvent  extends EventObject {
    public StatementExecutionStartedEvent(Object source) {
        super(source);
    }
    
}
