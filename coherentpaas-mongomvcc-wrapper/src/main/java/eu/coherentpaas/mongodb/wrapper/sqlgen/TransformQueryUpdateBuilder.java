package eu.coherentpaas.mongodb.wrapper.sqlgen;

import com.mongodb.BasicDBObject;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.plan.Call;
import eu.coherentpaas.cqe.plan.ColRef;
import eu.coherentpaas.cqe.plan.Const;
import eu.coherentpaas.cqe.plan.Expression;
import eu.coherentpaas.cqe.plan.NamedExpr;
import eu.coherentpaas.cqe.plan.Operation;
import eu.coherentpaas.cqe.plan.OperationExpression;
import eu.coherentpaas.cqe.plan.Param;
import eu.coherentpaas.cqe.plan.Project;
import eu.coherentpaas.cqe.plan.Select;
import eu.coherentpaas.cqe.plan.TableRef;
import eu.coherentpaas.cqe.plan.Update;
import eu.coherentpaas.mongodb.wrapper.exceptions.MalformedQueryException;
import eu.coherentpaas.mongodb.wrapper.exceptions.MultipleCollectionsQueryException;
import eu.coherentpaas.mongodb.wrapper.exceptions.NoCollectionQueryException;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQueryUpdateFactory;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamUnknown;
import eu.coherentpaas.mongodb.wrapper.sqlgen.utils.ParsingUtils;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class TransformQueryUpdateBuilder {
    private static final Logger Log = LoggerFactory.getLogger(TransformQueryDeleteBuilder.class);
    private final BasicDBObject setParametersDbObject = new BasicDBObject();
    private BasicDBObject queryDbObject;
    private String collectionName;
    private final List<NamedTableExpression> namedTables = new LinkedList<>();
    private List<String> columnNames = null;
    private List<Object> columnData = null;
    
    public MongoDBQueryUpdateFactory build(Update updateOp) throws CQEException {   
        
        if((updateOp.getOperands()==null) || (updateOp.getOperands().length!=2)) 
            throwException("invalid number of operands in this update oparation.");
        
        for(Operation op : updateOp.getOperands()) {
            if(!(op instanceof Project))
                throwException("Operands under the Update operation must be of type Project. This is of type: " + op.getClass().getName());
            
            if(((Project)op).getOperands()==null) {
                parseSetAssigment((Project) op);
            } else 
                parseSelection((Project) op);
        }
        
        if(columnNames!=null) {
            BasicDBObject obj = new BasicDBObject();
            for(int i=0; i<columnNames.size(); i++) {
                obj.append(columnNames.get(i), columnData.get(i));
            }
            this.setParametersDbObject.append("$set", obj);
        }
        
        MongoDBQueryUpdateFactory factory = new MongoDBQueryUpdateFactory(queryDbObject, setParametersDbObject);
        factory.setCollectionName(collectionName);
        factory.setNamedTables(namedTables);
        
        return factory;
    }
    
    
    private void parseSetAssigment(Project project) throws CQEException {
        if(project.getColumns()!=null) {
            this.columnData = new ArrayList<>(project.getColumns().length);
            for(NamedExpr namedExpr : project.getColumns()) {
                Expression expression = namedExpr.getValue();
                if(expression instanceof Const)
                    this.columnData.add(getColumn((Const) expression));
                else if (expression instanceof Param) 
                    this.columnData.add(getParam((Param) expression));
                else if(expression instanceof OperationExpression) {
                    Operation operation = ((OperationExpression) expression).getValue();
                    if(operation instanceof Call) {
                        NamedTableExpression namedTable = ParsingUtils.parseNamedTable((Call) operation);
                        this.namedTables.add(namedTable);
                        this.columnData.add(namedTable);
                    } else {
                        throwException("Invalid type of operation. It was expected Call but instead it is a " + operation.getClass().getName());
                    }
                } else
                    throwException("Uknown expression type. It was expected either Const, Param or Call, but found: " + expression.getClass().getName());
            }
        } 
    }
    
    private void parseSelection(Project project) throws CQEException {
        if((project.getOperands()==null) || (project.getOperands().length!=1)) 
            throwException("invalid number of operands in this project oparation.");
        
        Operation operation = project.getOperands()[0];
        
        if(operation instanceof Select) {
            Select selectOp = (Select) operation;

            if(selectOp.getOperands().length==0) {
                NoCollectionQueryException ex = new NoCollectionQueryException("Cannot perform query in a non existing collection");
                Log.error("{}", ex.getMessage());
                throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
            }

            if(selectOp.getOperands().length>1) {
                MultipleCollectionsQueryException ex = new MultipleCollectionsQueryException("Cannot perform query in more than one collection");
                Log.error("{}", ex.getMessage());
                throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
            }

            this.collectionName = ((TableRef) selectOp.getOperands()[0]).getName();
            Expression expression = selectOp.getFilter();

            MongoDBExpressionVisitor mongoDBExpressionVisitor = new MongoDBExpressionVisitor();
            this.queryDbObject = (BasicDBObject) mongoDBExpressionVisitor.interpret(expression);
            for(NamedTableExpression namedTable : mongoDBExpressionVisitor.getNamedTables()) 
                this.namedTables.add(namedTable);
            
            
        } else {
            throwException("A select operation is expected under a projection");
        }
        
        if(project.getColumns()!=null) {
            this.columnNames = new ArrayList<>(project.getColumns().length);
            for(NamedExpr namedExpr : project.getColumns()) {
                Expression expression = namedExpr.getValue();
                if(expression instanceof ColRef) {
                    this.columnNames.add(((ColRef)expression).getColref()[0]);
                } else {
                    throwException("Invalid exression. Expected a ColRef but found a " + expression.getClass().getName());
                }
            }
        }
    }
    
    private Object getColumn(Const column) throws CQEException {
        try {
            return ParsingUtils.getValueByConst(column);
        } catch(SQLException ex) {
            throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
        }
    }
    
    private Object getParam(Param param) {
        return new MongoDBParamUnknown(param.getName());
    }
    
    private void throwException(String message) throws CQEException {
        MalformedQueryException ex = new MalformedQueryException(message);
        throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
    }
}
