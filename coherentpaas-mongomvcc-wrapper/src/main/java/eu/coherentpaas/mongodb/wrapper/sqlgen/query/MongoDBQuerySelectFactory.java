package eu.coherentpaas.mongodb.wrapper.sqlgen.query;

import com.mongodb.BasicDBObject;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.wrapper.MongoOperationType;
import eu.coherentpaas.mongodb.wrapper.exceptions.InvalidQueryOperationException;
import eu.coherentpaas.mongodb.wrapper.exceptions.NoCollectionQueryException;
import eu.coherentpaas.mongodb.wrapper.sqlgen.NamedTableExpression;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class MongoDBQuerySelectFactory implements MongoDBQueryFactory {
    private static final Logger Log = LoggerFactory.getLogger(MongoDBQuerySelectFactory.class);
    protected String collectionName;
    protected MongoOperationType operationType;
    protected BasicDBObject queryDbObject;
    protected BasicDBObject projectDbObject;
    protected MongoDBSorterParam mongoDBSorterParam;
    protected Integer limit;
    protected Type[] signature;
    protected String [] projectionColumnNames;
    protected List<MongoDBParam> parameters = new LinkedList<>();
    protected List<NamedTableExpression> namedTables = new LinkedList<>();
    protected boolean includeIDInResult = false;
    
    public MongoDBQuerySelectFactory() {
        
    }

    public MongoDBQuerySelectFactory(String collectionName, MongoOperationType operationType, BasicDBObject queryDbObject, BasicDBObject projectDbObject, MongoDBSorterParam mongoDBSorterParam, Integer limit, boolean includeIDInResult) {
        this.collectionName = collectionName;
        this.operationType = operationType;
        this.queryDbObject = queryDbObject;
        this.projectDbObject = projectDbObject;
        this.mongoDBSorterParam = mongoDBSorterParam;
        this.limit = limit;
        this.includeIDInResult = includeIDInResult;
    }
    
    public MongoDBQuerySelectFactory(String collectionName, MongoOperationType operationType, BasicDBObject queryDbObject, BasicDBObject projectDbObject, MongoDBSorterParam mongoDBSorterParam, Integer limit, boolean includeIDInResult, List<NamedTableExpression> namedTables) {
        this.collectionName = collectionName;
        this.operationType = operationType;
        this.queryDbObject = queryDbObject;
        this.projectDbObject = projectDbObject;
        this.mongoDBSorterParam = mongoDBSorterParam;
        this.limit = limit;
        this.includeIDInResult = includeIDInResult;
        if(namedTables!=null)
            this.namedTables = namedTables;
    }

    @Override
    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public void setOperationType(MongoOperationType operationType) {
        this.operationType = operationType;
    }

    public void setQueryDbObject(BasicDBObject queryDbObject) {
        this.queryDbObject = queryDbObject;
    }

    public void setProjectDbObject(BasicDBObject projectDbObject) {
        this.projectDbObject = projectDbObject;
    }

    public void setMongoDBSorterParam(MongoDBSorterParam mongoDBSorterParam) {
        this.mongoDBSorterParam = mongoDBSorterParam;
       
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public void setIncludeIDInResult(boolean includeIDInResult) {
        this.includeIDInResult = includeIDInResult;
    }
    
    
    @Override
    public void addParam(MongoDBParam parameter) {
        this.parameters = MongoDBQueryFactoryUtils.addUniqueParam(parameters, parameter);
    }
    
    @Override
    public void addNamedTable(NamedTableExpression namedTable) {
        this.namedTables = MongoDBQueryFactoryUtils.addUniqueNamedTableExpr(namedTables, namedTable);
    }
    
    @Override
    public MongoDBQuery createInstance() throws CQEException {
        
        if(this.collectionName==null) {
            String message = "Collection name cannot be null";
            NoCollectionQueryException ex = new NoCollectionQueryException(message);
            Log.error(message);
            throw new CQEException(CQEException.Severity.Statement, message, ex);
        }
        
        if((this.operationType==null)) {
            String message = "Operation type has not been set";
            InvalidQueryOperationException ex = new InvalidQueryOperationException(message);
            Log.error(message);
            throw new CQEException(CQEException.Severity.Statement, message, ex);
        }
        
        if(!((this.operationType==MongoOperationType.SELECT)||(this.operationType==MongoOperationType.DISTINCT))) {
            String message = "No valid operation type. Must be either SELECT or DISTINCT";
            InvalidQueryOperationException ex = new InvalidQueryOperationException(message);
            Log.error(message);
            throw new CQEException(CQEException.Severity.Statement, message, ex);
        }
        
        MongoDBQuerySelect mongoDBQuerySelect = new MongoDBQuerySelect(
                        this.queryDbObject, 
                        this.projectDbObject, 
                        this.mongoDBSorterParam, 
                        this.limit, 
                        this.collectionName, 
                        this.operationType,
                        this.signature,
                        this.projectionColumnNames,
                        this.parameters,
                        this.namedTables,
                        this.includeIDInResult);
        
        return mongoDBQuerySelect;
    }

    @Override
    public void setSignature(Type[] signature) {
        this.signature = signature;
    }

    @Override
    public void setProjectionColumnNames(String[] projectionColumnNames) {
        this.projectionColumnNames = projectionColumnNames;
    }
}
