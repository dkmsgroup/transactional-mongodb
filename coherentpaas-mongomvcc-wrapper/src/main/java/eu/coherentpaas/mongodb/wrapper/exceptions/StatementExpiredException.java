package eu.coherentpaas.mongodb.wrapper.exceptions;

/**
 *
 * @author idezol
 */
public class StatementExpiredException extends Exception {
    public StatementExpiredException(String message) {
        super(message);
    }
    
}
