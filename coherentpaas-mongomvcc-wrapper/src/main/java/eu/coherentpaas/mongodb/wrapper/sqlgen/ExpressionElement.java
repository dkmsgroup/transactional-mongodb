package eu.coherentpaas.mongodb.wrapper.sqlgen;

import com.mongodb.BasicDBObject;
import eu.coherentpaas.cqe.plan.Expression;

/**
 *
 * @author idezol
 */
public class ExpressionElement {
    
    private BasicDBObject dbObject;
    private Expression expression;

    public ExpressionElement(BasicDBObject dbObject, Expression expression) {
        this.dbObject = dbObject;
        this.expression = expression;
    }
    
    public void accept(MongoDBExpressionVisitor visitor) {
        
    }
}
