package eu.coherentpaas.mongodb.wrapper.sqlgen;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.ResultSet;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.wrapper.exceptions.ParameterNotSetException;
import eu.coherentpaas.mongodb.wrapper.exceptions.QueryResultException;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamReference;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import org.eclipse.jetty.util.log.Log;

/**
 *
 * @author idezol
 */
public class MongoDBSetDependencies {
    private final TxnCtx txnCtx;
    private final List<MongoDBParam> parameters;

    public MongoDBSetDependencies(TxnCtx txnCtx, List<MongoDBParam> parameters) {
        this.txnCtx = txnCtx;
        this.parameters = parameters;
    }
    
    public List<DBObject> getListOfObjects(BasicDBObject dBObject, NamedTableExpression namedTableExpression) throws CQEException, InterruptedException {
        List<DBObject> listOfobjectsToAdd = new ArrayList<>();
        namedTableExpression = assignParametersToNamedTable(namedTableExpression);
        ResultSet resultSet = null;
        try {
            resultSet = namedTableExpression.executeExpression(txnCtx);
            Object [][] result = resultSet.next();
            for(Object [] row : result) {
                BasicDBObject objectToAdd = new BasicDBObject();
                Iterator<Entry<String, Object>> it = dBObject.entrySet().iterator();
                int index = 0;
                while(it.hasNext()) {
                    objectToAdd.append(it.next().getKey(), row[index++]);
                }
                listOfobjectsToAdd.add(objectToAdd);
            }
        } catch(Exception ex) {
            throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
        } finally {
            if(resultSet!=null)
                resultSet.close();
        }
        return listOfobjectsToAdd;
    }
    
    
    public BasicDBObject setParameters(BasicDBObject dBObject) throws CQEException, InterruptedException {
        //create a copy of the original query object and use this for construct the final query to be executed
        //doing this, allows to change the params. otherwise parameters would have been overrided by the param value, and no replace could take place
        BasicDBObject _dbObject = (BasicDBObject) dBObject.copy();
        return _setParameters(_dbObject);
    }
    
    
    @SuppressWarnings("BroadCatchBlock")
    private BasicDBObject _setParameters(BasicDBObject dBObject) throws CQEException, InterruptedException {
        Iterator<Map.Entry<String, Object>> it = dBObject.entrySet().iterator();
        while(it.hasNext()) {
            Map.Entry entry = it.next();
            Object obj = entry.getValue();
            
            if(obj instanceof MongoDBParam) {
                MongoDBParam param = getParameters(((MongoDBParam)obj).getName());
                if(param.getValue()==null) {
                    ParameterNotSetException ex = new ParameterNotSetException("Parameter " + param.getName() + " has not been set");
                    throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
                }
                entry.setValue(param.getValue());
                continue;
            }
            
            if(obj instanceof BasicDBObject) {
                BasicDBObject _dbObject = _setParameters((BasicDBObject) obj);
                entry.setValue(_dbObject);
                continue;
            }
            
            if(obj instanceof BasicDBList) {                
                Iterator<Object> _it = ((BasicDBList) obj).iterator();
                while(_it.hasNext()) {
                    BasicDBObject _obj = _setParameters((BasicDBObject) _it.next());
                }
            }
            
            if(obj instanceof NamedTableExpression) {
                NamedTableExpression namedTable = assignParametersToNamedTable((NamedTableExpression) obj);
                
                
                ResultSet resultSet = null;
                try {
                    resultSet = namedTable.executeExpression(txnCtx);
                    Type [] signature = ((NamedTableExpression) obj).getSignature();
                    if(signature.length==1) {
                        BasicDBList listToReplace = populateListFromResultSet(resultSet, signature[0]);
                        entry.setValue(listToReplace);
                    } else {
                        QueryResultException ex = new QueryResultException("The result set of the nested query is invalid. IN clauses require only one column");
                        throw ex;
                    }
                } catch(Exception ex) {
                    throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
                } finally {
                    if(resultSet!=null)
                        resultSet.close();
                }
            }
        }
        
        return dBObject;
    }
    
    
    private NamedTableExpression assignParametersToNamedTable(NamedTableExpression namedTable) {
        List<MongoDBParam> namedTableParams = namedTable.getParams();
        ListIterator<MongoDBParam> itParam = namedTableParams.listIterator();
        while(itParam.hasNext()) {
            MongoDBParam bParam = itParam.next();
            if(bParam instanceof MongoDBParamReference) {
                String namedTableParamName = bParam.getName();
                String paramReferedStr = ((MongoDBParam) bParam.getValue()).getName();
                MongoDBParam paramRefered = getParameters(paramReferedStr);
                if(paramRefered!=null) {
                    bParam = paramRefered;
                    bParam.setName(namedTableParamName);
                    itParam.set(bParam);
                } else {
                    Log.warn("Could not find refered param: {}", paramReferedStr);
                }
            }                    
        }
        
        return namedTable;
    }
    

    private BasicDBList populateListFromResultSet(ResultSet resultSet, Type type) throws CQEException {
        BasicDBList list = new BasicDBList();
        Object [][] result;
        if(resultSet!=null) { 
            result = resultSet.next();
            while((result!=null)&&(result.length>0)) {
                for (Object [] row : result) {
                    list.add(getObjectFromResultRow(row, type));
                }
                result = resultSet.next();
            }
        }
            
        /*
        if(resultSet!=null) { 
            while((result = resultSet.next()).length>0) {
                for (Object [] row : result) {
                    list.add(getObjectFromResultRow(row, type));
                }
            }
        }
        */
        
        //System.out.println("MongoDBWrapper received " + list.size() + " number of rows from the named table");
        
        return list;
    } 
    
    private Object getObjectFromResultRow(Object [] row, Type type) throws CQEException {
        
        Object value = row[0];
        switch(type) {
            case ARRAY:
                return (ArrayList) value;
            case BINARY:
                return (Blob) value;
            case BOOL:
                return (boolean) value;
            case DICTIONARY:
                return (Dictionary) value;
            case DOUBLE:
                return (double) value;
            case FLOAT:
                return (float) value;
            case INT:
                return (int) value;
            case LONG:
                return (long) value;
            case STRING:
                return (String) value;
            case TIMESTAMP:
                return (Timestamp) value;
            case UNKNOWN:
                return value;
            default:
                return "";
        }
    }
    
    private MongoDBParam getParameters(String name) {
        for(MongoDBParam param : parameters) {
            if(param.getName().equalsIgnoreCase(name))
                return param;
        }
        
        return null;
    }
}