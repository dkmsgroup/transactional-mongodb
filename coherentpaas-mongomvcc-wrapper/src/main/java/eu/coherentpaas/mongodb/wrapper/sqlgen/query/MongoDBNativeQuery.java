package eu.coherentpaas.mongodb.wrapper.sqlgen.query;

import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.wrapper.sqlgen.NamedTableExpression;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.concurrent.Immutable;

/**
 *
 * @author idezol
 */
@Immutable
public class MongoDBNativeQuery extends MongoDBQuery {
    private final LinkedList<String> args;
    private final String operationName;
    private final boolean includeIDInResult;

    public MongoDBNativeQuery(LinkedList<String> args, String collectionName, Type[] signature, String [] columnNames, List<MongoDBParam> parameters, List<NamedTableExpression> namedTables, String operationName, boolean includeIDInResult) {
        super(collectionName, signature, columnNames, parameters, namedTables);
        this.args = args;
        this.operationName = operationName;
        this.includeIDInResult = includeIDInResult;
    }

    public LinkedList<String> getArgs() {
        return args;
    }

    public String getOperationName() {
        return operationName;
    }

    public boolean isIncludeIDInResult() {
        return includeIDInResult;
    }
}
