package eu.coherentpaas.mongodb.wrapper.sqlgen.query;

import com.mongodb.BasicDBObject;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.wrapper.MongoOperationType;
import eu.coherentpaas.mongodb.wrapper.sqlgen.NamedTableExpression;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author idezol
 */
public class MongoDBQueryDeleteFactory implements MongoDBQueryFactory {
    private final BasicDBObject deleteObject;
    private String collectionName;
    private List<MongoDBParam> parameters = new LinkedList<>();
    private List<NamedTableExpression> namedTables = new LinkedList<>();
    private Type[] signature;
    protected String [] projectionColumnNames;

    public MongoDBQueryDeleteFactory(BasicDBObject deleteObject) {
        this.deleteObject = deleteObject;
    }

    
    
    @Override
    public MongoDBTransformationQuery createInstance() throws CQEException {
        MongoDBQueryDelete query = new MongoDBQueryDelete(collectionName, MongoOperationType.DELETE, signature, projectionColumnNames, parameters, namedTables, deleteObject);
        return query;
    }

    @Override
    public void setSignature(Type[] signature) {
        this.signature = signature;
    }

    @Override
    public void setProjectionColumnNames(String[] projectionColumnNames) {
        this.projectionColumnNames = projectionColumnNames;
    }
    
    
    @Override
    public void addParam(MongoDBParam parameter) {
        this.parameters = MongoDBQueryFactoryUtils.addUniqueParam(parameters, parameter);
    }

    @Override
    public void addNamedTable(NamedTableExpression namedTable) {
        this.namedTables = MongoDBQueryFactoryUtils.addUniqueNamedTableExpr(namedTables, namedTable);
    }

    @Override
    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public BasicDBObject getDeleteObject() {
        return deleteObject;
    }
 
    
}
