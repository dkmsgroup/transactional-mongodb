package eu.coherentpaas.mongodb.wrapper.sqlgen.query.params;

import eu.coherentpaas.cqe.Type;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author idezol
 */
public abstract class MongoDBParam implements Serializable {
    protected String name;
    protected Type dataType;

    protected MongoDBParam() {
    }

    protected MongoDBParam(String name, Type dataType) {
        this.name = name;
        this.dataType = dataType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Type getDataType() {
        return dataType;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass().getSuperclass() != MongoDBParam.class) {
            return false;
        }
        final MongoDBParam other = (MongoDBParam) obj;
        return Objects.equals(this.name, other.name);
    }

    @Override
    public String toString() {
        return "MongoDBParam{" + "name=" + name + ", dataType=" + dataType + '}';
    }
    
    public abstract Object getValue();
    
    public abstract void setValue(Object value);
}
