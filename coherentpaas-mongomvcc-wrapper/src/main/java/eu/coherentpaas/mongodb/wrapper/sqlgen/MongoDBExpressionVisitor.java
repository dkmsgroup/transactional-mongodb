package eu.coherentpaas.mongodb.wrapper.sqlgen;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.plan.Call;
import eu.coherentpaas.cqe.plan.ColRef;
import eu.coherentpaas.cqe.plan.Const;
import eu.coherentpaas.cqe.plan.Expression;
import eu.coherentpaas.cqe.plan.Func;
import eu.coherentpaas.cqe.plan.InList;
import eu.coherentpaas.cqe.plan.InQuery;
import eu.coherentpaas.cqe.plan.Operation;
import eu.coherentpaas.cqe.plan.Param;
import eu.coherentpaas.mongodb.wrapper.exceptions.InvalidParameterTypeException;
import eu.coherentpaas.mongodb.wrapper.exceptions.InvalidQueryOperationException;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamUnknown;
import eu.coherentpaas.mongodb.wrapper.sqlgen.utils.ParsingUtils;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;


/**
 *
 * @author idezol
 */
public class MongoDBExpressionVisitor  {
    private final List<NamedTableExpression> namedTables = new LinkedList<>();

    public MongoDBExpressionVisitor() {
    }

    public List<NamedTableExpression> getNamedTables() {
        return namedTables;
    }
    
    public Object interpret(Expression expr) throws CQEException {
        
        if(expr instanceof ColRef)
            return this._interpet((ColRef) expr);
        if(expr instanceof Const) {
            try {
                return this._interpet((Const) expr);
            } catch(SQLException ex) {
                throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
            }
        }
        if(expr instanceof Func)
            return this._interpet((Func) expr);
        if(expr instanceof Param)
            return this._interpet((Param) expr);
        if(expr instanceof InList)
            return this._interpet((InList) expr);
        if(expr instanceof InQuery)
            return this._interpet((InQuery) expr);
        
        return null;
    }

    private String _interpet(ColRef expr) {
        return expr.getColref()[0];
    }

    private Object _interpet(Const expr) throws SQLException {
        return ParsingUtils.getValueByConst(expr);
    }

    private BasicDBObject _interpet(Func expr) throws CQEException {
        String function = expr.getFunction();
        Expression [] operands = expr.getOperands();        
        
        if(function.equals("=")) 
            return this.interpretEquals(operands);
        
        if(function.equals("LIKE")) 
            return this.interpretEquals(operands, true);
        
        if(function.equals("<")) 
            return this.interpretSimpleClausesFunc(operands, "$lt");
        
        if((function.equals("<="))||(function.equals("=<")))
            return this.interpretSimpleClausesFunc(operands, "$lte");
        
        if(function.equals(">")) 
            return this.interpretSimpleClausesFunc(operands, "$gt");
        
        if((function.equals("=>"))||(function.equals(">="))) 
            return this.interpretSimpleClausesFunc(operands, "$gte");
        
        if((function.equals("!="))||(function.equals("<>"))) 
            return this.interpretSimpleClausesFunc(operands, "$ne");
        
        if(function.equalsIgnoreCase("BETWEEN")) 
            return this.interpretBetweenFunc(operands);        
        
        if(function.equalsIgnoreCase("AND")) 
            return this.interpretAndFunc(operands);
        
        if(function.equalsIgnoreCase("OR")) 
            return this.interpretOrFunc(operands);
        
        if(function.equalsIgnoreCase("NOT")) 
            return this.interpretNotFunc(operands);
        
        return null;
    }
    
    
    private MongoDBParam _interpet(Param param) {
        return new MongoDBParamUnknown(param.getName());
    }
    
    private Object _interpet(InQuery expr) throws CQEException {
        
        Expression [] operands = expr.getOperands(); 
        
        if(operands.length!=1) {
            InvalidQueryOperationException exception = new InvalidQueryOperationException("The IN oparation must have only one operand");
            throw new CQEException(CQEException.Severity.Execution, exception.getMessage(), exception);
        }
        
        Operation operation = expr.getValues();
        if(!(operation instanceof Call)) {
            InvalidQueryOperationException exception = new InvalidQueryOperationException("The IN_QUERY oparation must be associated with a call to another operation");
            throw new CQEException(CQEException.Severity.Execution, exception.getMessage(), exception);
        }
        
       
        NamedTableExpression namedTable = ParsingUtils.parseNamedTable((Call) operation);
        this.namedTables.add(namedTable);
        
        BasicDBObject dBObject = new BasicDBObject(((ColRef)operands[0]).getColref()[0], 
                                new BasicDBObject("$in", namedTable));
        
        return dBObject;
    }
    
    private Object _interpet(InList expr) throws CQEException {
        
        Expression [] operands = expr.getOperands(); 
        
        if(operands.length!=1) {
            InvalidQueryOperationException exception = new InvalidQueryOperationException("The IN oparation must have only one operand");
            throw new CQEException(CQEException.Severity.Execution, exception.getMessage(), exception);
        }
        
        Expression [] expressions = expr.getValues();
        BasicDBList listIn = new BasicDBList();
        for(Expression expression : Arrays.asList(expressions)) {
            listIn.add(this.interpret(expression));
        }
        
        BasicDBObject dBObject = new BasicDBObject(((ColRef)operands[0]).getColref()[0], 
                                new BasicDBObject("$in", listIn));
        
        return dBObject;
    }
    
    
    private BasicDBObject interpretEquals(Expression [] operands) throws CQEException {
        return interpretEquals(operands, false);
    }
    
    private BasicDBObject interpretEquals(Expression [] operands, boolean isLIKEOperator) throws CQEException {
        if(operands.length!=2) {
            InvalidQueryOperationException exception = new InvalidQueryOperationException("The equal oparation must have two arguments");
            throw new CQEException(CQEException.Severity.Execution, exception.getMessage(), exception);
        }

        BasicDBObject dBObject;
        if(!isLIKEOperator) {
            dBObject = new BasicDBObject((String) this.interpret(operands[0]), this.interpret(operands[1]));
        } else {
            //if LIKE operation, then check for '%' expressions
            Object obj = this.interpret(operands[1]);
            if(!(obj instanceof String)) {
                InvalidParameterTypeException ex = new InvalidParameterTypeException(obj + " operation must be type STRING in the LIKE function");
                CQEException exCQE = new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                throw exCQE;
            }
            
            String stringExpression = (String) obj;
            
            if((stringExpression.startsWith("%")) && (stringExpression.endsWith("%"))) {
                stringExpression = stringExpression.substring(1, stringExpression.length()-1);
                obj = new BasicDBObject("$regex", stringExpression )
                        .append("$options", "i");
            } else if(stringExpression.endsWith("%")) {
                stringExpression = "^" + stringExpression.substring(0, stringExpression.length()-1);
                obj = new BasicDBObject("$regex", stringExpression)
                        .append("$options", "i");
                
            } else if (stringExpression.startsWith("%")) {
                stringExpression = stringExpression.substring(1) + "$";
                obj = new BasicDBObject("$regex", stringExpression)
                        .append("$options", "i");
            } else {
                obj = stringExpression;
            }
            
            dBObject = new BasicDBObject((String) this.interpret(operands[0]), obj);
        }
        
        
        return dBObject;
    }
    
    private BasicDBObject interpretSimpleClausesFunc(Expression [] operands, String clause) throws CQEException {
        if(operands.length!=2) {
            InvalidQueryOperationException exception = new InvalidQueryOperationException("The " + clause + " oparation must have two arguments");
            throw new CQEException(CQEException.Severity.Execution, exception.getMessage(), exception);
        }

        BasicDBObject dBObject = new BasicDBObject(
                (String) this.interpret(operands[0]), 
                new BasicDBObject(clause, this.interpret(operands[1])));
        return dBObject;
    }
    
    private BasicDBObject interpretBetweenFunc(Expression [] operands) throws CQEException {
        if(operands.length!=3) {
            InvalidQueryOperationException exception = new InvalidQueryOperationException("The between oparation must have two arguments");
            throw new CQEException(CQEException.Severity.Execution, exception.getMessage(), exception);
        }

        if(operands[0].getClass()!=ColRef.class) {
            InvalidQueryOperationException exception = new InvalidQueryOperationException("The first oparation of a between function must be " + ColRef.class);
            throw new CQEException(CQEException.Severity.Execution, exception.getMessage(), exception);
        }

        BasicDBObject dBObjectValue = new BasicDBObject();
        if(operands[1].getClass()==(Const.class))
            dBObjectValue = dBObjectValue.append("$gte", ((Const) operands[1]).getValue());
        if(operands[2].getClass()==(Const.class))
            dBObjectValue = dBObjectValue.append("$lte", ((Const) operands[2]).getValue());


        BasicDBObject dBObject = new BasicDBObject(((ColRef)operands[0]).getColref()[0], dBObjectValue);
        return dBObject;
    }
    
    private BasicDBObject interpretAndFunc(Expression [] operands) throws CQEException {
        if(operands.length<2) {
            InvalidQueryOperationException exception = new InvalidQueryOperationException("The AND oparation must have at least two arguments");
            throw new CQEException(CQEException.Severity.Execution, exception.getMessage(), exception);
        }

        BasicDBObject dbObjectResult = new BasicDBObject();
        for(Expression andOperand : operands) {
            if(andOperand instanceof Func) {
                //an and operation must always have operands of type Func
                BasicDBObject andOperandElement = (BasicDBObject) this.interpret(andOperand);
                Iterator<Entry<String, Object>> it = andOperandElement.entrySet().iterator();
                while(it.hasNext()) {
                    Entry<String, Object> entry = it.next();
                    dbObjectResult.append(entry.getKey(), entry.getValue());
                }
            } else {
                InvalidQueryOperationException exception = new InvalidQueryOperationException("The AND oparation must have operands of type Func");
                throw new CQEException(CQEException.Severity.Execution, exception.getMessage(), exception);
            }
        }

        return dbObjectResult;
    }
    
    private BasicDBObject interpretOrFunc(Expression [] operands) throws CQEException {
        if(operands.length<2) {
            InvalidQueryOperationException exception = new InvalidQueryOperationException("The OR oparation must have at least two arguments");
            throw new CQEException(CQEException.Severity.Execution, exception.getMessage(), exception);
        }


        BasicDBList orList = new BasicDBList();
        for(Expression orOperand : operands) {
            if(orOperand instanceof Func) {
                //an and operation must always have operands of type Func
                BasicDBObject orOperandElement = (BasicDBObject) this.interpret(orOperand);
                orList.add(orOperandElement);
            } else {
                InvalidQueryOperationException exception = new InvalidQueryOperationException("The OR oparation must have operands of type Func");
                throw new CQEException(CQEException.Severity.Execution, exception.getMessage(), exception);
            }
        }

        BasicDBObject dbObjectResult = new BasicDBObject("$or", orList);

        return dbObjectResult;
    }
    
    private BasicDBObject interpretNotFunc(Expression [] operands) throws CQEException {
        String message = "NOT operation is not supported in MongoBD. Please, use DeMorgan's Law to define an equivalent valid query";
        InvalidQueryOperationException exception = new InvalidQueryOperationException(message);
        throw new CQEException(CQEException.Severity.Execution, exception.getMessage(), exception);
    }
    
    private MongoDBParam interpretParameter(String paramName, Const expr) {
        return ParsingUtils.interpretParameter(paramName, expr);
    }
}
