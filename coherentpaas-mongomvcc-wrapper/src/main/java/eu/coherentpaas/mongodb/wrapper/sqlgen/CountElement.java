package eu.coherentpaas.mongodb.wrapper.sqlgen;

/**
 *
 * @author idezol
 */
public class CountElement extends AggregationElement {

    public CountElement(String function, String name, String referencedField) {
        super(function, name, referencedField);
    }
    
}
