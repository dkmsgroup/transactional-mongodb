package eu.coherentpaas.mongodb.wrapper;

/**
 *
 * @author idezol
 */
public enum MongoOperationType {
    
    SELECT,
    DISTINCT,
    INSERT,
    UPDATE,
    DELETE,
    AGGREGATION
}
