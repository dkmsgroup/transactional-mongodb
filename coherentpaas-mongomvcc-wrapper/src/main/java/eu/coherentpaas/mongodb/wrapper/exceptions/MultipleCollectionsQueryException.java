package eu.coherentpaas.mongodb.wrapper.exceptions;

/**
 *
 * @author idezol
 */
public class MultipleCollectionsQueryException extends Exception {
    public MultipleCollectionsQueryException(String message) {
        super(message);
    }
    
}
