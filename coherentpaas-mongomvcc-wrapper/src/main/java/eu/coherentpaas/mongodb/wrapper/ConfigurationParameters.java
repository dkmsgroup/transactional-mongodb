package eu.coherentpaas.mongodb.wrapper;

/**
 *
 * @author idezol
 */
public class ConfigurationParameters {
    public static final String WRAPPER_CLASS = "wrapper.class";
    public static final String WRAPPER_NAME = "wrapper.name";
    public static final String WRAPPER_TRANSACTIONAL_CLIENT = "wrapper.transactional.client";
    public static final String HOSTNAME = "HOSTNAME";
    public static final String PORT = "PORT";
    public static final String DATABASENAME = "DATABASENAME";
    public static final String SYNCHRONIZE = "SYNCHRONIZE";
}
