package eu.coherentpaas.mongodb.wrapper.sqlgen.query.params;

import eu.coherentpaas.cqe.Type;

/**
 *
 * @author idezol
 */
public class MongoDBParamFloat extends MongoDBParam {
    protected Float value;

    public MongoDBParamFloat(String name) {
        super(name, Type.FLOAT);
    }

    
    @Override
    public Float getValue() {
        return value;
    }

    @Override
    public void setValue(Object value) {
        if(!(value instanceof Float)) 
            throw new IllegalArgumentException("value must be type of " + Type.FLOAT.name());
        else 
            this.value = (Float) value;
    }
    
    @Override
    public String toString() {
        return "MongoDBParamString{" + super.toString() + " value=" + value + '}';
    }
}
