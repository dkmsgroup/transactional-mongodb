package eu.coherentpaas.mongodb.wrapper;

import com.mongodb.DBCollection;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class MongoCollectionMethods {
    private static final Logger Log = LoggerFactory.getLogger(MongoCollectionMethods.class);
    private static List<Method> methods = null;
    private volatile static MongoCollectionMethods instance;
    
    private MongoCollectionMethods() {
    }
    
    protected static MongoCollectionMethods getInstance() {
        while(instance==null) {
            synchronized(MongoCollectionMethods.class) {
                if(instance==null) {
                    instance = new MongoCollectionMethods();
                    init();
                }
            }
        }
        return instance;
    }
    
    
    protected List<Method> getMethods() {
        return MongoCollectionMethods.methods;
    }
    
    private static void init()  {
        Class obj = null;
        try {
            obj =  Class.forName(DBCollection.class.getName());
            Log.info(obj.getName());
        } catch (ClassNotFoundException ex) {
            Log.warn("Could not load Mongo client supported methods. Native queries will be supported. {}: {}", ex.getClass().getName(), ex.getMessage());
        }
        if(obj!=null)
            methods = Arrays.asList(obj.getDeclaredMethods());
        else {
            Log.warn("Could not load Mongo client supported methods. Native queries will be supported");
            methods = new ArrayList<>(0);
        }
    }
}
