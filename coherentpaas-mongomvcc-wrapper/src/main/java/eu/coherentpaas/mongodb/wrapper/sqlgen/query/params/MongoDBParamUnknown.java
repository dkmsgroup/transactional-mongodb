package eu.coherentpaas.mongodb.wrapper.sqlgen.query.params;

import eu.coherentpaas.cqe.Type;

/**
 *
 * @author idezol
 */
public class MongoDBParamUnknown extends MongoDBParam{
    protected Object value;
    
    public MongoDBParamUnknown(String name) {
        super(name, Type.UNKNOWN);
    }

    @Override
    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public Object getValue() {
        return value;
    }

    
    @Override
    public String toString() {
        return "MongoDBParamUnknown{" + super.toString() + " value=" + value + '}';
    }
}
