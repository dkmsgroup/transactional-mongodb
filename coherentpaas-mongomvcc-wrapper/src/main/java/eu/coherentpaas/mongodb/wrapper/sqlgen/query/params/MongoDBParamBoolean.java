package eu.coherentpaas.mongodb.wrapper.sqlgen.query.params;

import eu.coherentpaas.cqe.Type;

/**
 *
 * @author idezol
 */
public class MongoDBParamBoolean extends MongoDBParam {
    protected Boolean value;

    public MongoDBParamBoolean(String name) {
        super(name, Type.BOOL);
    }

    @Override
    public Boolean getValue() {
        return value;
    }

    
    @Override
    public void setValue(Object value) {
        if(!(value instanceof Boolean)) 
            throw new IllegalArgumentException("value must be type of " + Type.BOOL.name());
        else 
            this.value = (Boolean) value;
    }
    
    
}
