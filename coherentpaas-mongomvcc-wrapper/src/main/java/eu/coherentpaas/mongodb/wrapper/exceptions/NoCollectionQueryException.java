package eu.coherentpaas.mongodb.wrapper.exceptions;

/**
 *
 * @author idezol
 */
public class NoCollectionQueryException extends Exception {
    public NoCollectionQueryException(String message) {
        super(message);
    }
    
}
