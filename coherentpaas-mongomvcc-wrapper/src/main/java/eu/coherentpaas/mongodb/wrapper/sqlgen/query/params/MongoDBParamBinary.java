package eu.coherentpaas.mongodb.wrapper.sqlgen.query.params;

import eu.coherentpaas.cqe.Type;
import java.sql.Blob;

/**
 *
 * @author idezol
 */
public class MongoDBParamBinary extends MongoDBParam {
    protected Blob value;
    
    
    public MongoDBParamBinary(String name) {
        super(name, Type.STRING);
    }

    @Override
    public void setValue(Object value) {
        if(!(value instanceof Blob)) 
            throw new IllegalArgumentException("value must be type of " + Type.BINARY.name());
        else 
            this.value = (Blob) value;
    }

    @Override
    public Blob getValue() {
        return value;
    }

    
    @Override
    public String toString() {
        return "MongoDBParamBinary{" + super.toString() + " value=" + value + '}';
    }
}
