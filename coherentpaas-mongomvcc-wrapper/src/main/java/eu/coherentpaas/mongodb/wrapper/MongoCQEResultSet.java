package eu.coherentpaas.mongodb.wrapper;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.ResultSet;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import eu.coherentpaas.mongodb.wrapper.exceptions.ResultSetClosedException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public class MongoCQEResultSet implements ResultSet {
    private static final Logger Log = LoggerFactory.getLogger(MongoCQEResultSet.class);
    
    //THIS SHOULD BE CONFIGURED 
    protected final int defaultNextRows = 200;
    protected int rowsCount = 0;
    protected final DBCursor mongoCursor;
    protected final Type [] signature;
    protected final String [] columnNames;
    protected final boolean includeIDInResult;
    protected volatile AtomicBoolean open = new AtomicBoolean(true);
    
    protected MongoCQEResultSet(DBCursor mongoCursor, boolean  includeIDInResult, Type [] signature, String [] columnNames) {
        this.mongoCursor = mongoCursor;
        this.includeIDInResult = includeIDInResult;
        this.signature = signature;
        this.columnNames = columnNames;
    }
    
    protected boolean isOpen() {
        return this.open.get();
    }

    @Override
    public int getRowCount() throws CQEException {
        Log.warn("MongoMVCC cannot support row count operation on an open cursor");
        return -1;
    }

    /**
     * Obtain an arbitrary number of rows according to the configuration
     * If no more rows are included, it closes the result Set
     * 
     * @return null if no more rows available
     * @throws eu.coherentpaas.cqe.CQEException
     */
    @Override
    public Object[][] next() throws CQEException {
        synchronized(this) {
            if(!this.open.get()) {
                ResultSetClosedException ex = new ResultSetClosedException("Cannot retrieve more data. ResultSet is closed");
                throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
            }
        }
        
        if(this.mongoCursor==null)
            return null;
        
        List<Object[]> rows = new ArrayList<>();
        while((rowsCount<defaultNextRows) && (mongoCursor.hasNext())) {
            BasicDBObject dbObject = (BasicDBObject) mongoCursor.next();
            rows.add(transformRow(dbObject));
            rowsCount++;
        }
 
        
        //reset the counter to zero
        rowsCount=0;
        
        return rows.toArray(new Object[rows.size()][]);
    }

    
    /**
     * Obtain at most max number of rows and then closes the resultSet
     * 
     * @param max the maximum number of rows to fetch
     * @return null if no more rows available
     * @throws eu.coherentpaas.cqe.CQEException
     */
    @Override
    public Object[][] next(int max) throws CQEException {
        synchronized(this) {
            if(!this.open.get()) {
                ResultSetClosedException ex = new ResultSetClosedException("Cannot retrieve more data. ResultSet is closed");
                throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
            }
        }
        
        if(this.mongoCursor==null)
            return null;
        
        List<Object[]> rows = new ArrayList<>();
        while((rowsCount<max) && (mongoCursor.hasNext())) {
            BasicDBObject dbObject = (BasicDBObject) mongoCursor.next();
            rows.add(transformRow(dbObject));
            rowsCount++;
        }
        
        
        return rows.toArray(new Object[rows.size()][]);
    }

    
    /**
     * Close the result set.
     * @throws eu.coherentpaas.cqe.CQEException
     */
    @Override
    public void close() throws CQEException {
        synchronized(this) {
            this.open.set(false);
        }
    }
    
    
    /**
     * Transforms a BasicDBObject to a Object []
     * @param dbObject
     * @return 
     */
    protected Object[] transformRow(BasicDBObject dbObject) {
        Object [] row = new Object[this.columnNames.length];
      
        
        //THIS CODE WHERE IS A WORKAROUND TO DEAL WITH LOWERCASE KEYS OF FIELDS, RETURNED BY THE COMPILER
        Map<String, String> keysMap = getLowerCaseColumns(dbObject);
        for(int i=0; i<this.columnNames.length; i++) {
            String key = keysMap.get(this.columnNames[i]);
            Object field =  dbObject.get(key);
            if(field!=null) {
                row[i] = getValueFromSignature(field, this.signature[i]);
            }
        
        }
        
        
        //THIS CODE WHERE IS A WORKAROUND TO DEAL WITH LOWERCASE KEYS OF FIELDS, RETURNED BY THE COMPILER
        
        
        //THIS CODE SHOULD BE USED INSTEAD
        /*
        for(int i=0; i<this.columnNames.length; i++) {
            Object field = dbObject.get(this.columnNames[i]);
            if(field!=null) {
                row[i] = getValueFromSignature(field, this.signature[i]);
            }
        }
        */
        
        return row;
    }
    
    /**
     * Workaround to deal with the lowercase column names that are returned by the compiler and causes MongoDB to not retrieve the corresponding values, 
     * cause the BasicDBOjbect is case sensitive
     * 
     * This should be removed when the compiler returns the valid names, without transforming them to lowercase
     * @param dbObject the BasicDBObject
     * @return the map between lowercase to keys
     */
    protected HashMap<String, String> getLowerCaseColumns(BasicDBObject dbObject) {
        HashMap<String, String> map = new HashMap<>();
        Iterator<Entry<String, Object>> it = dbObject.entrySet().iterator(); 
        while(it.hasNext()) {
            String entryKey = it.next().getKey();
            map.put(entryKey.toLowerCase(), entryKey);
        }
        return map;
    }
   
    
    protected Object[] stransformRow(BasicDBObject dbObject) {
        LinkedList<Object> list = new LinkedList<>();
        Iterator it = dbObject.entrySet().iterator(); 
        int position = 0;
        while(it.hasNext()) {
            Entry entry = (Entry) it.next();
            if(!isKeyMetaData((String) entry.getKey())) { 
                if(this.signature!=null) {
                    list.add(getValueFromSignature(entry.getValue(), this.signature[position]));
                    position++;
                }
                else
                    list.add(entry.getValue());
            }
        }
        
        //return row;
        return list.toArray();
    }
    
    
    protected Object getValueFromSignature(Object value, Type type) {
        if((type==Type.FLOAT)&&(value instanceof Double)) {
            return Float.parseFloat(((Double) value).toString());
        }
        
        return value;
    }
    
    protected boolean isKeyMetaData(String key) {        
        return ((key.equals(MongoMVCCFields._ID))
                || (key.equals(MongoMVCCFields.COMMIT_TIMESTAMP))
                || (key.equals(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP))
                || (key.equals(MongoMVCCFields.TID))
                || ((key.equalsIgnoreCase(MongoMVCCFields.DATA_ID))&&(!this.includeIDInResult)));
    }

}
