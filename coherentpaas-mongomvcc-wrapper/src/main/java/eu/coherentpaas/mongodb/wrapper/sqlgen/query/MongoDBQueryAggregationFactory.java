package eu.coherentpaas.mongodb.wrapper.sqlgen.query;

import com.mongodb.BasicDBObject;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.mongodb.wrapper.MongoOperationType;
import eu.coherentpaas.mongodb.wrapper.exceptions.InvalidQueryOperationException;
import eu.coherentpaas.mongodb.wrapper.exceptions.NoCollectionQueryException;
import eu.coherentpaas.mongodb.wrapper.sqlgen.GroupByElement;
import eu.coherentpaas.mongodb.wrapper.sqlgen.NamedTableExpression;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public class MongoDBQueryAggregationFactory extends MongoDBQuerySelectFactory {
    private static final Logger Log = LoggerFactory.getLogger(MongoDBQueryAggregationFactory.class);
    protected List<GroupByElement> groupByElements;
    
    public MongoDBQueryAggregationFactory() {
        
    }

    public MongoDBQueryAggregationFactory(
            List<GroupByElement> groupByElements, 
            String collectionName, 
            MongoOperationType operationType, 
            BasicDBObject queryDbObject, 
            BasicDBObject projectDbObject, 
            MongoDBSorterParam mongoDBSorterParam, 
            Integer limit, 
            boolean includeIDInResult, 
            List<NamedTableExpression> namedTables) {
        super(collectionName, operationType, queryDbObject, projectDbObject, mongoDBSorterParam, limit, includeIDInResult, namedTables);
        this.groupByElements = groupByElements;
    }

    
    
    
    
    
    @Override
    public MongoDBQuery createInstance() throws CQEException {
        
        if(this.collectionName==null) {
            String message = "Collection name cannot be null";
            NoCollectionQueryException ex = new NoCollectionQueryException(message);
            Log.error(message);
            throw new CQEException(CQEException.Severity.Statement, message, ex);
        }
        
        if((this.operationType==null)) {
            String message = "Operation type has not been set";
            InvalidQueryOperationException ex = new InvalidQueryOperationException(message);
            Log.error(message);
            throw new CQEException(CQEException.Severity.Statement, message, ex);
        }
        
        if(!(this.operationType==MongoOperationType.AGGREGATION)) {
            String message = "No valid operation type. Must be AGGREGATION";
            InvalidQueryOperationException ex = new InvalidQueryOperationException(message);
            Log.error(message);
            throw new CQEException(CQEException.Severity.Statement, message, ex);
        }
        
        MongoDBQueryAggregation mongoDBQueryAggregation = new MongoDBQueryAggregation
        (       groupByElements, 
                queryDbObject, 
                projectDbObject, 
                mongoDBSorterParam, 
                collectionName, 
                operationType, 
                signature, 
                projectionColumnNames, 
                parameters, 
                namedTables);
        
        return mongoDBQueryAggregation;
    }
    
}
