package eu.coherentpaas.mongodb.wrapper.exceptions;

/**
 *
 * @author idezol
 */
public class QueryResultException extends Exception {
    public QueryResultException(String message) {
        super(message);
    }
}
