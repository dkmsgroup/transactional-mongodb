package eu.coherentpaas.mongodb.wrapper.exceptions;

/**
 *
 * @author idezol
 */
public class InvalidQueryOperationException extends Exception {
    public InvalidQueryOperationException(String message) {
        super(message);
    }
    
    public InvalidQueryOperationException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
