package eu.coherentpaas.mongodb.wrapper.sqlgen.query;

import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.wrapper.sqlgen.NamedTableExpression;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public class MongoDBNativeQueryFactory implements MongoDBQueryFactory { 
    private static final Logger Log = LoggerFactory.getLogger(MongoDBNativeQueryFactory.class);
    protected String collectionName;
    protected Type[] signature;
    protected String [] projectionColumnNames;
    protected List<MongoDBParam> parameters = new LinkedList<>();
    protected List<NamedTableExpression> namedTables = new LinkedList<>();
    protected LinkedList<String> arguments = new LinkedList<>();
    protected String operationName;
    protected boolean includeIDInResult = true;

    @Override
    public MongoDBNativeQuery createInstance() throws CQEException {
        MongoDBNativeQuery mongoDBNativeQuery = new MongoDBNativeQuery(arguments, collectionName, signature, projectionColumnNames, parameters, namedTables, operationName, includeIDInResult);
        return mongoDBNativeQuery;
    }

    @Override
    public void setSignature(Type[] signature) {
        this.signature = signature;
    }

    @Override
    public void setProjectionColumnNames(String[] projectionColumnNames) {
        this.projectionColumnNames = projectionColumnNames;
    }
    
    
    @Override
    public void addParam(MongoDBParam parameter) {
        this.parameters.add(parameter);
    }

    @Override
    public void addNamedTable(NamedTableExpression namedTable) {
        this.namedTables.add(namedTable);
    }

    @Override
    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }
    
    
    public void addArguments(String argument) {
        this.arguments.add(argument);
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public void setIncludeIDInResult(boolean includeIDInResult) {
        this.includeIDInResult = includeIDInResult;
    }

    public LinkedList<String> getArguments() {
        return arguments;
    }
    
    
}
