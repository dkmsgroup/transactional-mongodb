package eu.coherentpaas.mongodb.wrapper.exceptions;

/**
 *
 * @author idezol
 */
public class UnknownResponseTypeException extends Exception {
    public UnknownResponseTypeException(String message) {
        super(message);
    }
}
