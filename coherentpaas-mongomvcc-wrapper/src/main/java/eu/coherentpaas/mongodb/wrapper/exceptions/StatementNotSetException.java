package eu.coherentpaas.mongodb.wrapper.exceptions;

/**
 *
 * @author idezol
 */
public class StatementNotSetException extends Exception {
    public StatementNotSetException(String message) {
        super(message);
    }
    
}
