package eu.coherentpaas.mongodb.wrapper.test.parameters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.QueryPlan;
import eu.coherentpaas.cqe.ResultSet;
import eu.coherentpaas.cqe.datastore.Connection;
import eu.coherentpaas.cqe.datastore.Statement;
import eu.coherentpaas.mongodb.wrapper.App;
import eu.coherentpaas.mongodb.wrapper.ConfigurationParameters;
import eu.coherentpaas.mongodb.wrapper.MongoCQEDatastore;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.transactionmanager.minicluster.TMMiniCluster;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import junit.framework.Assert;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class UpdateNamedTableTest {
    private static final Logger Log = LoggerFactory.getLogger(UpdateNamedTableTest.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 0);
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(App.TestConfiguration.HOSTNAME, App.TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(App.TestConfiguration.DATABASENAME);
        try (DB database = mongoClient.getDB(App.TestConfiguration.DATABASENAME)) {
            database.startTransaction();
            DBCollection collection = database.createCollection("reviewers", null);
            Random generator = new Random();
            List<DBObject> list = new ArrayList<>();
            for(int i=0; i<200; i++) {
                BasicDBObject obj = new BasicDBObject("rev_id", (i+1))
                        .append("name", "sample")
                        .append("pub_id", (i+1))
                        .append("random_id", generator.nextInt(1000));
                if(i<70)
                    obj.append("city", "athens");
                else
                    obj.append("city", "berlin");
                
                list.add(obj);
            }
            
            collection.insert(list);
            database.commit();
            database.startTransaction();
            database.commit();
            
            database.startTransaction();
            collection = database.createCollection("publications", null);
            list = new ArrayList<>();
            for(int i=0; i<200; i++) {
                BasicDBObject obj = new BasicDBObject("rev_id", (i+1))
                        .append("name", RandomStringUtils.random(5))
                        .append("pub_id", (i+1));
                
                list.add(obj);
            }
            
            collection.insert(list);
            database.commit();
            database.startTransaction();
            database.commit();
            
            
            database.startTransaction();
            collection = database.createCollection("authors", null);
            list = new ArrayList<>();
            for(int i=0; i<200; i++) {
                BasicDBObject obj = new BasicDBObject("auth_id", (i+1))
                        .append("pub_id", (i+1));
                if(i<50)
                    obj.append("name", "Pavlaras");
                else
                    obj.append("name", "Kwstas Mitroglou!");
                
                list.add(obj);
            }
            
            collection.insert(list);
            database.commit();
            database.startTransaction();
            database.commit();
            
            Log.info("Records to nested tables added.");
        }
    }
    
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(App.TestConfiguration.HOSTNAME, App.TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(App.TestConfiguration.DATABASENAME);
        Log.info("database dropped");
    }
    
    @Test
    public void test() throws IOException, CQEException, InterruptedException, TransactionManagerException, DataStoreException, Exception {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 0);
        
        Log.info("Setting properties file");
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, App.TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(App.TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, App.TestConfiguration.DATABASENAME);
        Log.info("Properties have been set");
        
        
        
        
        
        InputStream inStreamNested1 = ClassLoader.getSystemResourceAsStream("updated_named_nested1.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlanNested1= mapper.readValue(inStreamNested1, QueryPlan.class);
        InputStream inStreamNested2 = ClassLoader.getSystemResourceAsStream("updated_named_nested2.json");
        QueryPlan queryPlanNested2= mapper.readValue(inStreamNested2, QueryPlan.class);
        
        
        MongoCQEDatastore ddsNested1 = new MongoCQEDatastore();        
        ddsNested1.start(properties);
        Connection connectionNested1 = ddsNested1.getConnection(); 
        Statement statementNested1 = connectionNested1.createStatement();
        statementNested1.prepare(queryPlanNested1);
        MongoCQEDatastore ddsNested2 = new MongoCQEDatastore();        
        ddsNested2.start(properties);
        Connection connectionNested2 = ddsNested2.getConnection(); 
        Statement statementNested2 = connectionNested2.createStatement();
        statementNested2.prepare(queryPlanNested2);
        Log.info("Two nested queries are prepared");
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("updated_named.json");
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        MongoCQEDatastore dds = new MongoCQEDatastore();
        dds.start(properties);
        Connection connection = dds.getConnection(); 
        Statement statement = connection.createStatement();
        statement.prepare(queryPlan);
        
        statement.useNamedTable("t1", statementNested1);
        statement.useNamedTable("t2", statementNested2);
        statement.setInt("x", 30);
        statement.setInt("y", 10);
        LTMClient.getInstance().getConnection().startTransaction();
        TxnCtx txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();  
        statement.execute(txnCtx);
        LTMClient.getInstance().getConnection().commit();
        
        LTMClient.getInstance().getConnection().startTransaction();
        LTMClient.getInstance().getConnection().commit();
        
        
        statementNested1.close();
        connectionNested1.close();
        ddsNested1.close();
        statementNested2.close();
        connectionNested2.close();
        ddsNested2.close();
        statement.close();
        connection.close();
        dds.close();
        
        
        inStream = ClassLoader.getSystemResourceAsStream("updated_named_validator.json");
        queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        dds = new MongoCQEDatastore();
        dds.start(properties);
        connection = dds.getConnection(); 
        statement = connection.createStatement();
        statement.prepare(queryPlan);
        LTMClient.getInstance().getConnection().startTransaction();
        txnCtx = LTMClient.getInstance().getConnection().getTxnCtx(); 
        
        ResultSet resultSet = statement.execute(txnCtx);
        Object [][] result = resultSet.next();
        Assert.assertEquals(10, result.length);
        
        LTMClient.getInstance().getConnection().commit();
        resultSet.close();
        statement.close();
        connection.close();
        dds.close();
       
        
        
    }
    
}
