package eu.coherentpaas.mongodb.wrapper.sqlgen;

import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBNativeQuery;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import junit.framework.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mortbay.log.Log;

/**
 *
 * @author idezol
 */
public class NativeQueryBuilderTest {
    private static List<String> inputs;
    private final NativeQueryBuilder builder = new NativeQueryBuilder();
    
    public static void main(String [] args) throws IOException, CQEException {
        NativeQueryBuilderTest test = new NativeQueryBuilderTest();
        NativeQueryBuilderTest.getStrFromFile();
        test.test1();
        test.test2();
        test.test3();
        test.test4();
        test.test5();
        test.test6();
        test.test7();
        test.test8();
        test.test9();
    }
    
    @Test
    public void test9() throws CQEException {
        MongoDBNativeQuery query = (MongoDBNativeQuery) builder.build(inputs.get(9)).createInstance();
        Assert.assertEquals("publication", query.getCollectionName());
        Assert.assertEquals("insert", query.getOperationName());
        LinkedList<String> args = query.getArgs();
        Assert.assertEquals(8, args.size());
    }
    
    @Test
    public void test8() throws CQEException {
        MongoDBNativeQuery query = (MongoDBNativeQuery) builder.build(inputs.get(8)).createInstance();
        Assert.assertEquals("reviewers", query.getCollectionName());
        Assert.assertEquals("update", query.getOperationName());
        LinkedList<String> args = query.getArgs();
        Assert.assertEquals(2, args.size());
        Log.debug(args.get(0));
        Log.debug(args.get(1));
        Assert.assertEquals("{\"auth_id\" : $$id }", args.get(0));
        Assert.assertEquals("{\"$set :{\"affilation\" : $$name }}", args.get(1));
    }
    
    @Test
    public void test7() throws CQEException {
        MongoDBNativeQuery query = (MongoDBNativeQuery) builder.build(inputs.get(7)).createInstance();
        Assert.assertEquals("reviewers", query.getCollectionName());
        Assert.assertEquals("update", query.getOperationName());
        LinkedList<String> args = query.getArgs();
        Assert.assertEquals(2, args.size());
        Log.debug(args.get(0));
        Log.debug(args.get(1));
        Assert.assertEquals("{\"auth_id\"   : $$id  }", args.get(0));
        Assert.assertEquals("{\"$set\"   :  {\"affilation\"   :   $$name  }}", args.get(1));
    }
    
    @Test
    public void test6() throws CQEException {
        MongoDBNativeQuery query = (MongoDBNativeQuery) builder.build(inputs.get(6)).createInstance();
        Assert.assertEquals("reviewers", query.getCollectionName());
        Assert.assertEquals("update", query.getOperationName());
        LinkedList<String> args = query.getArgs();
        Assert.assertEquals(2, args.size());
        Log.debug(args.get(0));
        Log.debug(args.get(1));
        Assert.assertEquals("{\"auth_id\"   :   $$id }", args.get(0));
        Assert.assertEquals("{\"$set\"   :  {\"affilation\"   :   $$name }}", args.get(1));
    }
    
    @Test
    public void test5() throws CQEException {
        MongoDBNativeQuery query = (MongoDBNativeQuery) builder.build(inputs.get(5)).createInstance();
        Assert.assertEquals("reviewers", query.getCollectionName());
        Assert.assertEquals("remove", query.getOperationName());
        LinkedList<String> args = query.getArgs();
        Assert.assertEquals(1, args.size());
        Log.debug(args.get(0));
        Assert.assertEquals("{\"auth_id\"   :  10}", args.get(0));
    }
    
    @Test
    public void test4() throws CQEException {
        MongoDBNativeQuery query = (MongoDBNativeQuery) builder.build(inputs.get(4)).createInstance();
        Assert.assertEquals("reviewers", query.getCollectionName());
        Assert.assertEquals("update", query.getOperationName());
        LinkedList<String> args = query.getArgs();
        Assert.assertEquals(2, args.size());
        Log.debug(args.get(0));
        Log.debug(args.get(1));
        Assert.assertEquals("{\"auth_id\"   :  50}", args.get(0));
        Assert.assertEquals("{\"$set\"   :  {\"affilation\"   :  \"INRIA/Montpellier\"}}", args.get(1));
    }
    
    @Test
    public void test3() throws CQEException {
        MongoDBNativeQuery query = (MongoDBNativeQuery) builder.build(inputs.get(3)).createInstance();
        Assert.assertEquals("reviewers", query.getCollectionName());
        Assert.assertEquals("find", query.getOperationName());
        LinkedList<String> args = query.getArgs();
        Assert.assertEquals(1, args.size());
        Log.debug(args.get(0));
        Assert.assertEquals("{\"auth_id\"   :  {\"$gt\"   :  30}}", args.get(0));
    }
    
    @Test
    public void test2() throws CQEException {
        MongoDBNativeQuery query = (MongoDBNativeQuery) builder.build(inputs.get(2)).createInstance();
        Assert.assertEquals("reviewers", query.getCollectionName());
        Assert.assertEquals("insert", query.getOperationName());
        LinkedList<String> args = query.getArgs();
        Assert.assertEquals(1, args.size());
        Log.debug(args.get(0));
        Assert.assertEquals("{\"name\" :\"Boyan\",\"affilation\" :\"INRIA\",\"auth_id\" :50}", args.get(0));
    }
    
    @Test
    public void test1() throws CQEException {
        MongoDBNativeQuery query = (MongoDBNativeQuery) builder.build(inputs.get(1)).createInstance();
        Assert.assertEquals("reviewers", query.getCollectionName());
        Assert.assertEquals("insert", query.getOperationName());
        LinkedList<String> args = query.getArgs();
        Assert.assertEquals(1, args.size());
        Log.debug(args.get(0));
        Assert.assertEquals("{\"name\"   :  \"Boyan\",  \"affilation\"   :  \"INRIA\",  \"auth_id\"   :  50}", args.get(0));
    }
    
    @Test
    public void test0() throws CQEException {
        MongoDBNativeQuery query = (MongoDBNativeQuery) builder.build(inputs.get(0)).createInstance();
        Assert.assertEquals("reviewers", query.getCollectionName());
        Assert.assertEquals("find", query.getOperationName());
        LinkedList<String> args = query.getArgs();
        Assert.assertEquals(2, args.size());
        Log.debug(args.get(0));
        Log.debug(args.get(1));
        Assert.assertEquals("{\"auth_id\"   :  {\"$gt\"   :  30}}", args.get(0));
        Assert.assertEquals("{\"_id\"   :  0,  \"name\"   :  1}", args.get(1));
    }
    
    @BeforeClass
    public static void getStrFromFile() throws IOException {
        List<String> list = new ArrayList<>();
        InputStream inStreamIn = ClassLoader.getSystemResourceAsStream("native/mongodb_native_queries");
        BufferedReader reader = new BufferedReader(new InputStreamReader(inStreamIn));
        String line;
        try {
            while( ( line = reader.readLine() ) != null ) {
                list.add(line);
            }
        } finally {
            if(inStreamIn!=null)
                inStreamIn.close();
            if(reader!=null)
                reader.close();
        }
        inputs = list;
    }
}
