package eu.coherentpaas.mongodb.wrapper.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.QueryPlan;
import eu.coherentpaas.cqe.datastore.Connection;
import eu.coherentpaas.cqe.datastore.Statement;
import eu.coherentpaas.mongodb.wrapper.App;
import eu.coherentpaas.mongodb.wrapper.ConfigurationParameters;
import eu.coherentpaas.mongodb.wrapper.MongoCQEDatastore;
import eu.coherentpaas.mongodb.wrapper.exceptions.InvalidQueryOperationException;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.transactionmanager.minicluster.TMMiniCluster;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import junit.framework.Assert;

/**
 *
 * @author idezol
 */
public class NotOperationTest {
    
    public void test() throws IOException, CQEException, InterruptedException, TransactionManagerException, DataStoreException, Exception {
        TMMiniCluster.startMiniCluster(true, 0);
        
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, App.TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(App.TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, App.TestConfiguration.DATABASENAME);
        InputStream inStream = ClassLoader.getSystemResourceAsStream("not_operation.json");        
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        MongoCQEDatastore dds = new MongoCQEDatastore();        
        dds.start(properties);
        Connection connection = dds.getConnection(); 
        Statement statement = connection.createStatement();
        try {
            statement.prepare(queryPlan);
            Assert.fail("An InvalidQueryOperationException should have been thrown because NOT operation is not suppoted in MongoDB");
        } catch(Exception ex) {
            //DO NOTHING
        }
        
        dds.close();
    }
}
