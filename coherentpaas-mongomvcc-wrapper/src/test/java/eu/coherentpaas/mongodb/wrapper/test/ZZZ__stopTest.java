package eu.coherentpaas.mongodb.wrapper.test;

import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.transactionmanager.minicluster.TMMiniCluster;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public class ZZZ__stopTest {
    private static final Logger Log = LoggerFactory.getLogger(ZZZ__stopTest.class);
    
    @BeforeClass
    public static void stopMiniCluster() throws TransactionManagerException {
        
        Log.info("Closting mini cluster");
        LTMClient.close();
        TMMiniCluster.stopMiniCluster();
    }
    
    
    @Test
    public void report() {
        Log.info("Mini Cluster has closed ...");
    }
}
