package eu.coherentpaas.mongodb.wrapper.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author idezol
 */
public class DateFunction {
    
    public static String getDateStringFromDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.setTimeZone(TimeZone.getTimeZone("EET"));
        String formattedDate = formatter.format(date);
        return formattedDate;
    }
    
    public static String getDateStringFromDate(long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.setTimeZone(TimeZone.getTimeZone("EET"));
        String formattedDate = formatter.format(new Date(date));
        return formattedDate;
    }
    
    public static Date getDateFromString(String string) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.setTimeZone(TimeZone.getTimeZone("EET"));
        Date date = formatter.parse(string);
        return date;
    }
    
}
