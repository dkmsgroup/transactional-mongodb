package eu.coherentpaas.mongodb.wrapper.sqlgen.utils;

import com.mongodb.BasicDBObject;
import eu.coherentpaas.cqe.CQEException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author idezol
 */
public class ParsingUtilsTest {
    
    
    public static void main(String [] args) throws IOException, CQEException {
        ParsingUtilsTest test = new ParsingUtilsTest();
        test.test();
    }
    
    
    @Test
    public void test() throws IOException, CQEException {
        List<String> list = getStrFromFile("input_arg_str");
        for(String str : list) {
            str = assignParameters(str);
            BasicDBObject dbObject = ParsingUtils.getDBObjectFromString(str);
            Assert.assertEquals(str, dbObject.toJson());
        }
    }
    
    public static List<String> getStrFromFile(String path) throws IOException {
        List<String> list = new ArrayList<>();
        InputStream inStreamIn = ClassLoader.getSystemResourceAsStream("input_arg_str");
        BufferedReader reader = new BufferedReader(new InputStreamReader(inStreamIn));
        String line;
        try {
            while( ( line = reader.readLine() ) != null ) {
                list.add(line);
            }
        } finally {
            if(inStreamIn!=null)
                inStreamIn.close();
            if(reader!=null)
                reader.close();
        }
        return list;
    }
    
    private String assignParameters(String str) {
        
        boolean foundSth = true;
        while(foundSth) {
            if(str.contains("$$false")) {
                str = str.replace("$$false", "false");
            }
            else if(str.contains("$$long")) {
                str = str.replace("$$long", "13999");
            }
            else if(str.contains("$$in_param")) {
                str = str.replace("$$in_param", "[10, 200]");
            }
            else if(str.contains("$$name")) {
                str = str.replace("$$name", "\"pavlos\"");
            }
            else if(str.contains("$$name_other")) {
                str = str.replace("$$name_other", "\"giannis\"");
            } else {
                foundSth = false;
            }
        }
        return str;
    }
}
