package eu.coherentpaas.mongodb.wrapper.test.parameters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.QueryPlan;
import eu.coherentpaas.cqe.ResultSet;
import eu.coherentpaas.cqe.datastore.Connection;
import eu.coherentpaas.cqe.datastore.Statement;
import eu.coherentpaas.mongodb.wrapper.App;
import eu.coherentpaas.mongodb.wrapper.ConfigurationParameters;
import eu.coherentpaas.mongodb.wrapper.MongoCQEDatastore;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.transactionmanager.minicluster.TMMiniCluster;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class DeleteTest {
    private static final Logger Log = LoggerFactory.getLogger(DeleteTest.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 0);
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(App.TestConfiguration.HOSTNAME, App.TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(App.TestConfiguration.DATABASENAME);
        try (DB database = mongoClient.getDB(App.TestConfiguration.DATABASENAME)) {
            database.startTransaction();
            DBCollection collection = database.createCollection("reviwers", null);
            Random generator = new Random();
            List<DBObject> list = new ArrayList<>();
            for(int i=0; i<200; i++) {
                BasicDBObject obj = new BasicDBObject("rev_id", (i+1))
                        .append("name", RandomStringUtils.random(5))
                        .append("pub_id", (i+1))
                        .append("random_id", generator.nextInt(1000));
                if(i<70)
                    obj.append("city", "athens");
                else
                    obj.append("city", "berlin");
                
                list.add(obj);
            }
            
            collection.insert(list);
            database.commit();
            database.startTransaction();
            database.commit();
            Log.info("Records to nested table added.");
        }
    }
    
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(App.TestConfiguration.HOSTNAME, App.TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(App.TestConfiguration.DATABASENAME);
        Log.info("database dropped");
    }
    
    @Test
    public void test() throws IOException, CQEException, InterruptedException, TransactionManagerException, DataStoreException, Exception {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 0);
        
        Log.info("Setting properties file");
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, App.TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(App.TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, App.TestConfiguration.DATABASENAME);
        Log.info("Properties have been set");
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("delete_const.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        
        
        MongoCQEDatastore dds = new MongoCQEDatastore();        
        dds.start(properties);
        Connection connection = dds.getConnection(); 
        Statement statement = connection.createStatement();
        statement.prepare(queryPlan);
        LTMClient.getInstance().getConnection().startTransaction();
        TxnCtx txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();       
        statement.execute(txnCtx);
        LTMClient.getInstance().getConnection().commitSync();
        statement.close();
        connection.close();
        dds.close();
        
        inStream = ClassLoader.getSystemResourceAsStream("delete_validator.json");
        queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        
        dds = new MongoCQEDatastore();
        dds.start(properties);
        connection = dds.getConnection(); 
        statement = connection.createStatement();
        statement.prepare(queryPlan);
        LTMClient.getInstance().getConnection().startTransaction();
        txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();   
        ResultSet resultSet = statement.execute(txnCtx);
        Object [][] result = resultSet.next();
        Assert.assertEquals(190, result.length);
        Log.info("Total records remaining after deletion: " + result.length + "");
        
        
        LTMClient.getInstance().getConnection().commit();
        resultSet.close();
        statement.close();
        connection.close();
        dds.close();
       
        
        inStream = ClassLoader.getSystemResourceAsStream("delete.json");
        queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        
        dds = new MongoCQEDatastore();
        dds.start(properties);
        connection = dds.getConnection(); 
        statement = connection.createStatement();
        statement.prepare(queryPlan);
        statement.setInt("x", 150);
        LTMClient.getInstance().getConnection().startTransaction();
        txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();   
        statement.execute(txnCtx);
        LTMClient.getInstance().getConnection().commitSync();
        statement.close();
        connection.close();
        dds.close();
        
        
        inStream = ClassLoader.getSystemResourceAsStream("delete_validator.json");
        queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        
        
        dds = new MongoCQEDatastore();
        dds.start(properties);
        connection = dds.getConnection(); 
        statement = connection.createStatement();
        statement.prepare(queryPlan);
        LTMClient.getInstance().getConnection().startTransaction();
        txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();
        resultSet = statement.execute(txnCtx);
        result = resultSet.next();
        Assert.assertEquals(140, result.length);
        Log.info("Total records remaining after deletion: " + result.length + "");
        
        LTMClient.getInstance().getConnection().commit();
           
        
        
    }
}
