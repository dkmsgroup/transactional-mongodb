package eu.coherentpaas.mongodb.wrapper.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.QueryPlan;
import eu.coherentpaas.cqe.ResultSet;
import eu.coherentpaas.cqe.datastore.Connection;
import eu.coherentpaas.cqe.datastore.Statement;
import eu.coherentpaas.mongodb.wrapper.ConfigurationParameters;
import eu.coherentpaas.mongodb.wrapper.MongoCQEDatastore;
import eu.coherentpaas.mongodb.wrapper.sqlgen.MongoDBInterprenter;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQuery;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBTransformationQuery;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.transactionmanager.minicluster.TMMiniCluster;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.Properties;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.AfterClass;
import junit.framework.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public class BibSearchTest {
    private static final Logger Log = LoggerFactory.getLogger(BibSearchTest.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 0);
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASENAME);
        DB database = mongoClient.getDB(TestConfiguration.DATABASENAME);
        database.startTransaction();
        DBCollection collection = database.createCollection("documents", null);
        
        for(int i=0; i<100; i++) {            
            BasicDBObject obj = new BasicDBObject("projectId", (i+1))
                    .append("description ", RandomStringUtils.randomAlphanumeric(50));
            Log.info((i+1) + ": inserting: " + obj);
            collection.insert(obj);
        }
        
        database.commit();
        
        database.startTransaction();
        database.commit();
        Log.info("committed");
        database.close();
    }
    
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASENAME);
        Log.info("database dropped");
    }
    
    @Test
    public void test() throws IOException, CQEException, InterruptedException, TransactionManagerException, DataStoreException {
        Log.info("Setting properties file");
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, TestConfiguration.DATABASENAME);
        Log.info("Properties have been set");
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("bibsearch_long.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        MongoDBQuery query = (new MongoDBInterprenter()).getMongoDBQueryParam(queryPlan);
        Log.info(query.toString());
        
        MongoCQEDatastore dds = new MongoCQEDatastore();
        //LTMClient.register(dds);
        dds.start(properties);
        
        LTMClient.getInstance().getConnection().startTransaction();
        Connection connection = dds.getConnection();
        Statement statement = connection.createStatement();
        
        statement.prepare(queryPlan);
        
        TxnCtx txcCtx = LTMClient.getInstance().getConnection().getTxnCtx();
        ResultSet resultSet = statement.execute(txcCtx);
        
        Object [][] result = resultSet.next();
        //printResults(result);
        Assert.assertEquals(100, result.length);
       
        resultSet.close();
        
        //txcCtx.abort();
        LTMClient.getInstance().getConnection().commit();
        Log.info("total records: " + result.length);
    }
    
    
    
    private static void printResults(Object [][] result) {
        
        for(int i=0; i<result.length; i++) {
            Object[] row = result[i];
            String line = (i+1) + ": ";
            for(int j=0;j<row.length;j++) {
                line+=row[j].toString() + " ";
            }
            System.out.println(line);
        }
    }
    
}
