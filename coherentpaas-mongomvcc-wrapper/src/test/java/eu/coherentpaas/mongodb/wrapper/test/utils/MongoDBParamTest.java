package eu.coherentpaas.mongodb.wrapper.test.utils;

import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamInt;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamString;
import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author idezol
 */
public class MongoDBParamTest {
    
    @Test
    public void test() {
        MongoDBParamInt paramInt = new MongoDBParamInt("test");
        MongoDBParamInt paramInt2 = new MongoDBParamInt("test");
        MongoDBParamString paramString = new MongoDBParamString("test");
        
        Assert.assertEquals(paramInt, paramInt2);
        Assert.assertEquals(paramInt, paramString);
    }
}
