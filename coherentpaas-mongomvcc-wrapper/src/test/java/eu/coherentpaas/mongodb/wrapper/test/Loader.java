package eu.coherentpaas.mongodb.wrapper.test;

import java.io.File;
import java.io.FileReader;

import au.com.bytecode.opencsv.CSVParser;
import au.com.bytecode.opencsv.CSVReader;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;

public class Loader {

	public static void main(String[] args) throws Exception {

		if (args.length == 0 || args[0].equals("--help")) {
			System.out.println("args: host port database file collection");
		}
		String host = args[0]; // "83.212.84.244"
                
                int port = Integer.parseInt(args[1]);
                
		String databaseName = args[2];
		MongoClient mongoClient = MongoClientFactory.getInstance()
				.createClient(host, port);
                mongoClient.dropDatabase(TestConfiguration.DATABASENAME);
		DB database = mongoClient.getDB(databaseName);
		String file = args[3];
		String colName = args[4];
		File f = new File(file);
		try {
			if (f.exists()) {
				database.startTransaction();
				DBCollection coll = database.getCollection(colName);
				CSVReader reader = new CSVReader(new FileReader(f),
						CSVParser.DEFAULT_SEPARATOR, '\0');
				try {

					String[] next = reader.readNext();
					int i = 0;
					String[] columns = null;
					while ((next != null)) {
						if (i == 0) {
							columns = next;
						} else {
							BasicDBObjectBuilder builder = new BasicDBObjectBuilder();
							int j = 0;
							for (String field : columns) {
								builder.add(field.trim(), next[j].trim());
								j++;
							}
							coll.insert(builder.get());
						}
						next = reader.readNext();
						i++;
					}
					database.commit();
                                        
                                        
                                        database.startTransaction();
                                        database.commit();
        
					System.out.println("Inserted " + (i + 1) + " documents");
				} catch (Exception e) {
					database.rollback();
				} finally {

					coll.close();
					reader.close();
				}

			}
		} finally {

			database.close();
			
		}

	}
}
