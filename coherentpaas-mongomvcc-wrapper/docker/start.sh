#!/usr/bin/env bash

mkdir -p $HOME/.m2
touch $HOME/.m2/settings.xml

echo "<settings><servers>" >> $HOME/.m2/settings.xml
echo "<server><id>adapt03-libs</id><username>" >> $HOME/.m2/settings.xml
echo $MVN_USER >> $HOME/.m2/settings.xml
echo "</username><password>" >> $HOME/.m2/settings.xml
echo $MVN_PWD >> $HOME/.m2/settings.xml
echo "</password></server>" >> $HOME/.m2/settings.xml


echo "<server><id>adapt03</id><username>" >> $HOME/.m2/settings.xml
echo $MVN_USER >> $HOME/.m2/settings.xml
echo "</username><password>" >> $HOME/.m2/settings.xml
echo $MVN_PWD >> $HOME/.m2/settings.xml
echo "</password></server>" >> $HOME/.m2/settings.xml


echo "<server><id>adapt03-coherentpaas-snapshots</id><username>" >> $HOME/.m2/settings.xml
echo $MVN_USER >> $HOME/.m2/settings.xml
echo "</username><password>" >> $HOME/.m2/settings.xml
echo $MVN_PWD >> $HOME/.m2/settings.xml
echo "</password></server>" >> $HOME/.m2/settings.xml
echo "</servers></settings>" >> $HOME/.m2/settings.xml

git clone $MVN_USER@adapt03.ls.fi.upm.es:/gitcoherentpaas/cqe-server /home/root/cqe

cd /home/root/cqe && git checkout aggregates

echo "building the CQE project"

cd /home/root/cqe && mvn clean package assembly:assembly

echo "building the sparksee wrapper"

cd /home/root/sparksee-wrapper && mvn package

echo "copying the cqe into the working dir"

cd /home/root/cqe/target && ls && unzip cqe-1.0-SNAPSHOT-bin.zip

cp -R /home/root/cqe/target/cqe-1.0-SNAPSHOT/* /usr/local/cqe

cp /home/root/sparksee-wrapper/target/coherentpaas-mongomvcc-wrapper-2.2-SNAPSHOT-shaded.jar /usr/local/cqe/lib/.

rm /usr/local/cqe/config/wrappers/mockup.properties

echo "wrapper.class = eu.coherentpaas.mongodb.wrapper.MongoCQEDatastore" >> /usr/local/cqe/config/wrappers/sparksee.properties
echo "wrapper.name = mongodb" >> /usr/local/cqe/config/sparksee.properties
echo "HOSTNAME = $SPARKSEE_SERVER_HOST" >> /usr/local/cqe/config/sparksee.properties
echo "PORT = $SPARKSEE_SERVER_PORT" >> /usr/local/cqe/config/sparksee.properties
echo "DATABASENAME = $SPARKSEE_SERVER_DATABASE" >> /usr/local/cqe/config/sparksee.properties
echo "wrapper.transactional.client = eu.coherentpaas.mongodb.wrapper.MongoCQEDatastore" >> /usr/local/cqe/config/sparksee.properties

cat /usr/local/cqe/config/wrappers/sparksee.properties

chmod u+x /usr/local/cqe/bin/*

echo "derby.drda.host=$HOSTNAME" > /usr/local/cqe/derby.properties


echo "starting the cqe server with sparksee-wrapper"

startNetworkServer -p 1527