package com.mongodb;

import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public class SelectTest {
    private static final Logger Log = LoggerFactory.getLogger(SelectTest.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection collection = database.createCollection(TestConfiguration.COLLECTION, null);
        for(int i=0; i<20; i++)
            collection.insert(new BasicDBObject("key", (i+1)));
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        Log.info("DB init with 20 elements");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
    }
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection collection = database.getCollection(TestConfiguration.COLLECTION);
        int count =0;
        DBCursor cursor = collection.find();
        while(cursor.hasNext()) {
            BasicDBObject obj = (BasicDBObject) cursor.next();
            count++;
            Log.info(count + ": " + obj.toString());
        }
        
        Assert.assertEquals(20, count);
        Log.info("20 elements found");
        database.rollback();
        database.close();
    }
    
    @Test
    public void testScan() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection collection = database.getCollection(TestConfiguration.COLLECTION);
        int count =0;
        DBCursor cursor = collection.find(new BasicDBObject("key", new BasicDBObject("$gt", 10)));
        while(cursor.hasNext()) {
            BasicDBObject obj = (BasicDBObject) cursor.next();
            count++;
            Log.info(count + ": " + obj.toString());
        }
        
        Assert.assertEquals(10, count);
        database.rollback();
        database.close();
        Log.info("20 elements found");
    }
}
