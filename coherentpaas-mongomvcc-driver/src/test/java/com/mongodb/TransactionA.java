package com.mongodb;

import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.util.concurrent.BlockingQueue;
import junit.framework.Assert;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public class TransactionA implements Runnable {
    private static final Logger Log = LoggerFactory.getLogger(TransactionA.class);
    
    private BlockingQueue<DBObject> a_BlockingB;
    private BlockingQueue<DBObject> b_BlockingA;
    private MongoClient mongoClient;

    public TransactionA(MongoClient mongoClient, BlockingQueue<DBObject> a_BlockingB, BlockingQueue<DBObject> b_BlockingA) {
        this.mongoClient = mongoClient;
        this.a_BlockingB = a_BlockingB;
        this.b_BlockingA = b_BlockingA;
    }
    
    
    @Override
    public void run() {
        
        try {
            int count = 0;
            DB database = mongoClient.getDB(TestConfiguration.DATABASE);
            database.startTransaction();
            DBCollection collection = database.getCollection(TestConfiguration.COLLECTION);
            DBCursor cursor = collection.find();
            count = readRecords(collection);
            Assert.assertEquals(0, count);
            
            insertRecords(collection);
            count = readRecords(collection);
            Assert.assertEquals(10, count);
            
            unBlockB();
            waitForB();
            
            count = readRecords(collection);
            Assert.assertEquals(10, count);
            commit(database);
            
            database.startTransaction();
            collection = database.getCollection(TestConfiguration.COLLECTION);
            count = readRecords(collection);
            Assert.assertEquals(10, count);
            commit(database);
            
            
            unBlockB();
            waitForB();
            
            database.startTransaction();
            collection = database.getCollection(TestConfiguration.COLLECTION);
            count = readRecords(collection);
            Assert.assertEquals(20, count);
            
            collection = database.getCollection(TestConfiguration.COLLECTION);
            removeAll(collection);
            count = readRecords(collection);
            Assert.assertEquals(0, count);
            
            unBlockB();
            waitForB();
            
            commit(database);
            
            database.startTransaction();
            collection = database.getCollection(TestConfiguration.COLLECTION);
            count = readRecords(collection);
            commit(database);
            
            unBlockB();
            
            database.close();
            
            Log.info("TranscationA is closing");
            
            
            
        } catch (Exception ex) {
            Log.error(ex.getClass().getName() + ". " + ex.getMessage());
            Assert.fail(ex.getMessage());
        } finally {
            Thread.currentThread().interrupt();
        }
    }
    
    private void removeAll(DBCollection collection) {
        collection.remove(new BasicDBObject());
        Log.info("TransactionA removes everything");
    }
    
    private void insertRecords(DBCollection collection) {
        for(int i=0; i<10; i++) {
            collection.insert(new BasicDBObject("key", (i+1))
                    .append("transaction", "A")
                    .append("value", RandomStringUtils.randomAlphabetic(10))
                    );
        }
        Log.info("TransactionA added 10 elements");
    }
    
    
    private int readRecords(DBCollection collection) {
        int count = 0;
        DBCursor cursor = collection.find();
        while(cursor.hasNext()) {
            cursor.next();
            count++;
        }
        
        Log.info("TranscationA read " + count + " elements");
        return count;
    }
    
    private void commit(DB database) throws TransactionManagerException, DataStoreException {
        database.commit();
        Log.info("TransactionA commits");
    }
    
    
    private void waitForB() {
        try {
            Log.info("TranasctionA blocks");
            DBObject obk = this.b_BlockingA.take();
            Log.info("TranasctionA unblocks");
        } catch (InterruptedException ex) {
            Log.error(ex.getClass().getName() + ". " + ex.getMessage());
            Thread.currentThread().interrupt();
        }
    }
    
    private void unBlockB() {
        try {
            Log.info("TranscationA is unblocking B");
            this.a_BlockingB.put(new BasicDBObject());
        } catch (InterruptedException ex) {
            Log.error(ex.getClass().getName() + ". " + ex.getMessage());
            Thread.currentThread().interrupt();
        }
    }

}
