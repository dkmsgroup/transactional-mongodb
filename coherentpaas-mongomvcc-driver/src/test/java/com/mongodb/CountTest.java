package com.mongodb;


import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public class CountTest {
    private static final Logger Log = LoggerFactory.getLogger(CountTest.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection collection = database.createCollection(TestConfiguration.COLLECTION, null);
        for(int i=0; i<20; i++)
            collection.insert(new BasicDBObject("key", (i+1)));
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        Log.info("DB init with 20 elements");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection collection = database.getCollection(TestConfiguration.COLLECTION);
        long count = collection.count();
        Assert.assertEquals(20, count);
        Log.info("20 elements found");
        
        count = collection.count(new BasicDBObject("key", new BasicDBObject("$gt", 18)));
        Assert.assertEquals(2, count);
        Log.info("2 elements found");
        
        count = collection.count(new BasicDBObject("key", new BasicDBObject("$gt", 10)));
        Assert.assertEquals(10, count);
        Log.info("10 elements found");
        
        for(int i=20; i<40; i++) {
            collection.insert(new BasicDBObject("key", (i+1)));
        }
        
        
        count = collection.count();
        Assert.assertEquals(40, count);
        Log.info("40 elements found");
        
        count = collection.count(new BasicDBObject("key", new BasicDBObject("$gt", 18)));
        Assert.assertEquals(22, count);
        Log.info("22 elements found");
        
        
        
        database.rollback();
        
        database.close();
    }
}
