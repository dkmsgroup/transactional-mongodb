package com.mongodb.v2;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.TestConfiguration;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.datastore.DataStoreId;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.util.concurrent.BlockingQueue;
import junit.framework.Assert;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class TransactionBv2 implements Runnable {
    private static final Logger Log = LoggerFactory.getLogger(TransactionBv2.class);
    
    private BlockingQueue<DBObject> a_BlockingB;
    private BlockingQueue<DBObject> b_BlockingA;
    private MongoClient mongoClient;
    
    public TransactionBv2(MongoClient mongoClient, BlockingQueue<DBObject> a_BlockingB, BlockingQueue<DBObject> b_BlockingA) {
        this.mongoClient = mongoClient;
        this.a_BlockingB = a_BlockingB;
        this.b_BlockingA = b_BlockingA;
    }
    
    @Override
    public void run() {
        
        try {
            int count = 0;
            DB database = mongoClient.getDB(TestConfiguration.DATABASE);
            LTMClient.getInstance().getConnection().startTransaction();
            LTMClient.getInstance().getConnection().associate(mongoClient);
            
            DBCollection collection = database.getCollection(TestConfiguration.COLLECTION);
            DBCursor cursor = collection.find();
            count = readRecords(collection);
            Assert.assertEquals(0, count);
            
            waitForA();
            
            count = readRecords(collection);
            Assert.assertEquals(0, count);
            insertRecords(collection);
            count = readRecords(collection);
            Assert.assertEquals(10, count);
            
            unBlockA();
            waitForA();
            
            count = readRecords(collection);
            Assert.assertEquals(10, count);
            commit(database);
            
            LTMClient.getInstance().getConnection().startTransaction();
            LTMClient.getInstance().getConnection().associate(mongoClient);
            
            collection = database.getCollection(TestConfiguration.COLLECTION);
            count = readRecords(collection);
            Assert.assertEquals(20, count);
            commit(database);
            
            LTMClient.getInstance().getConnection().startTransaction();
            LTMClient.getInstance().getConnection().associate(mongoClient);
            
            collection = database.getCollection(TestConfiguration.COLLECTION);
            
            unBlockA();
            waitForA();
            
            count = readRecords(collection);
            Assert.assertEquals(20, count);
            commit(database);
            
            unBlockA();
            waitForA();
            
            LTMClient.getInstance().getConnection().startTransaction();
            LTMClient.getInstance().getConnection().associate(mongoClient);
            
            collection = database.getCollection(TestConfiguration.COLLECTION);
            count = readRecords(collection);
            Assert.assertEquals(0, count);
            
            
            commit(database);
            database.close();
            
            Log.info("TranscationB is closing");
            
        } catch (Exception ex) {
            Log.error(ex.getClass().getName() + ". " + ex.getMessage());
            Assert.fail(ex.getMessage());
        } finally {
            Thread.currentThread().interrupt();
        }
    }
    
    private void removeAll(DBCollection collection) {
        collection.remove(new BasicDBObject());
        Log.info("TransactionB removes everything");
    }
    
    private void insertRecords(DBCollection collection) {
        for(int i=0; i<10; i++) {
            collection.insert(new BasicDBObject("key", (i+1))
                    .append("transaction", "B")
                    .append("value", RandomStringUtils.randomAlphabetic(10))
                    );
        }
        Log.info("TransactionB added 10 elements");
    }
    
    
    private int readRecords(DBCollection collection) {
        int count = 0;
        DBCursor cursor = collection.find();
        while(cursor.hasNext()) {
            cursor.next();
            count++;
        }
        
        Log.info("TranscationB read " + count + " elements");
        return count;
    }
    
    private void commit(DB database) throws TransactionManagerException, DataStoreException {
        LTMClient.getInstance().getConnection().commit();
        Log.info("TransactionB commits");
    }
    
    
    private void waitForA() {
        try {
            Log.info("TranasctionB blocks");
            DBObject obk = this.a_BlockingB.take();
            Log.info("TranasctionB unblocks");
        } catch (InterruptedException ex) {
            Log.error(ex.getClass().getName() + ". " + ex.getMessage());
            Thread.currentThread().interrupt();
        }
    }
    
    private void unBlockA() {
        try {
            Log.info("TranscationB is unblocking A");
            this.b_BlockingA.put(new BasicDBObject());
        } catch (InterruptedException ex) {
            Log.error(ex.getClass().getName() + ". " + ex.getMessage());
            Thread.currentThread().interrupt();
        }
    }

}
