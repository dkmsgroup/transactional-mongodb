package com.mongodb;

import com.leanxcale.tests.minicluster.TMMiniCluster;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import org.junit.AfterClass;
import junit.framework.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class AggregateTest {
    private static final Logger Log = LoggerFactory.getLogger(AggregateTest.class);
    
    public static void main(String [] args) {
        TMMiniCluster.startMiniCluster(true, 0);
        
        try {
            AggregateTest.initDB();
            AggregateTest test = new AggregateTest();
            test.test();
            AggregateTest.dropDb();
            
            com.mongodb.autocommit.CountTest.initDB();
            com.mongodb.autocommit.CountTest testasd = new com.mongodb.autocommit.CountTest();
            testasd.test();
            com.mongodb.autocommit.CountTest.dropDb();
            
        } catch(Exception ex) {
            Log.error(ex.getMessage());
        }
    }
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
       
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection coll = database.createCollection(TestConfiguration.COLLECTION, null);
        
        coll.insert(new BasicDBObjectBuilder()
               .add("employee", 1)
               .add("department", "Sales")
               .add("amount", 71)
               .add("type", "airfare")
               .get());
        coll.insert(new BasicDBObjectBuilder()
               .add("employee", 2)
               .add("department", "Engineering")
               .add("amount", 15)
               .add("type", "airfare")
               .get());
        coll.insert(new BasicDBObjectBuilder()
               .add("employee", 4)
               .add("department", "Human Resources")
               .add("amount", 5)
               .add("type", "airfare")
               .get());
        coll.insert(new BasicDBObjectBuilder()
               .add("employee", 42)
               .add("department", "Sales")
               .add("amount", 77)
               .add("type", "airfare")
               .get());
        
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        
        Log.info("DB init with 4 elements to start aggregation test");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
    }
    
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection col = database.getCollection(TestConfiguration.COLLECTION);
        
        // create our pipeline operations, first with the $match
        DBObject match = new BasicDBObject("$match", new BasicDBObject("type", "airfare"));
        
        // build the $projection operation
        DBObject fields = new BasicDBObject("department", 1);
        fields.put("amount", 1);
        fields.put("_id", 0);
        DBObject project = new BasicDBObject("$project", fields );

        // Now the $group operation
        DBObject groupFields = new BasicDBObject( "_id", "$department");
        groupFields.put("average", new BasicDBObject( "$avg", "$amount"));
        DBObject group = new BasicDBObject("$group", groupFields);

        // Finally the $sort operation
        DBObject sort = new BasicDBObject("$sort", new BasicDBObject("average", -1));

        // run aggregation
        List<DBObject> pipeline = Arrays.asList(match, project, group, sort);
        
        AggregationOutput output = col.aggregate(pipeline);
        
        for(DBObject res : output.results()) {
            
            String key = (String) res.get("_id");
            if(key.equals("Sales"))
                Assert.assertEquals((double)74.0, ((Double) res.get("average")));
            if(key.equals("Engineering"))
                Assert.assertEquals((double)15.0, ((Double) res.get("average")));
            if(key.equals("Human Resources"))
                Assert.assertEquals((double)5.0, ((Double) res.get("average")));
        }
        
        database.commit();
    }
}
