package com.mongodb;

import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class InsertRollbackTest {
    private static final Logger Log = LoggerFactory.getLogger(InsertRollbackTest.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection collection = database.createCollection(TestConfiguration.COLLECTION, null);
        
        collection.insert(new BasicDBObject("keyTest", "keyValue"));
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        Log.info("DB init");
        database.close();
    }
    
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        
        
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
       
    }
    
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection collection = database.createCollection(TestConfiguration.COLLECTION, null);
        
        BasicDBObject testingObj = new BasicDBObject("testing", "this value");
        collection.insert(testingObj);
        
        int count = 0;
        DBCursor cursor = collection.find();
        while(cursor.hasNext()) {
            cursor.next();
            count++;
        }
        
        Assert.assertEquals(count, 2);
        Log.info("two elements found before commit.");
        
        database.rollback();
        
        
        database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        collection = database.getCollection(TestConfiguration.COLLECTION);
        
        count = 0;
        cursor = collection.find();
        while(cursor.hasNext()) {
            BasicDBObject result = (BasicDBObject) cursor.next();
            count++;
        }
        
        Assert.assertEquals(1, count);
        Log.info("element not found after commit.");
        
       
        
        database.commit();
        database.close();
    }
}