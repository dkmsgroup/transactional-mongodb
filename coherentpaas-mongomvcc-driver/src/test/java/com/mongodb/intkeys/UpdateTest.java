package com.mongodb.intkeys;

import com.leanxcale.tests.minicluster.TMMiniCluster;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class UpdateTest {
    private static final Logger Log = LoggerFactory.getLogger(UpdateTest.class);
    
    public static void main(String [] args) {
        try {
            TMMiniCluster.startMiniCluster(true, 0);
            UpdateTest.initDB();
            UpdateTest test = new UpdateTest();
            test.test();
            UpdateTest.dropDb();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
                
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        try (DB database = mongoClient.getDB(TestConfiguration.DATABASE)) {
            database.startTransaction();
            DBCollection collection = database.createCollection(TestConfiguration.COLLECTION, null);
            collection.insert(new BasicDBObject("key", "value: " + 5).append(MongoMVCCFields._ID, 5));
            collection.insert(new BasicDBObject("key", "value: " + 6).append(MongoMVCCFields._ID, 6));
            collection.insert(new BasicDBObject("key", "value: " + 7).append(MongoMVCCFields._ID, 7));
            database.commit();
            
            database.startTransaction();
            database.commit();
            
            Log.info("DB init with 20 elements");
        }
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection collection = database.createCollection(TestConfiguration.COLLECTION, null);
        DBCursor cursor = collection.find();
        int count = 0;
        while(cursor.hasNext()) {
            count++;
            cursor.next();
        }
        
        Assert.assertEquals(3, count);
        
        
        collection.update(new BasicDBObject(MongoMVCCFields._ID, 5), new BasicDBObject("$set", new BasicDBObject("key", "yolo")));
        cursor = collection.find(new BasicDBObject(MongoMVCCFields._ID, 5));
        if(cursor.hasNext()) {
            DBObject dObject = cursor.next();
            String key = (String) dObject.get("key");
            Assert.assertEquals("yolo", key);
        } else {
            Assert.fail("error retrieving document with id 5");
        }
        
        
        
        
        database.commit();
        database.startTransaction();
                
                
        collection.update(new BasicDBObject(MongoMVCCFields._ID, 6), new BasicDBObject("$set", new BasicDBObject("key", "yolo")));
        database.rollback();
        database.startTransaction();
        
        cursor = collection.find(new BasicDBObject(MongoMVCCFields._ID, 6));
        if(cursor.hasNext()) {
            DBObject dObject = cursor.next();
            String key = (String) dObject.get("key");
            Assert.assertEquals("value: " + 6, key);
        } else {
            Assert.fail("error retrieving document with id 6");
        }
                
        database.commit();
        database.close();
        
    }
    
}
