package com.mongodb.intkeys;

import com.leanxcale.tests.minicluster.TMMiniCluster;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class DeleteTest {
    private static final Logger Log = LoggerFactory.getLogger(DeleteTest.class);
    
    public static void main(String [] args) {
        com.leanxcale.tests.minicluster.TMMiniCluster.startMiniCluster(true, 0);
        
        try {
           
            DeleteTest.initDB();
            DeleteTest testasd = new DeleteTest();
            testasd.test();
            DeleteTest.dropDb();
            
        } catch(Exception ex) {
            Log.error(ex.getMessage());
        }
    }
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
                
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        try (DB database = mongoClient.getDB(TestConfiguration.DATABASE)) {
            database.startTransaction();
            DBCollection collection = database.createCollection(TestConfiguration.COLLECTION, null);
            for(int i=0; i<20; i++)
                collection.insert(new BasicDBObject("key", (i+1)).append(MongoMVCCFields._ID, (i+1)));
            database.commit();
            
            database.startTransaction();
            database.commit();
            
            Log.info("DB init with 20 elements");
        }
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection collection = database.createCollection(TestConfiguration.COLLECTION, null);
        DBCursor cursor = collection.find(new BasicDBObject(MongoMVCCFields._ID, new BasicDBObject("$lte", 5)));
        int count = 0;
        while(cursor.hasNext()) {
            count++;
            cursor.next();
        }
        
        Assert.assertEquals(5, count);
        
        collection.remove(new BasicDBObject(MongoMVCCFields._ID, 18));
        
        cursor = collection.find();
        count = 0;
        while(cursor.hasNext()) {
            count++;
            cursor.next();
        }
        
        Assert.assertEquals(19, count);
        
        database.rollback();
        database.startTransaction();
        
        
        cursor = collection.find();
        count = 0;
        while(cursor.hasNext()) {
            count++;
            cursor.next();
        }
        
        Assert.assertEquals(20, count);
        
        
        collection.remove(new BasicDBObject(MongoMVCCFields._ID, new BasicDBObject("$lte", 3)));
        
        cursor = collection.find(new BasicDBObject(MongoMVCCFields._ID, new BasicDBObject("$lte", 5)));
        count = 0;
        while(cursor.hasNext()) {
            count++;
            cursor.next();
        }
        
        Assert.assertEquals(2, count);
        
        database.commit();
        database.startTransaction();
        database.commit();
        database.close();
        
    }
}
