package com.mongodb.intkeys;

import com.leanxcale.tests.minicluster.TMMiniCluster;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class InsertCommitTest {
    private static final Logger Log = LoggerFactory.getLogger(InsertCommitTest.class);
    
    
    
    @Test
    public void testIntegerKeys() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection collection = database.createCollection(TestConfiguration.COLLECTION, null);
        for(int i=1; i<11 ; i++) {
            BasicDBObject dBObject = new BasicDBObject("keyTest", "keyValue" + i)
                    .append("_id", i);
            collection.insert(dBObject);
        }
        Log.info("Added {} documents", 10);
        
        DBCursor cursor = collection.find(new BasicDBObject("_id", new BasicDBObject("$lte", 5)));
        int count = 0;
        while(cursor.hasNext()) {
            Log.info("{}: {}", ++count, (BasicDBObject) cursor.next());
            
        }
        
        Assert.assertEquals(5, count);
        
        
        database.commit();
        database.startTransaction();
        database.commit();
        
        database.startTransaction();
        
        collection = database.createCollection(TestConfiguration.COLLECTION, null);
        cursor = collection.find(new BasicDBObject("_id", new BasicDBObject("$lte", 5)));
        count = 0;
        while(cursor.hasNext()) {
            Log.info("{}: {}", ++count, (BasicDBObject) cursor.next());
        }
        
        Assert.assertEquals(5, count);
        
        database.commit();
        database.close();
        
    }
    
    @Test
    public void testIntegerKeysRollback() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection collection = database.createCollection(TestConfiguration.COLLECTION, null);
        for(int i=1; i<11 ; i++) {
            BasicDBObject dBObject = new BasicDBObject("keyTest", "keyValue" + i)
                    .append("_id", i);
            collection.insert(dBObject);
        }
        Log.info("Added {} documents", 10);
        
        DBCursor cursor = collection.find(new BasicDBObject("_id", new BasicDBObject("$lte", 5)));
        int count = 0;
        while(cursor.hasNext()) {
            Log.info("{}: {}", ++count, (BasicDBObject) cursor.next());
            
        }
        
        Assert.assertEquals(5, count);
        
        
        database.rollback();
        database.startTransaction();
        database.commit();
        
        database.startTransaction();
        
        cursor = collection.find(new BasicDBObject("_id", new BasicDBObject("$lte", 5)));
        count = 0;
        while(cursor.hasNext()) {
            Log.info("{}: {}", ++count, (BasicDBObject) cursor.next());
        }
        
        Assert.assertEquals(0, count);
        
        database.commit();
        database.close();
        
    }
}
