package com.mongodb;

import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class DeleteQuery2Test {
    private static final Logger Log = LoggerFactory.getLogger(DeleteQuery2Test.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection collection = database.createCollection(TestConfiguration.COLLECTION, null);
        for(int i=0; i<20; i++)
            collection.insert(new BasicDBObject("key", (i+1)));
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        Log.info("DB init with 20 elements");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection collection = database.getCollection(TestConfiguration.COLLECTION);
        int count = 0;
        BasicDBObject obj = null;
        DBCursor cursor = collection.find();
        while(cursor.hasNext()) {
            obj = (BasicDBObject) cursor.next();
            count++;
        }
        Assert.assertEquals(20, count);
        Log.info("found 20 elements");
        
        BasicDBObject query = new BasicDBObject("key", 3);
        collection.remove(query);
        
                
        cursor = collection.find();
        count = 0;
        while(cursor.hasNext()) {
            count++;
            cursor.next();
        }
        Assert.assertEquals(19, count);
        Log.info("found 19 element before commit");
        
        database.commit();
        
        
        database.startTransaction();
        collection = database.getCollection(TestConfiguration.COLLECTION);
        count = 0;
        cursor = collection.find();
        while(cursor.hasNext()) {
            cursor.next();
            count++;
        }
        Assert.assertEquals(19, count);
        Log.info("found 19 element after commit");
        
        database.rollback();
        database.close();
    }
}