package com.mongodb;


import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.transactionmanager.minicluster.TMMiniCluster;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class BulkOperationTest {
    private static final org.slf4j.Logger Log = LoggerFactory.getLogger(BulkOperationTest.class);
    
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection collection = database.createCollection(TestConfiguration.COLLECTION, null);
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        Log.info("DB init with 20 elements");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection col = database.getCollection(TestConfiguration.COLLECTION);
        
        BulkWriteOperation builder = col.initializeOrderedBulkOperation();
        builder.insert(new BasicDBObject("key", 1));
        builder.insert(new BasicDBObject("key", 2));
        builder.insert(new BasicDBObject("key", 3));

        builder.find(new BasicDBObject("key", 1)).updateOne(new BasicDBObject("$set", new BasicDBObject("x", 2)));
        builder.find(new BasicDBObject("key", 2)).removeOne();
        builder.find(new BasicDBObject("key", 3)).replaceOne(new BasicDBObject("key", 3).append("x", 4));

        com.mongodb.BulkWriteResult result = builder.execute();
        
        database.commit();
        database.startTransaction();
        col = database.getCollection(TestConfiguration.COLLECTION);
        DBCursor cursor = col.find();
        int count = 0;
        while(cursor.hasNext()) {
            count++;
            BasicDBObject obj = (BasicDBObject) cursor.next();
            if(obj.containsField("key")) {
                int key = obj.getInt("key");
                switch(key) {
                    
                    case 1:
                        int value = obj.getInt("x");
                        Assert.assertEquals(2, value);
                        Log.info("key 1 has x value " + value);
                        break;
                    case 3:
                        value = obj.getInt("x");
                        Assert.assertEquals(4, value);
                        Log.info("key 3 has x value " + value);
                        break;
                    default:
                        break;                        
                }
            }
        }
        Assert.assertEquals(2, count);
        Log.info("2 elements found after bulk operations");
        
        database.commit();
        database.close();
    }
    
}
