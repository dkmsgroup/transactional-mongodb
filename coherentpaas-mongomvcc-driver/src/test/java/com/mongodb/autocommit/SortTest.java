package com.mongodb.autocommit;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class SortTest {
    private static final Logger Log = LoggerFactory.getLogger(com.mongodb.SortTest.class);
    private static final String FIELD = "numbering";
    private static final String FIELD2 = "numbering2";
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        populate();
    }
    
    
     private static void populate() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        DBCollection col = database.getCollection(TestConfiguration.COLLECTION);
        List<DBObject> list = new ArrayList<>();
        for(int i=0; i<100; i++) 
            list.add(new BasicDBObject("key", (i+1))
                                    .append(FIELD2, ((i+1)%10))
                                    .append(FIELD, Integer.valueOf(RandomStringUtils.randomNumeric(3)))
                                    .append("texting", RandomStringUtils.randomAlphabetic(50)));
        
        col.insert(list); 
        
        database.close();
    }
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        
        DBCollection col = database.getCollection(TestConfiguration.COLLECTION);
        
        
        
        BasicDBObject sortObject = new BasicDBObject(FIELD2, -1)
                                            .append(FIELD, 1);
        int count = 0;
        DBCursor cursor = col.find();
        cursor.sort(sortObject);
        int number = Integer.MAX_VALUE;
        while(cursor.hasNext()) {
            BasicDBObject obj = (BasicDBObject)cursor.next();
            int cuurentNumber = obj.getInt(FIELD2);
            Assert.assertTrue((number>=cuurentNumber));       
            count++;
        }
        
        Assert.assertEquals(100, count);
        Log.info("total 100 records found, all sorted by the corresponding field");
        
        
        database.close();
    }
}
