package com.mongodb.autocommit;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class DeleteQueryTest {
    private static final Logger Log = LoggerFactory.getLogger(com.mongodb.DeleteQueryTest.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
                
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        DBCollection collection = database.createCollection(TestConfiguration.COLLECTION, null);
        for(int i=0; i<20; i++)
            collection.insert(new BasicDBObject("key", (i+1)));
       
        Log.info("DB init with 20 elements");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    
    
    
    @Test
    public void testScan() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        
        DBCollection collection = database.getCollection(TestConfiguration.COLLECTION);
        int count = 0;
        BasicDBObject obj = null;
        DBCursor cursor = collection.find();
        while(cursor.hasNext()) {
            obj = (BasicDBObject) cursor.next();
            count++;
        }
        Assert.assertEquals(20, count);
        Log.info("found 20 elements");
        
        BasicDBObject query = new BasicDBObject("key", new BasicDBObject("$gt", 15));
        collection.remove(query);
        
                
        cursor = collection.find();
        count = 0;
        while(cursor.hasNext()) {
            count++;
            cursor.next();
        }
        Assert.assertEquals(15, count);
        Log.info("found 15 element before commit");
        
      
        collection = database.getCollection(TestConfiguration.COLLECTION);
        count = 0;
        cursor = collection.find();
        while(cursor.hasNext()) {
            cursor.next();
            count++;
        }
        Assert.assertEquals(15, count);
        Log.info("found 15 element after commit");
        
       
    }
    
}
