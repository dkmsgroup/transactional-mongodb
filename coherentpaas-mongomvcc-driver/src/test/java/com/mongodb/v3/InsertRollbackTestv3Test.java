package com.mongodb.v3;


import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.io.IOException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.bson.Document;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class InsertRollbackTestv3Test {
    private static final Logger Log = LoggerFactory.getLogger(InsertRollbackTestv3Test.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        MongoDatabase mongoDatabase = mongoClient.getDatabase(TestConfiguration.DATABASE);
        mongoDatabase.startTransaction();
        mongoDatabase.createCollection(TestConfiguration.COLLECTION);
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection(TestConfiguration.COLLECTION);
        Document doc = new Document("keyTest", "keyValue");
        mongoCollection.insertOne(doc);
        mongoDatabase.commit();
        mongoDatabase.startTransaction();
        mongoDatabase.commit();
        Log.info("DB init");
        mongoDatabase.close();
    }
    
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
    }
    
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        MongoCollection<Document> collection = database.getCollection(TestConfiguration.COLLECTION);
        Document testingObj = new Document("testing", "this value");
        collection.insertOne(testingObj);
        
        int count = 0;
        MongoCursor<Document> cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            cursor.next();
            count++;
        }
        
        Assert.assertEquals(2, count);
        Log.info("two elements found before commit.");
        
        database.rollback();
        
        
        database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        collection = database.getCollection(TestConfiguration.COLLECTION);
        
        count = 0;
        cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            Document result = cursor.next();
            count++;
        }
        
        Assert.assertEquals(1, count);
        Log.info("element not found after commit.");
        
       
        
        database.commit();
        database.close();
    }
}
