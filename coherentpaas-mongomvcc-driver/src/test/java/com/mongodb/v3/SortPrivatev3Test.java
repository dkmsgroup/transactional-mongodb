package com.mongodb.v3;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.io.IOException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.bson.Document;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class SortPrivatev3Test {
    private static final Logger Log = LoggerFactory.getLogger(SortPrivatev3Test.class);
    private static final String FIELD = "numbering";
    private static final String FIELD2 = "numbering2";
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        database.createCollection(TestConfiguration.COLLECTION);
        MongoCollection<Document> collection = database.getCollection(TestConfiguration.COLLECTION);
        
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        Log.info("DB init");
        database.close();
        
    }
    
    
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    //@Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        MongoCollection<Document> col = database.getCollection(TestConfiguration.COLLECTION);
        
        int keyNumber = 0;
        col.insertOne(new Document("key", ++keyNumber)
                            .append(FIELD, 1));
        col.insertOne(new Document("key", ++keyNumber)
                            .append(FIELD, 2));
        col.insertOne(new Document("key", ++keyNumber)
                            .append(FIELD, 3));
        col.insertOne(new Document("key", ++keyNumber)
                            .append(FIELD, 4));
        col.insertOne(new Document("key", ++keyNumber)
                            .append(FIELD, 5));
        database.commit();
        
        database.startTransaction();
        col = database.getCollection(TestConfiguration.COLLECTION);
        Document sort = new Document(FIELD, 1);
        MongoCursor<Document> cursor = col.find().sort(sort).iterator();
        int number = Integer.MIN_VALUE;
        while(cursor.hasNext()) {
            Document object = cursor.next();
            Assert.assertTrue((number<=object.getInteger(FIELD)));
        }
        cursor.close();
        
        
        
        //col.update(new BasicDBObject("key", 3), new BasicDBObject("$set", new BasicDBObject(FIELD, 10)));
        
      
        
        cursor = col.find().sort(sort).iterator();
        number = Integer.MIN_VALUE;
        while(cursor.hasNext()) {
            Document object =  cursor.next();
            Assert.assertTrue((number<=object.getInteger(FIELD)));
        }
        cursor.close();
        
        database.commit();
        
        database.startTransaction();
        database.commit();
                
    }    
}
