package com.mongodb.v3.intkeys;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.bson.Document;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class UpdateTest {
    private static final Logger Log = LoggerFactory.getLogger(UpdateTest.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
                
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        MongoCollection collection = database.getCollection(TestConfiguration.COLLECTION, Document.class);
        collection.insertOne(new Document("key", "value: " + 5).append(MongoMVCCFields._ID, 5));
        collection.insertOne(new Document("key", "value: " + 6).append(MongoMVCCFields._ID, 6));
        collection.insertOne(new Document("key", "value: " + 7).append(MongoMVCCFields._ID, 7));
        database.commit();

        database.startTransaction();
        database.commit();

        Log.info("DB init with 20 elements");
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        MongoCollection collection = database.getCollection(TestConfiguration.COLLECTION, Document.class);
        MongoCursor<Document> cursor = collection.find().iterator();
        int count = 0;
        while(cursor.hasNext()) {
            count++;
            cursor.next();
        }
        
        Assert.assertEquals(3, count);
        
        
        collection.updateOne(new Document(MongoMVCCFields._ID, 5), new Document("$set", new Document("key", "yolo")));
        cursor = collection.find(new Document(MongoMVCCFields._ID, 5)).iterator();
        if(cursor.hasNext()) {
            Document dObject = cursor.next(); 
            String key = (String)  dObject.get("key");
            Assert.assertEquals("yolo", key);
        } else {
            Assert.fail("error retrieving document with id 5");
        }
        
        
        
        
        database.commit();
        database.startTransaction();
                
                
        collection.updateOne(new Document(MongoMVCCFields._ID, 6), new Document("$set", new Document("key", "yolo")));
        database.rollback();
        database.startTransaction();
        
        cursor = collection.find(new Document(MongoMVCCFields._ID, 6)).iterator();
        if(cursor.hasNext()) {
            Document dObject = cursor.next();
            String key = (String) dObject.get("key");
            Assert.assertEquals("value: " + 6, key);
        } else {
            Assert.fail("error retrieving document with id 6");
        }
                
        database.commit();        
    }
}
