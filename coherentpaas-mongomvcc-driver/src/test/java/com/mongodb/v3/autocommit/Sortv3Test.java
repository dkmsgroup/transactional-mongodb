package com.mongodb.v3.autocommit;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.apache.commons.lang3.RandomStringUtils;
import org.bson.Document;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class Sortv3Test {
    private static final Logger Log = LoggerFactory.getLogger(com.mongodb.v3.Sortv3Test.class);
    private static final String FIELD = "numbering";
    private static final String FIELD2 = "numbering2";
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        populate();
    }
    
    
     private static void populate() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);

        database.createCollection(TestConfiguration.COLLECTION);
        MongoCollection<Document> col = database.getCollection(TestConfiguration.COLLECTION);
        List<Document> list = new ArrayList<>();
        for(int i=0; i<100; i++) 
            list.add(new Document("key", (i+1))
                                    .append(FIELD2, ((i+1)%10))
                                    .append(FIELD, Integer.valueOf(RandomStringUtils.randomNumeric(3)))
                                    .append("texting", RandomStringUtils.randomAlphabetic(50)));
        
        col.insertMany(list); 
        
        database.close();
    }
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        
        MongoCollection<Document> col = database.getCollection(TestConfiguration.COLLECTION);
        
        
        
        Document sortObject = new Document(FIELD2, -1)
                                            .append(FIELD, 1);
        int count = 0;
        MongoCursor<Document> cursor = col.find().sort(sortObject).iterator();
        int number = Integer.MAX_VALUE;
        while(cursor.hasNext()) {
            Document obj = cursor.next();
            int cuurentNumber = obj.getInteger(FIELD2);
            Assert.assertTrue((number>=cuurentNumber));       
            count++;
        }
        
        Assert.assertEquals(100, count);
        Log.info("total 100 records found, all sorted by the corresponding field");
        
        database.close();
    }
}
