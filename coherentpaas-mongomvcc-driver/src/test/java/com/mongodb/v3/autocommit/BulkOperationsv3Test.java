package com.mongodb.v3.autocommit;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.BulkWriteOptions;
import com.mongodb.client.model.DeleteManyModel;
import com.mongodb.client.model.DeleteOneModel;
import com.mongodb.client.model.InsertOneModel;
import com.mongodb.client.model.ReplaceOneModel;
import com.mongodb.client.model.UpdateManyModel;
import com.mongodb.client.model.UpdateOneModel;
import com.mongodb.client.model.WriteModel;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class BulkOperationsv3Test {
    private static final Logger Log = LoggerFactory.getLogger(com.mongodb.v3.autocommit.BulkOperationsv3Test.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.createCollection(TestConfiguration.COLLECTION);
                
        Log.info("DB init with elements");
        database.close();
    }
    
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        
        MongoCollection<Document> collection = database.getCollection(TestConfiguration.COLLECTION);
        
        
        List<WriteModel<Document>> list = new ArrayList<>();
        for(int i=0; i<100; i++) {
            
            WriteModel writeModel = new InsertOneModel<>(new Document("key", (i+1))
                                                                    .append("status", "bulk"));
            
            list.add(writeModel);
        }
        
        BulkWriteOptions options = new BulkWriteOptions();
        BulkWriteResult res = collection.bulkWrite(list);
        
        Assert.assertEquals(100, res.getInsertedCount());
        Log.info("100 elements added");
        
        
        long resultCount = collection.count();
        Assert.assertEquals(100, resultCount);
        Log.info("100 elements inserted");
        
        
        collection = database.getCollection(TestConfiguration.COLLECTION);
        
        list = new ArrayList<>();
        WriteModel<Document> updateWriteModel = new UpdateOneModel<>(
                          new Document("key", new Document("$lte", 5)),
                          new Document("$set", new Document("status", "update")));
        
        list.add(updateWriteModel);
        
        res = collection.bulkWrite(list);
        Assert.assertEquals(0, res.getInsertedCount());
        Assert.assertEquals(1, res.getModifiedCount());
        Log.info("1 element updated");
        
        resultCount = collection.count(new Document("status", "update"));
        Assert.assertEquals(1, resultCount);
        Log.info("1 element updated");
        
        collection = database.getCollection(TestConfiguration.COLLECTION);
        
        
        list = new ArrayList<>();
        updateWriteModel = new UpdateManyModel<>(
                          new Document("key", new Document("$gte", 90)),
                          new Document("$set", new Document("status", "update")));
        
        list.add(updateWriteModel);        
        res = collection.bulkWrite(list);
        Assert.assertEquals(0, res.getInsertedCount());
        Assert.assertEquals(11, res.getModifiedCount());
        Log.info("11 element updated");
        
        collection = database.getCollection(TestConfiguration.COLLECTION);
        resultCount = collection.count(new Document("status", "update"));
        Assert.assertEquals(12, resultCount);
        Log.info("12 element updated so far");
        
        
        list = new ArrayList<>();
        WriteModel<Document> deleteWriteModel = new DeleteManyModel<>(new Document("key", new Document("$lte", 10)));
        list.add(deleteWriteModel);
        res = collection.bulkWrite(list);
        Assert.assertEquals(10, res.getDeletedCount());
        Log.info("10 elements deleted");
        
        resultCount = collection.count();
        Assert.assertEquals(90, resultCount);
        Log.info("90 elements now exist");
        
        collection = database.getCollection(TestConfiguration.COLLECTION);
        resultCount = collection.count();
        Assert.assertEquals(90, resultCount);
        Log.info("90 elements now exists after commit");
        
        list = new ArrayList<>();
        deleteWriteModel = new DeleteOneModel<>(new Document("key", new Document("$gte", 50)));
        list.add(deleteWriteModel);
        res = collection.bulkWrite(list);
        Assert.assertEquals(1, res.getDeletedCount());
        Log.info("1 element deleted");
        
        resultCount = collection.count();
        Assert.assertEquals(89, resultCount);
        Log.info("89 elements now exist");
        
        collection = database.getCollection(TestConfiguration.COLLECTION);
        resultCount = collection.count();
        Assert.assertEquals(89, resultCount);
        Log.info("89 elements now exists after commit");
        
        
        list = new ArrayList<>();
        updateWriteModel = new ReplaceOneModel<>(
                          new Document("key", new Document("$gte", 90)),
                          new Document("key", "YOLO"));
        list.add(updateWriteModel);
        res = collection.bulkWrite(list);
        Assert.assertEquals(1, res.getModifiedCount());
        Log.info("1 element replaced");
        
        resultCount = collection.count(new Document("key", "YOLO"));
        Assert.assertEquals(1, resultCount);
        Log.info("1 element with key named YOLO");
        
        
        database.close();
    }
}
