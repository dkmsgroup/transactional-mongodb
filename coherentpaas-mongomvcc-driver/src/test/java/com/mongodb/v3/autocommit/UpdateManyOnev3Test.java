package com.mongodb.v3.autocommit;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.UpdateResult;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.bson.Document;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class UpdateManyOnev3Test {
    private static final Logger Log = LoggerFactory.getLogger(com.mongodb.v3.UpdateManyOnev3Test.class);
    
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.createCollection(TestConfiguration.COLLECTION);
        MongoCollection<Document> collection = database.getCollection(TestConfiguration.COLLECTION);
        
        
        List<Document> list = new ArrayList<>();
        for(int i=0; i<20; i++)
            list.add(new Document("key", (i+1))
                            .append("attr", "init"));
        collection.insertMany(list);
        
        
        Log.info("DB init with 20 elements");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");        
    }
    
    @Test
    public void test() throws TransactionManagerException, UnknownHostException, DataStoreException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        
        MongoCollection<Document> collection = database.getCollection(TestConfiguration.COLLECTION);
        Log.info("got collection named: " + collection.getNamespace());
        MongoCursor<Document> cursor = collection.find().iterator();
        Document obj = null;
        int count = 0;
        while(cursor.hasNext()) {
            obj = cursor.next();
            if(!obj.getString("attr").equals("init"))
                Assert.fail("attribute is not set to init");
            count++;
        }
        Assert.assertEquals(20, count);
        Log.info("20 elements found");
        
        Document query = new Document("key", new Document("$lte", 3));
        Document set = new Document("$set", new Document("attr", "1st"));
        
        UpdateResult result = collection.updateMany(query, set);
        Assert.assertEquals(3, result.getModifiedCount());
        
        count=0;
        cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            if(obj.getString("attr").equals("1st"))
                count++;            
        }
        Assert.assertEquals(3, count);
        Log.info("3 elements found updated");
        
        collection = database.getCollection(TestConfiguration.COLLECTION);
        
        
        count=0;
        cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            if(obj.getString("attr").equals("1st"))
                count++;            
        }
        Assert.assertEquals(3, count);
        Log.info("3 elements found updated");
        
        query = new Document("key", new Document("$gte", 15));
        result = collection.updateOne(query, set);
        Assert.assertEquals(1, result.getModifiedCount());
        
        count=0;
        cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            if(obj.getString("attr").equals("1st"))
                count++;            
        }
        Assert.assertEquals(4, count);
        Log.info("4 elements found updated");
        
        collection = database.getCollection(TestConfiguration.COLLECTION);
        count=0;
        cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            if(obj.getString("attr").equals("1st"))
                count++;            
        }
        Assert.assertEquals(4, count);
        Log.info("4 elements found updated");
        
        database.close();        
    }
}
