package com.mongodb.v3;

import com.mongodb.DeleteTest;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.FindOneAndDeleteOptions;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.bson.Document;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class FindOneAndDeleteQueryv3Test {
    private static final Logger Log = LoggerFactory.getLogger(DeleteTest.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        database.createCollection(TestConfiguration.COLLECTION);
        MongoCollection<Document> collection = database.getCollection(TestConfiguration.COLLECTION);
        
        List<Document> list = new ArrayList<>();
        for(int i=0; i<20; i++)
            list.add(new Document("key", (i+1)));
        
        collection.insertMany(list);
        
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        Log.info("DB init");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");        
    }
    
    @Test
    public void test() throws TransactionManagerException, UnknownHostException, DataStoreException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        MongoCollection<Document> collection = database.getCollection(TestConfiguration.COLLECTION);
        int count = 0;
        Document obj = null;
        MongoCursor<Document> cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            count++;
        }
        Assert.assertEquals(20, count);
        Log.info("found 20 element");
        
        Document query = new Document("key", new Document("$lte", 3));
        Document sort = new Document("key", 1);
        cursor = collection.find(query).sort(sort).iterator();
        Document firstResult = null;
        count=0;
        while(cursor.hasNext()) {
            if(firstResult==null)
                firstResult = cursor.next();
            else
                cursor.next();
            
            count++;
        }
        Assert.assertEquals(3, count);
        Log.info("found 3 element");
        
        Assert.assertNotNull(firstResult);
        Log.info("first result is: " + firstResult.toJson());
        
        FindOneAndDeleteOptions options = new FindOneAndDeleteOptions();
        options.sort(sort);
        Document deletedObject = collection.findOneAndDelete(query, options);
        Assert.assertNotNull(deletedObject);
        Log.info("Returned one mathed document that was deleted: " + deletedObject.toJson());
        
        Assert.assertEquals(deletedObject, firstResult);
        Log.info("The returned matched document equals the one to be deleted");
        
        count = 0;
        cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            count++;
        }
        Assert.assertEquals(19, count);
        Log.info("19 documents found after deletion");
        
        database.rollback();
        
        
        database.startTransaction();
        collection = database.getCollection(TestConfiguration.COLLECTION);
        count=0;
        cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            count++;
        }
        
        Assert.assertEquals(20, count);
        Log.info("20 documents found after deletion");
        
        
        query = new Document("key", new Document("$lte", 3));
        sort = new Document("key", -1);
        cursor = collection.find(query).sort(sort).iterator();
        firstResult = null;
        count=0;
        while(cursor.hasNext()) {
            if(firstResult==null)
                firstResult = cursor.next();
            else
                cursor.next();
            
            count++;
        }
        Assert.assertEquals(3, count);
        Log.info("found 3 element");
        
        Assert.assertNotNull(firstResult);
        Log.info("first result is: " + firstResult.toJson());
        
        options = new FindOneAndDeleteOptions();
        options.sort(sort);
        deletedObject = collection.findOneAndDelete(query, options);
        Assert.assertNotNull(deletedObject);
        Log.info("Returned one mathed document that was deleted: " + deletedObject.toJson());
        
        Assert.assertEquals(deletedObject, firstResult);
        Log.info("The returned matched document equals the one to be deleted");
        
        count = 0;
        cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            count++;
        }
        Assert.assertEquals(19, count);
        Log.info("19 documents found after deletion");
        
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        database.close();
    }
}
