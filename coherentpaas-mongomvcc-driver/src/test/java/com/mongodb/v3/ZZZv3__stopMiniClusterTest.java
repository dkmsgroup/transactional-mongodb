package com.mongodb.v3;

import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import com.leanxcale.tests.minicluster.TMMiniCluster;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class ZZZv3__stopMiniClusterTest {
    
    private static final Logger Log = LoggerFactory.getLogger(ZZZv3__stopMiniClusterTest.class);
    
    @BeforeClass
    public static void stopMiniCluster() throws TransactionManagerException {
        
        Log.info("Closting mini cluster");
        
        
        LTMClient.close();
        TMMiniCluster.stopMiniCluster();
    }
    
    
    @Test
    public void report() {
        Log.info("Mini Cluster has closed ...");
    }
}
