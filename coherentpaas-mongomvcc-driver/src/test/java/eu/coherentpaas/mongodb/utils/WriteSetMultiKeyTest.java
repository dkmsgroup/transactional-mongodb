package eu.coherentpaas.mongodb.utils;

import com.mongodb.BasicDBObject;
import java.util.HashMap;
import java.util.Map;
import org.bson.types.ObjectId;
import org.junit.Assert.*;
import org.junit.Assert;
import org.junit.Test;
/**
 *
 * @author idezol
 */
public class WriteSetMultiKeyTest {
    
    protected static Map<Object, BasicDBObject> writeset = new HashMap<Object, BasicDBObject>();
    
    @Test
    public void test() {
        
        Long longKey = new Long(100);
        ObjectId objectId = new ObjectId();
        BasicDBObject doc1 = new BasicDBObject("key", longKey).append("value", "value1");
        BasicDBObject doc2 = new BasicDBObject("key", objectId).append("value", "value2");
        
        writeset.put(longKey, doc1);
        writeset.put(objectId, doc2);
        
        Assert.assertEquals(doc1, writeset.get(longKey));
        Assert.assertEquals(doc2, writeset.get(objectId));
    }
}
