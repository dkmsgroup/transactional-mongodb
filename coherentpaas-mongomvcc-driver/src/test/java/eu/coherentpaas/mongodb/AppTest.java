package eu.coherentpaas.mongodb;

import com.mongodb.ParallelTest;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;

/**
 *
 * @author idezol
 */
public class AppTest {
    
    public static void main(String [] args) throws UnknownHostException, TransactionManagerException, InterruptedException, DataStoreException {
        ParallelTest test = new ParallelTest();
        ParallelTest.initDB();
        test.test();
        
        ParallelTest.dropDb();
    }
}
