
import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.Block;
import com.mongodb.BulkWriteOperation;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.DBObject;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.BulkWriteOptions;
import com.mongodb.client.model.DeleteManyModel;
import com.mongodb.client.model.DeleteOneModel;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.InsertOneModel;
import com.mongodb.client.model.ReplaceOneModel;
import com.mongodb.client.model.ReturnDocument;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import com.leanxcale.tests.minicluster.TMMiniCluster;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import eu.coherentpaas.mongodb.utils.ByteOperators;
import eu.coherentpaas.mongodb.utils.VersionQueryUtils;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.lang.SerializationUtils;
import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonInt32;
import org.bson.BsonInt64;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.slf4j.LoggerFactory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author idezol
 */
public class AppTest {
    
    private static final org.slf4j.Logger Log = LoggerFactory.getLogger(AppTest.class);
    
    private static class TestConfiguration {
        public static final String HOSTNAME = "127.0.0.1";
        public static final int PORT = 27017;

        public static final String DATABASE = "UnitTestDriverDB";
        public static final String COLLECTION = "UnitTestDriverCollection";
    }
    
    public static void main(String [] args) throws UnknownHostException, TransactionManagerException, DataStoreException, IOException, Exception {
        
        long version = 242342;
        BsonDocument doc = new BsonDocument(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, new BsonDocument("$gt", new BsonInt64(version)));
        System.out.println(doc.toJson());
        BsonDocument da = (BsonDocument) ((Entry<String, BsonValue>) doc.entrySet().toArray()[0]).getValue();
        da.put("$gt", new BsonInt64(0));
        System.out.println(doc.toJson());
        
        
        BsonDocument docElement = new BsonDocument("$lte", new BsonInt64(version));
        System.out.println(docElement.toJson());
        
        da.put("$lte", new BsonInt64(0));
        System.out.println(docElement.toJson());
        
        //BsonDocument docasdas = new BsonDocument("_id", new BsonInt32(5));
        //VersionQueryUtils queryUtils = new VersionQueryUtils();
        //BsonDocument result = queryUtils.getQueryVersion(docasdas, 3999);
        //System.out.println(result.toJson());
        
        
        TMMiniCluster.startMiniCluster(true, 0);
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        
        MongoDatabase mongoDatabase = mongoClient.getDatabase(TestConfiguration.DATABASE);
        mongoDatabase.startTransaction();
        mongoDatabase.commit();
        
        mongoDatabase.startTransaction();
        mongoDatabase.commit();
        
        
        
        
        AppTest appTest = new AppTest();
        
        appTest.testIntegerKeys3();
        AppTest.initDB();
        appTest.test();
        
        AppTest.dropDb();
        
        
        
        LTMClient.close();
        TMMiniCluster.stopMiniCluster();
    }
    
    
    
    public void testIntegerKeys3() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        
        MongoDatabase mongoDatabase = mongoClient.getDatabase(TestConfiguration.DATABASE);
        mongoDatabase.startTransaction();
        MongoCollection mongoCollection = mongoDatabase.getCollection(TestConfiguration.COLLECTION, Document.class);
        for(int i=1; i<11; i++) {
            Document document = new Document("keyTest", "keyValue" + i)
                    .append("_id", i);
            mongoCollection.insertOne(document);
        }
        
        mongoDatabase.commit();
        mongoDatabase.startTransaction();
        
        MongoCursor cursor = mongoCollection.find(new Document("_id", new BasicDBObject("$lte", 5))).iterator();
        int count = 0;
        while(cursor.hasNext()) {
            Log.info("{}: {}", ++count, (Document) cursor.next());
        }
        
        
        final String deviceID = "1";
        final String tipStart = "1451638390000";
        final String tripEnd = "1451645354000";
        final String type = "LineString";
        
        StringBuilder sb = new StringBuilder(deviceID)
                .append(";")
                .append(tipStart)
                .append(";")
                .append(tripEnd);
        
        
        String geoJsonLocation = "{\n" +
                                "  \"type\": \"LineString\",\n" +
                                "  \"coordinates\": [\n" +
                                "    [-2.551082,48.5955632],\n" +
                                "    [-2.551229,48.594312],\n" +
                                "    [-2.551550,48.593312],\n" +
                                "    [-2.552400,48.592312],\n" +
                                "    [-2.553677, 48.590898]\n" +
                                "  ]\n" +
                                "}";
        
        Document document = new Document("tripId", sb.toString())
                .append("route", geoJsonLocation);
        
        MongoDatabase database  = mongoClient.getDatabase("databaseName");
        MongoCollection collection = database.getCollection("collectionName");
        collection.insertOne(document);
        
        //Document doc = new Document(type, type)
        
        
        mongoDatabase.commit();
        mongoDatabase.startTransaction();
        mongoDatabase.commit();
        
        mongoDatabase.startTransaction();
        
        cursor = mongoCollection.find(new Document("_id", new BasicDBObject("$lte", 5))).iterator();
        count = 0;
        while(cursor.hasNext()) {
            Log.info("{}: {}", ++count, (Document) cursor.next());
        }
        
        Log.info("Total: {}", count);
        
        mongoDatabase.commit();
        mongoDatabase.close();
        mongoClient.close();
        TMMiniCluster.stopMiniCluster();
    }
    
    public void testIntegerKeys() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection collection = database.createCollection(TestConfiguration.COLLECTION, null);
        for(int i=1; i<11 ; i++) {
            BasicDBObject dBObject = new BasicDBObject("keyTest", "keyValue" + i)
                    .append("_id", i);
            collection.insert(dBObject);
        }
        
        DBCursor cursor = collection.find(new BasicDBObject("_id", new BasicDBObject("$lte", 5)));
        int count = 0;
        while(cursor.hasNext()) {
            Log.info("{}: {}", ++count, (BasicDBObject) cursor.next());
        }
        
        database.commit();
        database.startTransaction();
        database.commit();
        
        database.startTransaction();
        
        cursor = collection.find(new BasicDBObject("_id", new BasicDBObject("$lte", 5)));
        count = 0;
        while(cursor.hasNext()) {
            Log.info("{}: {}", ++count, (BasicDBObject) cursor.next());
        }
        
        Log.info("Total: {}", count);
        
        database.commit();
        database.close();
        mongoClient.close();
        TMMiniCluster.stopMiniCluster();
        
    }
    
    
    public void testIdentifier3() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        MongoDatabase mongoDatabase = mongoClient.getDatabase(TestConfiguration.DATABASE);
        mongoDatabase.startTransaction();
        mongoDatabase.createCollection(TestConfiguration.COLLECTION);
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection(TestConfiguration.COLLECTION);
        ObjectId objID = new ObjectId();
        Document doc = new Document("keyTest", "keyValue").append("_id", objID);
        mongoCollection.insertOne(doc);
        mongoDatabase.commit();
        mongoDatabase.startTransaction();
        mongoDatabase.commit();
        
        mongoDatabase.startTransaction();
        MongoCursor<Document> cursor = mongoCollection.find(new Document("_id", objID)).iterator();
        int count = 0;
        while(cursor.hasNext()) {
            Log.info("{}: {}", ++count,  cursor.next().toJson());
        }
        
        Log.info("Total: {}", count);
        
    }
    
    public void testIdentifier() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection collection = database.createCollection(TestConfiguration.COLLECTION, null);
        ObjectId objID = new ObjectId();
        BasicDBObject dBObject = new BasicDBObject("keyTest", "keyValue")
                .append("_id", objID);
        
        collection.insert(dBObject);
        database.commit();
        database.startTransaction();
        database.commit();
        
        database.startTransaction();
        DBCursor cursor = collection.find(new BasicDBObject("_id", objID));
        int count = 0;
        while(cursor.hasNext()) {
            Log.info("{}: {}", ++count, (BasicDBObject) cursor.next());
        }
        
        Log.info("Total: {}", count);
        
    }
    
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection collection = database.createCollection(TestConfiguration.COLLECTION, null);
        
        collection.insert(new BasicDBObject("keyTest", "keyValue"));
        database.commit();
        database.startTransaction();
        database.commit();
        
        Log.info("DB init");
        database.close();
                
    }
    
    
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        
        
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
        
    }
    
    
    
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        DB database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollection collection = database.createCollection(TestConfiguration.COLLECTION, null);
        
        BasicDBObject testingObj = new BasicDBObject("testing", "this value");
        collection.insert(testingObj);
        
        int count = 0;
        DBCursor cursor = collection.find();
        while(cursor.hasNext()) {
            DBObject dbObject = cursor.next();
            Log.info(dbObject.toString());
            count++;
        }
        
        //Assert.assertEquals(count, 2);
        Log.info("Total count: {}", count);
        Log.info("two elements found before commit.");
        
        database.commit();
        
        
        database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        collection = database.getCollection(TestConfiguration.COLLECTION);
        
        count = 0;
        cursor = collection.find(new BasicDBObject("testing", "this value"));
        while(cursor.hasNext()) {
            BasicDBObject result = (BasicDBObject) cursor.next();
            count++;
        }
        
        ///Assert.assertEquals(1, count);
        Log.info("Total count: {}", count);
        Log.info("element found after commit.");
        
       
        
        database.commit();
        database.close();
    }
}
