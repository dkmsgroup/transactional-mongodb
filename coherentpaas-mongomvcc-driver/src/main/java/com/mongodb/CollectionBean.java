package com.mongodb;

import eu.coherentpaas.mongodb.utils.MongoTransactionContext;
import java.util.concurrent.ConcurrentMap;
import org.bson.conversions.Bson;

/**
 *
 * @author idezol
 */
public class CollectionBean {
    
    protected final AbstractMongoCollectionImpl collection;
    protected final MongoTransactionContext mongoTransactionalContext;
    protected final ConcurrentMap<Object, Bson> writeset;

    public CollectionBean(AbstractMongoCollectionImpl collection, MongoTransactionContext mongoTransactionalContext, ConcurrentMap<Object, Bson> writeset) {
        this.collection = collection;
        this.mongoTransactionalContext = mongoTransactionalContext;
        this.writeset = writeset;
    }

    public AbstractMongoCollectionImpl getCollection() {
        return collection;
    }

    public MongoTransactionContext getMongoTransactionalContext() {
        return mongoTransactionalContext;
    }

    public ConcurrentMap<Object, Bson> getWriteset() {
        return writeset;
    }
    
    
}
