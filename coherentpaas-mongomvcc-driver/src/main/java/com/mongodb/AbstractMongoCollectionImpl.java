package com.mongodb;

import static com.mongodb.ReadPreference.primary;
import com.mongodb.binding.ClusterBinding;
import com.mongodb.binding.ReadBinding;
import com.mongodb.binding.ReadWriteBinding;
import com.mongodb.binding.WriteBinding;
import com.mongodb.connection.Cluster;
import eu.coherentpaas.mongodb.utils.MongoTransactionContext;
import eu.coherentpaas.mongodb.utils.WriteSetToApply;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.exception.CoherentPaaSException;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.bson.conversions.Bson;

/**
 *
 * @author idezol
 */
public abstract class AbstractMongoCollectionImpl implements CpMongoCollection {
    protected final String collectionName;
    //protected Long version;
    //protected Long tid;
    protected MongoTransactionContext mongoTransactionalContext;
    protected final AbstractMongoDatabaseImpl abstractDB;
    protected final ConcurrentMap<Object, Bson> writeset;
    protected final ConcurrentMap<Long, WriteSetToApply> pendingWritesetsToApply;
    //protected final List<AbstractMongoCursorImpl> clientCursors = new ArrayList<>();
    protected AtomicBoolean isClosed = new AtomicBoolean(false);
    protected AtomicBoolean isAutoCommit = new AtomicBoolean(false);
    protected boolean hasUpdates = false;

    protected AbstractMongoCollectionImpl(AbstractMongoDatabaseImpl abstractDB, String collectionName, MongoTransactionContext mongoTransactionalContext) {
        this.abstractDB = abstractDB;
        this.collectionName = collectionName;
        this.mongoTransactionalContext = mongoTransactionalContext;
        this.writeset = new ConcurrentHashMap<>();
        this.pendingWritesetsToApply = new ConcurrentHashMap<>();
    }
    
    protected AbstractMongoCollectionImpl(AbstractMongoDatabaseImpl abstractDB, String collectionName, MongoTransactionContext mongoTransactionalContext, ConcurrentMap<Object, Bson> writeset, ConcurrentMap<Long, WriteSetToApply> pendingWritesetsToApply) {
        this.abstractDB = abstractDB;
        this.collectionName = collectionName;
        this.mongoTransactionalContext = mongoTransactionalContext;
        this.writeset = writeset;
        this.pendingWritesetsToApply = pendingWritesetsToApply;
    }
    
    

    @Override
    public String getName() {
        return this.collectionName;
    }
    
    public String getDabaseName() {
        return this.abstractDB.getName();
    }

    @Override
    public void close() {
        _close();
        this.abstractDB.removeCollection(this);
    }
    
    /*
    protected void fireConflictResolutionDetectedEvent() throws TransactionManagerException {
        this.abstractDB.rollback();
    }
    */
    
    protected void _checkClose() {
        if(isClosed.get())
            throw new IllegalStateException("Collection has been closed");
    }
    
    
    /**
     * Checks if the transactiona context is set. If not, then it checks if there is an active transaction and re-sets it
     * If no active transaction, then opens a new one and sets to autocommit
     * 
     * @throws CoherentPaaSException 
     */
    protected void _getTransactionalContext() throws CoherentPaaSException {
        
        if(this.mongoTransactionalContext == null) {
            if(!LTMClient.getInstance().isTransactionOpen()) {
                //if not yet open, then set to autocommit
                LTMClient.getInstance().getConnection().startTransaction();
                this.isAutoCommit.set(true);
            }
             MongoTransactionContext transactionalCtx = new MongoTransactionContext(
                LTMClient.getInstance().getConnection().getCtxId(), 
                LTMClient.getInstance().getConnection().getStartTimestamp(), 
                LTMClient.getInstance().getConnection().getCommitTimestamp());
             this.mongoTransactionalContext = transactionalCtx;
             this.abstractDB.startTransaction(transactionalCtx);
         }
    }
    
    /*
    protected void removeCursor(AbstractMongoCursorImpl cursor) {
        this.clientCursors.remove(cursor);
    }
    
    @Override
    public void cleanCursors() {
        Iterator<AbstractMongoCursorImpl> it = this.clientCursors.iterator();
        while(it.hasNext())
            it.next()._close();
    }
    */
    
    protected void clearTxnCtx() {
        this.mongoTransactionalContext = null;
        if(this.hasUpdates)
            this.hasUpdates=false;
        if(!this.writeset.isEmpty())
            this.writeset.clear();
        if(this.isAutoCommit.get())
            this.isAutoCommit.set(false);
    }
    
    protected void _close() {
        
        this.mongoTransactionalContext = null;
        this.isClosed.set(true);
        this.writeset.clear();      
        
        //close cursors
        /*
        Iterator<AbstractMongoCursorImpl> it = this.clientCursors.iterator();
        while(it.hasNext())
            it.next()._close();

        this.clientCursors.clear();        
        */
    }
    
    
    protected ReadBinding getReadBinding(final ReadPreference readPreference) {
        return getReadWriteBinding(readPreference);
    }
    
    protected ReadWriteBinding getReadWriteBinding(final ReadPreference readPreference) {
        return new ClusterBinding(getCluster(), readPreference);
    }
    
    protected Cluster getCluster() {
        return this.abstractDB._depracedMongoClient.getCluster();
    }
    
    protected WriteBinding getWriteBinding() {
        return getReadWriteBinding(primary());
    }
    
    protected void autoCommit() {
        try {
            LTMClient.getInstance().getConnection().commitSync();
            //LTMClient.getInstance().getConnection().commit();
            
        } catch (TransactionManagerException | DataStoreException ex) {
            throw new MongoException(ex.getMessage(), ex);
        } finally {
            this.isAutoCommit.set(false);
        }
        
        //again for flushing...
        /*
        try {
            LTMClient.getInstance().getConnection().startTransaction();
            LTMClient.getInstance().getConnection().commit();
        } catch (TransactionManagerException | DataStoreException ex) {
            Logger.getLogger(AbstractMongoCollectionImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.isAutoCommit.set(false);
        }
        */
    }
    
    protected void addToPendingWritesetsToApply(long tid, WriteSetToApply writeSetToApply) {
        this.pendingWritesetsToApply.put(tid, writeSetToApply);
    }
     
    
    //protected abstract ConcurrentMap _getWriteSet() throws MongoException;
    
    protected abstract void _applyWriteSet(long tid, long commitTmp) throws MongoException;
    
    protected abstract void _rollback(long tid) throws MongoException;
    
}
