package com.mongodb;

import eu.coherentpaas.mongodb.utils.CollectionRecoveryWriteset;
import eu.coherentpaas.mongodb.utils.MongoTransactionContext;
import eu.coherentpaas.mongodb.utils.WrapperTransactionContext;
import eu.coherentpaas.mongodb.utils.WriteSetToApply;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.exception.CoherentPaaSException;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.bson.conversions.Bson;
import org.mortbay.log.Log;

/**
 *
 * @author idezol
 */
public abstract class AbstractMongoDatabaseImpl implements CpMongoDatabase {
    private final long dbID;
    //protected Long tid;
    //protected Long version;
    protected final String databaseName;
    protected final MongoClient mongoMVCCClient;
    protected final DeprecatedMongoClient _depracedMongoClient;
    protected MongoTransactionContext mongoTransactionalContexct;
    protected ConcurrentMap<String, AbstractMongoCollectionImpl> dbOpenCollections = new ConcurrentHashMap<> ();

    protected AbstractMongoDatabaseImpl(long dbID, DeprecatedMongoClient _depracedMongoClient, MongoClient mongoMVCCClient, String dataBaseName) {
        this.dbID = dbID;
        this._depracedMongoClient = _depracedMongoClient;
        this.mongoMVCCClient = mongoMVCCClient;
        this.databaseName = dataBaseName;
    }
    
    
    /**
     * Returns the name of this database.
     *
     * @return the name
     */
    @Override
    public String getName() {
        return this.databaseName;
    }
    
    /**
     * Returns the name of this database.
     *
     * @return the name
     */
    
    public String getTransactionalName() {
        return this.databaseName + ": " + dbID;
    } 
    
    
    protected void removeCollection(AbstractMongoCollectionImpl collection) {
        this.dbOpenCollections.remove(collection.getName());
        //this.dbOpenCollections.remove(collection);
    }
    
    
    @Override
    public void close() {
        _close();
        this.mongoMVCCClient.removeDatabase(this.dbID);
    }
    
    
    protected void _close() {
        //do closing things
        
        //close collections
        Iterator<Map.Entry<String, AbstractMongoCollectionImpl>> it = this.dbOpenCollections.entrySet().iterator();
        while(it.hasNext()) {
            it.next().getValue()._close();
            it.remove();
        }
        
        //this.tid=null;
        //this.version=null;
        this.mongoTransactionalContexct = null;
    }
   
    /**
     * @param force is remained only for compatibility reasons
     * @throws MongoException
     */
    public void cleanCursors( boolean force ){
        /*
        Iterator<Map.Entry<String, AbstractMongoCollectionImpl>> it = this.dbOpenCollections.entrySet().iterator();
        while(it.hasNext())
            it.next().getValue().cleanCursors();
        */
    }
    
    
    @Override
    public void startTransaction() { 
        
        try {
            LTMClient.getInstance().getConnection().startTransaction();
            MongoTransactionContext txnContext = new MongoTransactionContext(
                    LTMClient.getInstance().getConnection().getCtxId(), 
                    LTMClient.getInstance().getConnection().getStartTimestamp(), 
                    LTMClient.getInstance().getConnection().getCommitTimestamp());
            startTransaction(txnContext);
        } catch(TransactionManagerException ex) {
            throw new MongoException(ex.getMessage(), ex);
        } catch(CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
    }
    

    public void startTransaction(MongoTransactionContext txnContext) throws CoherentPaaSException {       
        
        if((this.mongoTransactionalContexct!=null)&&(this.mongoTransactionalContexct.getTid()==txnContext.getTid()))
                return; //already assigned
        
        if(!(txnContext instanceof WrapperTransactionContext)) {
             try {
                LTMClient.getInstance().getConnection().associate(this.mongoMVCCClient);
            } catch (CoherentPaaSException ex) {
                throw new TransactionManagerException(ex);
            }
        } else {
            //if coming from the wrapper, then propagate the transactional context to all its open collections
            if(!this.dbOpenCollections.isEmpty()) {
                Iterator<Entry<String, AbstractMongoCollectionImpl>> it = this.dbOpenCollections.entrySet().iterator();
                while(it.hasNext()) {
                    AbstractMongoCollectionImpl collection = it.next().getValue();
                    collection.mongoTransactionalContext = txnContext;
                }
            }
        }

        this.mongoTransactionalContexct = txnContext;
        this.mongoMVCCClient.addToOpenTransactions(this, txnContext.getTid());
    }
    
    /*
    protected void markAsUpdateTransaction(MongoTransactionContext txnContext) throws CumuloNimboException, TransactionManagerException {
        //LTMClient.getInstance().getConnection().
        this.tid = txnContext.getTid();
        this.mongoMVCCClient.addToOpenTransactions(this, this.tid);
    }
    */
    
    @Override
    public void commit()  {        
        try {
            LTMClient.getInstance().getConnection().commit();
        } catch (TransactionManagerException | DataStoreException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
    }
    
    @Override
    public void rollback() {
        try {
            LTMClient.getInstance().getConnection().rollback();
        } catch (TransactionManagerException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
    }
 
    
    /**
     * Returns a list of CollectionRecoveryWriteset for recovery. if no collection is open or there's no writeset to apply (RO operations) then it returns an empty list
     *
     * @return list of writesets to recover from this database
     */
    protected List<CollectionRecoveryWriteset> _getWS() {
        List<CollectionRecoveryWriteset> listRecoverWritesets = new ArrayList<>(this.dbOpenCollections.size());
        
        if(this.dbOpenCollections.isEmpty())
            return listRecoverWritesets;
        
        for (Map.Entry entry : this.dbOpenCollections.entrySet()) {
            AbstractMongoCollectionImpl dBCollection = (AbstractMongoCollectionImpl) entry.getValue();
            if(dBCollection.writeset.isEmpty()) {
                dBCollection.clearTxnCtx();
                continue;
            }
            
            //here store the ids to be applied in the 2nd phase of the 2PC protocol
            BasicDBList dbList = new BasicDBList();
            //here store (copy) the writeset to recover during the 1st phase of the 2PC protocol
            Map<Object, Bson> writesetToRecover = new HashMap<>(dBCollection.writeset.size());
            Iterator<Map.Entry<Object, Bson>> it = dBCollection.writeset.entrySet().iterator();
            while(it.hasNext()) {
                Map.Entry<Object, Bson> writesetEntry = it.next();
                //add to recover 
                writesetToRecover.put(writesetEntry.getKey(), writesetEntry.getValue());
                dbList.add(writesetEntry.getKey());
                it.remove();
            }
            listRecoverWritesets.add(new CollectionRecoveryWriteset(dBCollection.collectionName, writesetToRecover));
            dBCollection.addToPendingWritesetsToApply(this.mongoTransactionalContexct.getTid(), new WriteSetToApply(dBCollection.hasUpdates, dbList));
            
            //and clear the transactional context, so the next operation on the same collection will get the new one
            dBCollection.clearTxnCtx();
        }
        
        return listRecoverWritesets;
    }
    
    /*
    protected byte[] _getWS() throws IOException {
        Iterator it = this.dbOpenCollections.entrySet().iterator();
        List<byte []> list = new ArrayList<>();
        int countBytes = 0;
        while(it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            AbstractMongoCollectionImpl dBCollection = (AbstractMongoCollectionImpl) entry.getValue();
            
            if(dBCollection.mongoTransactionalContext==null) //no write operaetion performed so far, return null
                continue;
            
            byte [] bytes = ByteOperators.getBytesFromWriteSet(dBCollection.getDabaseName(), dBCollection.getName(), dBCollection._getWriteSet());
            countBytes += bytes.length;
            list.add(bytes);
        } 
        
        ByteBuffer bf = ByteBuffer.allocate(countBytes);
        for(int i=0; i<list.size(); i++) 
            bf.put(list.get(i));
        
        return bf.array();
    }
    */
    
    protected void _unleash(TxnCtx txnctx) throws TransactionManagerException{
        if(!this.dbOpenCollections.isEmpty()) {
            Iterator iterator = this.dbOpenCollections.entrySet().iterator();
            while(iterator.hasNext()) {

                AbstractMongoCollectionImpl dbCollection = (AbstractMongoCollectionImpl) ((Map.Entry) iterator.next()).getValue();

                try {
                    dbCollection.clearTxnCtx();
                } catch(MongoException ex) {
                    throw new TransactionManagerException(ex);
                }
            }
        }
    }
    
    protected void _applyWS(MongoTransactionContext txnctx) throws TransactionManagerException {
        
        //apply WS
        Iterator iterator = this.dbOpenCollections.entrySet().iterator();
        while(iterator.hasNext()) {
            
            AbstractMongoCollectionImpl dbCollection = (AbstractMongoCollectionImpl) ((Map.Entry) iterator.next()).getValue();
            
            try {
                dbCollection._applyWriteSet(txnctx.getTid(), txnctx.getCommitTimestamp());
            } catch(MongoException ex) {
                throw new TransactionManagerException(ex);
            }
        }
        
        //and close
        //_close();
    }
    
    protected void _rollback(MongoTransactionContext txnctx) {
        //remove private versions from database
        Iterator iterator = this.dbOpenCollections.entrySet().iterator();
        while(iterator.hasNext()) {            
            AbstractMongoCollectionImpl dbCollection = (AbstractMongoCollectionImpl) ((Map.Entry) iterator.next()).getValue();
            dbCollection._rollback(txnctx.getTid());
            dbCollection.clearTxnCtx();
        }
        
        //and close
        //_close();
    }

    @Override
    public String toString() {
        return "AbstractMongoDatabaseImpl{" + "dbID=" + dbID + ", databaseName=" + databaseName + '}';
    }
    
    
}
