package com.mongodb;

import eu.coherentpaas.mongodb.utils.MongoTransactionContext;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.bson.conversions.Bson;

/**
 *
 * @author idezol
 */
public abstract class AbstractMongoCursorImpl implements CpMongoCursor {
    
    protected final CollectionBean dbCollection;
    protected final MongoTransactionContext mongoTransactionalContext;
    protected final ConcurrentMap<Object, Bson> writeset;
    protected int skip;
    protected int limit;
    protected int totalScanned = 0;
    protected AtomicBoolean isClosed = new AtomicBoolean(false);

    protected AbstractMongoCursorImpl(CollectionBean dbCollection, int skip, int limit) {
        this.dbCollection = dbCollection;
        this.skip = skip;
        this.limit = limit;
        this.writeset = dbCollection.writeset;
        this.mongoTransactionalContext = dbCollection.mongoTransactionalContext;
    }
    
     /**
     * kills the current cursor on the server.
     */
    @Override
    public void close() {
        /*
        if(this.dbCollection.collection!=null)
            this.dbCollection.collection.removeCursor(null);
        */        
        _close();
    }
    
    protected void _checkIfClosed() {
        if(this.isClosed.get())
            throw new IllegalStateException("Cursor has been closed");
    }
    
    protected abstract void _close();
    
}
