package com.mongodb;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author stavroula
 */
public interface IDBCursor extends CpMongoCursor {
    
     public IDBCursor comment(final String comment);
    
     public IDBCursor maxScan(final int max);
     
     public IDBCursor max(final DBObject max);
     
     public IDBCursor min(final DBObject min);
     
     public IDBCursor returnKey();
     
     public IDBCursor showDiskLoc();
     
     public IDBCursor copy();
    
    public IDBCursor sort( DBObject orderBy );
    
    public IDBCursor addSpecial(String name, Object o);
    
    public IDBCursor hint(final DBObject indexKeys);
    
    public IDBCursor hint(final String indexName);
    
    public IDBCursor maxTime(final long maxTime, final TimeUnit timeUnit);
    
    public IDBCursor snapshot();
    
    public DBObject explain();
    
    public IDBCursor limit(final int limit);
    
    public IDBCursor batchSize(int numberOfElements);
    
    public IDBCursor skip(final int numberOfElements);
    
    public IDBCursor addOption(final int option);
    
    public IDBCursor setOptions( int options );
    
    public IDBCursor resetOptions();
    
    public int getOptions();
    
    public int getLimit();
    
    public int getBatchSize();
    
    public int numSeen();
    
    public boolean hasNext();
    
    public DBObject tryNext();
    
    public DBObject next();
    
    public DBObject curr();
    
    public void remove();
    
    
    public int length();
    
    public List<DBObject> toArray();
    
    public List<DBObject> toArray( int max );
    
    public int itcount();
    
    public int count();
    
    public DBObject one();
    
    public int size();
    
    public DBObject getKeysWanted();
    
    public DBObject getQuery();
    
    public IDBCollection getCollection();
    
    public ServerAddress getServerAddress();
    
    public IDBCursor setReadPreference(final ReadPreference readPreference);
    
    public ReadPreference getReadPreference();
    
    public IDBCursor setDecoderFactory(final DBDecoderFactory fact);
    
    public DBDecoderFactory getDecoderFactory();

}
