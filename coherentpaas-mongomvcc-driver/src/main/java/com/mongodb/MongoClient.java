package com.mongodb;

import com.mongodb.utils.TestConf;
import com.mongodb.client.ListDatabasesIterable;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import eu.coherentpaas.mongodb.utils.CollectionRecoveryWriteset;
import eu.coherentpaas.mongodb.utils.DataBaseRecoveryWriteset;
import eu.coherentpaas.mongodb.utils.MongoTransactionContext;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.datastore.DataStoreId;
import eu.coherentpaas.transactionmanager.datastore.TransactionalDSClient;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.commons.lang.SerializationUtils;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public class MongoClient implements TransactionalDSClient, IMongoClient {
    private static final Logger Log = LoggerFactory.getLogger(MongoClient.class);
    
    private final AtomicLong dbCounter = new AtomicLong(0);
    private static DeprecatedMongoClient mongoClient = null;
    private ServerAddress serverAddress;
    private MongoClientOptions options;
    private final ConcurrentMap<Long, AbstractMongoDatabaseImpl> databases = new ConcurrentHashMap<>();
    private final ConcurrentMap<Long, AbstractMongoDatabaseImpl> openTransactionalDatabases = new ConcurrentHashMap<> ();
    private final ReentrantLock lock = new ReentrantLock();
    
    protected void removeDatabase(Long dbId) {
        this.databases.remove(dbId);
    }
    
    protected void addToOpenTransactions(AbstractMongoDatabaseImpl db, long tid) {
        this.openTransactionalDatabases.putIfAbsent(tid, db);
    }
    
    protected void removeFromOpenTransactions(AbstractMongoDatabaseImpl clientDatabase, long tid) {
        this.openTransactionalDatabases.remove(tid);
    }
    
    protected DeprecatedMongoClient getDepracedMongoClient() {
        return mongoClient;
    }
    
    /**
     * Creates an instance based on a (single) mongodb node (localhost, default port).
     *
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     */
    @Deprecated
    public MongoClient() throws UnknownHostException {
        while(mongoClient==null) {
            lock.lock();
            try {
                 
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(serverAddress);
                                  
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    
    
    /**
     * Creates a Mongo instance based on a (single) mongodb node.
     *
     * @param host server to connect to in format host[:port]
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     */
    @Deprecated
    public MongoClient(final String host) throws UnknownHostException {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.serverAddress = new ServerAddress(host);
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(serverAddress);
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongodb node (default port).
     *
     * @param host    server to connect to in format host[:port]
     * @param options default query options
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     */
    @Deprecated
    public MongoClient(final String host, final MongoClientOptions options) throws UnknownHostException {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.serverAddress = new ServerAddress(host);
                 this.options = options;
                 mongoClient = new DeprecatedMongoClient(host, options);
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongodb node.
     *
     * @param host the database's host address
     * @param port the port on which the database is running
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     */
    @Deprecated
    public MongoClient(final String host, final int port) throws UnknownHostException {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.serverAddress = new ServerAddress(host, port);
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(host);
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongodb node
     *
     * @param addr the database address
     * @see com.mongodb.ServerAddress
     */
    @Deprecated
    public MongoClient(final ServerAddress addr) throws UnknownHostException {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 this.serverAddress = addr;
                 mongoClient = new DeprecatedMongoClient(addr, options);
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongodb node and a list of credentials
     *
     * @param addr the database address
     * @param credentialsList the list of credentials used to authenticate all connections
     * @see com.mongodb.ServerAddress
     * @since 2.11.0
     */
    @Deprecated
    public MongoClient(final ServerAddress addr, final List<MongoCredential> credentialsList) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 this.serverAddress = addr;
                 mongoClient = new DeprecatedMongoClient(addr, credentialsList, options);
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongo node using a given ServerAddress and default options.
     *
     * @param addr    the database address
     * @param options default options
     * @see com.mongodb.ServerAddress
     */
    @Deprecated
    public MongoClient(final ServerAddress addr, final MongoClientOptions options) {
        //this(addr, null, options);
        this(addr, new ArrayList<MongoCredential>(), options);
    }

    /**
     * Creates a Mongo instance based on a (single) mongo node using a given ServerAddress and default options.
     *
     * @param addr    the database address
     * @param credentialsList the list of credentials used to authenticate all connections
     * @param options default options
     * @see com.mongodb.ServerAddress
     * @since 2.11.0
     */
    @Deprecated
    @SuppressWarnings("deprecation")
    public MongoClient(final ServerAddress addr, final List<MongoCredential> credentialsList, final MongoClientOptions options) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = options;
                 this.serverAddress = addr;
                 mongoClient = new DeprecatedMongoClient(addr, credentialsList, options);
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param seeds Put as many servers as you can in the list and the system will figure out the rest.  This can either be a list of mongod
     *              servers in the same replica set or a list of mongos servers in the same sharded cluster.
     * @see com.mongodb.ServerAddress
     */
    @Deprecated
    public MongoClient(final List<ServerAddress> seeds) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(seeds);
                 serverAddress = mongoClient.getAddress();
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param seeds           Put as many servers as you can in the list and the system will figure out the rest.  This can either be a list
     *                        of mongod servers in the same replica set or a list of mongos servers in the same sharded cluster. \
     * @param credentialsList the list of credentials used to authenticate all connections
     * @see com.mongodb.ServerAddress
     * @since 2.11.0
     */
    @Deprecated
    public MongoClient(final List<ServerAddress> seeds, final List<MongoCredential> credentialsList) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(seeds, credentialsList, options);
                 serverAddress = mongoClient.getAddress();
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param seeds   Put as many servers as you can in the list and the system will figure out the rest.  This can either be a list of
     *                mongod servers in the same replica set or a list of mongos servers in the same sharded cluster.
     * @param options default options
     * @see com.mongodb.ServerAddress
     */
    @Deprecated
    public MongoClient(final List<ServerAddress> seeds, final MongoClientOptions options) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = options;
                 mongoClient = new DeprecatedMongoClient(seeds, options);
                 this.serverAddress = mongoClient.getAddress();
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }
    
    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param seeds   Put as many servers as you can in the list and the system will figure out the rest.  This can either be a list of
     *                mongod servers in the same replica set or a list of mongos servers in the same sharded cluster.
     * @param credentialsList
     * @param options default options
     * @see com.mongodb.ServerAddress
     */
    @Deprecated
    public MongoClient(final List<ServerAddress> seeds, final List<MongoCredential> credentialsList, final MongoClientOptions options) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = options;
                 mongoClient = new DeprecatedMongoClient(seeds, credentialsList, options);
                 this.serverAddress = mongoClient.getAddress();
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }
    
    
    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param uri
     * @see com.mongodb.ServerAddress
     */
    @Deprecated
    public MongoClient(final MongoClientURI uri) {       
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(uri);
                 this.serverAddress = mongoClient.getAddress(); 
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }
    
    
    /**
     * Creates an instance based on a (single) mongodb node (localhost, default port).
     *
     * @param fromFactory
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     */
    protected MongoClient(boolean fromFactory) throws UnknownHostException {        
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.serverAddress = new ServerAddress();
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(serverAddress);
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }
    
    /**
     * Creates a Mongo instance based on a (single) mongodb node.
     *
     * @param host server to connect to in format host[:port]
     * @param fromFactory
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     */
    protected MongoClient(final String host, boolean fromFactory) throws UnknownHostException {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.serverAddress = new ServerAddress(host);
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(serverAddress);
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongodb node (default port).
     *
     * @param host    server to connect to in format host[:port]
     * @param options default query options
     * @param fromFactory
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     */
    protected MongoClient(final String host, final MongoClientOptions options, boolean fromFactory) throws UnknownHostException {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.serverAddress = new ServerAddress(host);
                 this.options = options;
                 mongoClient = new DeprecatedMongoClient(host, options);
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongodb node.
     *
     * @param host the database's host address
     * @param port the port on which the database is running
     * @param fromFactory
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     */
    protected MongoClient(final String host, final int port, boolean fromFactory) throws UnknownHostException {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.serverAddress = new ServerAddress(host, port);
                 this.options = new MongoClientOptions.Builder().build();
                 
                 mongoClient = new DeprecatedMongoClient(this.serverAddress);
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongodb node
     *
     * @param addr the database address
     * @param fromFactory
     * @throws java.net.UnknownHostException
     * @see com.mongodb.ServerAddress
     */
    protected MongoClient(final ServerAddress addr, boolean fromFactory) throws UnknownHostException {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 this.serverAddress = addr;
                 mongoClient = new DeprecatedMongoClient(addr, options);
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongodb node and a list of credentials
     *
     * @param addr the database address
     * @param credentialsList the list of credentials used to authenticate all connections
     * @param fromFactory
     * @see com.mongodb.ServerAddress
     * @since 2.11.0
     */
    protected MongoClient(final ServerAddress addr, final List<MongoCredential> credentialsList, boolean fromFactory) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 this.serverAddress = addr;
                 mongoClient = new DeprecatedMongoClient(addr, credentialsList, options);
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongo node using a given ServerAddress and default options.
     *
     * @param addr    the database address
     * @param options default options
     * @see com.mongodb.ServerAddress
     */
    protected MongoClient(final ServerAddress addr, final MongoClientOptions options, boolean fromFactory) {
        this(addr, new ArrayList<MongoCredential>(), options, fromFactory);
    }

    /**
     * Creates a Mongo instance based on a (single) mongo node using a given ServerAddress and default options.
     *
     * @param addr    the database address
     * @param credentialsList the list of credentials used to authenticate all connections
     * @param options default options
     * @see com.mongodb.ServerAddress
     * @since 2.11.0
     */
    @SuppressWarnings("deprecation")
    protected MongoClient(final ServerAddress addr, final List<MongoCredential> credentialsList, final MongoClientOptions options, boolean fromFactory) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = options;
                 this.serverAddress = addr;
                 mongoClient = new DeprecatedMongoClient(addr, credentialsList, options);
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param seeds Put as many servers as you can in the list and the system will figure out the rest.  This can either be a list of mongod
     *              servers in the same replica set or a list of mongos servers in the same sharded cluster.
     * @see com.mongodb.ServerAddress
     */
    protected MongoClient(final List<ServerAddress> seeds, boolean fromFactory) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(seeds);
                 serverAddress = mongoClient.getAddress();
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param seeds           Put as many servers as you can in the list and the system will figure out the rest.  This can either be a list
     *                        of mongod servers in the same replica set or a list of mongos servers in the same sharded cluster. \
     * @param credentialsList the list of credentials used to authenticate all connections
     * @see com.mongodb.ServerAddress
     * @since 2.11.0
     */
    protected MongoClient(final List<ServerAddress> seeds, final List<MongoCredential> credentialsList, boolean fromFactory) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(seeds, credentialsList, options);
                 serverAddress = mongoClient.getAddress();
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param seeds   Put as many servers as you can in the list and the system will figure out the rest.  This can either be a list of
     *                mongod servers in the same replica set or a list of mongos servers in the same sharded cluster.
     * @param options default options
     * @see com.mongodb.ServerAddress
     */
    protected MongoClient(final List<ServerAddress> seeds, final MongoClientOptions options, boolean fromFactory) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = options;
                 mongoClient = new DeprecatedMongoClient(seeds, options);
                 this.serverAddress = mongoClient.getAddress();
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }
    
    protected MongoClient(final List<ServerAddress> seeds, final List<MongoCredential> credentialsList, final MongoClientOptions options, boolean fromFactory) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = options;
                 mongoClient = new DeprecatedMongoClient(seeds, credentialsList, options);
                 this.serverAddress = mongoClient.getAddress();
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }
    
    
    protected MongoClient(final MongoClientURI uri, boolean fromFactory) {       
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(uri);
                 this.serverAddress = mongoClient.getAddress(); 
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }
    
    
    /**
     * Gets the list of credentials that this client authenticates all connections with
     *
     * @return the list of credentials
     * @since 2.11.0
     */
    @Override
    public List<MongoCredential> getCredentialsList() {
        return mongoClient.getCredentialsList();
    }

    @Override
    public MongoClientOptions getMongoClientOptions() {
        return options;
    }
    
    
    /**
     * Gets a {@code String} representation of current connection point, i.e. master.
     *
     * @return server address in a host:port form
     */
    @Override
    public String getConnectPoint(){
        return mongoClient.getConnectPoint();
    }
    
    /**
     * Get the status of the replica set cluster.
     *
     * @return replica set status information
     */
    @Override
    public ReplicaSetStatus getReplicaSetStatus() {
        return this.mongoClient.getReplicaSetStatus();
    }
    
    /**
     * Gets the address of the current master
     *
     * @return the address
     */
    @Override
    public ServerAddress getAddress(){
        return mongoClient.getAddress();
    }
    
    /**
     * Gets a list of all server addresses used when this Mongo was created
     *
     * @return list of server addresses
     * @throws MongoException
     */
    @Override
    public List<ServerAddress> getAllAddress() {
        return mongoClient.getAllAddress();
    }
    
    /**
     * Gets the list of server addresses currently seen by this client. This includes addresses auto-discovered from a replica set.
     *
     * @return list of server addresses
     * @throws MongoException
     */
    @Override
    public List<ServerAddress> getServerAddressList() {
        return mongoClient.getServerAddressList();
    }
    
    /**
     * Sets the write concern for this database. Will be used as default for writes to any collection in any database. See the documentation
     * for {@link WriteConcern} for more information.
     *
     * @param concern write concern to use
     */
    @Override
    public void setWriteConcern( WriteConcern concern ){
        mongoClient.setWriteConcern(concern);
    }
    
    /**
     * Gets the default write concern
     *
     * @return the default write concern
     */
    @Override
    public WriteConcern getWriteConcern(){
        return mongoClient.getWriteConcern();
    }
    
    
    /**
     * Sets the read preference for this database. Will be used as default for reads from any collection in any database. See the
     * documentation for {@link ReadPreference} for more information.
     *
     * @param preference Read Preference to use
     */
    @Override
    public void setReadPreference(ReadPreference preference) {
        mongoClient.setReadPreference(preference);
    }
    
    /**
     * Gets the default read preference
     *
     * @return the default read preference
     */
    @Override
    public ReadPreference getReadPreference(){
        return mongoClient.getReadPreference();
    }
    
    /**
     * Add a default query option keeping any previously added options.
     *
     * @param option value to be added to current options
     */
    @Override
    public void addOption( int option ){
        mongoClient.addOption(option);
    }

    /**
     * Set the default query options.  Overrides any existing options.
     *
     * @param options value to be set
     */
    @Override
    public void setOptions( int options ){
        mongoClient.setOptions(options);
    }

    /**
     * Reset the default query options
     */
    @Override
    public void resetOptions(){
        mongoClient.resetOptions();
    }

    /**
     * Gets the default query options
     *
     * @return an int representing the options to be used by queries
     */
    @Override
    public int getOptions(){
        return mongoClient.getOptions();
    }
    
    /**
     * Gets the maximum size for a BSON object supported by the current master server. Note that this value may change over time depending
     * on which server is master.
     *
     * @return the maximum size, or 0 if not obtained from servers yet.
     * @throws MongoException
     */
    @Override
    public int getMaxBsonObjectSize() {
        return mongoClient.getMaxBsonObjectSize();
    }
    
    /**
     * Forces the master server to fsync the RAM data to disk This is done automatically by the server at intervals, but can be forced for
     * better reliability.
     *
     * @param async if true, the fsync will be done asynchronously on the server.
     * @return result of the command execution
     * @throws MongoException
     * @mongodb.driver.manual reference/command/fsync/ fsync command
     */
    @Override
    public CommandResult fsync(boolean async) {
        return mongoClient.fsync(async);
    }
    
    /**
     * Forces the master server to fsync the RAM data to disk, then lock all writes. The database will be read-only after this command
     * returns.
     *
     * @return result of the command execution
     * @throws MongoException
     * @mongodb.driver.manual reference/command/fsync/ fsync command
     */
    @Override
    public CommandResult fsyncAndLock() {
        return mongoClient.fsyncAndLock();
    }
    
    /**
     * Unlocks the database, allowing the write operations to go through. This command may be asynchronous on the server, which means there
     * may be a small delay before the database becomes writable.
     *
     * @return {@code DBObject} in the following form {@code {"ok": 1,"info": "unlock completed"}}
     * @throws MongoException
     * @mongodb.driver.manual reference/command/fsync/ fsync command
     */
    @Override
    public DBObject unlock() {
        return mongoClient.unlock();
    }
    
    
    /**
     * Returns true if the database is locked (read-only), false otherwise.
     *
     * @return result of the command execution
     * @throws MongoException
     * @mongodb.driver.manual reference/command/fsync/ fsync command
     */
    @Override
    public boolean isLocked() {
        return mongoClient.isLocked();
    }
    
    
    @Override
    public String toString() {
        return "Mongo{" +
                "serverAddress=" + this.serverAddress +
                ", options=" + this.options +
                '}';
    }
    
    
    /**
     * Returns the list of databases used by the driver since this Mongo instance was created. This may include DBs that exist in the client
     * but not yet on the server.
     *
     * @return a collection of database objects
     */
    @Override
    public Collection<IDB> getUsedDatabases(){
        List<IDB> list = new ArrayList<>();
        Iterator<Entry<Long, AbstractMongoDatabaseImpl>> iterator = this.databases.entrySet().iterator();
        while(iterator.hasNext()) {
            AbstractMongoDatabaseImpl database = iterator.next().getValue();
            if(database instanceof DB)
                list.add((DB) database);
        }
        
        return list;
    }
    
    /**
     * Gets a list of the names of all databases on the connected server.
     *
     * @return list of database names
     * @throws MongoException
     */
    @Override
    public List<String> getDatabaseNames(){
        List<String> list = new ArrayList<>();
        Iterator<Entry<Long, AbstractMongoDatabaseImpl>> it = this.databases.entrySet().iterator();
        while(it.hasNext()) {
            list.add(it.next().getValue().getName());
        }
        return list;
    }
    
    
    /**
     * Gets a database object from this MongoDB instance.
     *
     * @param dbname the name of the database to retrieve
     * @return a DB representing the specified database
     */
    @Override
    public DB getDB(final String dbname){
        /*
        AbstractMongoDatabaseImpl db = this.databases.get(dbname);
        if(db!=null) {
            if(db instanceof DB)
                return (DB) db;
            else
                throw new IllegalStateException(dbname + " has already been initialized and it is not type of " + DB.class.getName());
        }
        
        DeprecatedDB deprecatedDb = this.mongoClient.getDB(dbname);
        DB database = new DB(dbname, mongoClient, this, deprecatedDb);
        DB temp = (DB) this.databases.putIfAbsent(dbname, database);
        if (temp != null) {
            return temp;
        }
        return database;        
        */
        
        long dbID = this.dbCounter.incrementAndGet();
        DeprecatedDB deprecatedDb = mongoClient.getDB(dbname);
        DB database = new DB(dbID, dbname, mongoClient, this, deprecatedDb);
        this.databases.put(dbID, database);
        return database;
        
    }
    
    /**
     * Gets a database object from this MongoDB instance.
     *
     * @param dbname the name of the database to retrieve
     * @return a DB representing the specified database
     */
    public DBWrapper getDBWrapper(String dbname){
        /*
        AbstractMongoDatabaseImpl db = this.databases.get(dbname);
        if(db!=null) {
            if(db instanceof DBWrapper)
                return (DBWrapper) db;
            else
                throw new IllegalStateException(dbname + " has already been initialized and it is not type of " + DBWrapper.class.getName());
        }
        
        DeprecatedDB deprecatedDb = this.mongoClient.getDB(dbname);
        DBWrapper database = new DBWrapper(dbname, mongoClient, this, deprecatedDb);
        DBWrapper temp = (DBWrapper) this.databases.putIfAbsent(dbname, database);
        if (temp != null) {
            return temp;
        }
        return database;
        */
        
        long dbID = this.dbCounter.incrementAndGet();
        DeprecatedDB deprecatedDb = mongoClient.getDB(dbname);
        DBWrapper database = new DBWrapper(dbID, dbname, mongoClient, this, deprecatedDb);
        this.databases.put(dbID, database);
        return database;
    }
    
    
    
    
    /**
     * Drops the database if it exists.
     *
     * @param dbName name of database to drop
     * @throws MongoException
     */
    @Override
    public void dropDatabase(String dbName) {
        Iterator it = this.openTransactionalDatabases.entrySet().iterator();
        while(it.hasNext()) {
            AbstractMongoDatabaseImpl db = (AbstractMongoDatabaseImpl) ((Map.Entry) it.next()).getValue();
            if(db.getName().equals(dbName)) {
                try {
                    if(LTMClient.getInstance().isTransactionOpen())
                        db.rollback();
                    it.remove();
                } catch (Exception ex) {
                    throw new MongoException(ex.getMessage(), ex);
                }
            }
            
        }
        
        it = this.databases.entrySet().iterator();
        while(it.hasNext()) {
            AbstractMongoDatabaseImpl db = (AbstractMongoDatabaseImpl) ((Map.Entry) it.next()).getValue();
            if(db.getName().equals(dbName)) {
                db._close();                
                it.remove();
            }
        }
        
        mongoClient.dropDatabase(dbName);
    }
    
    
    /**
     * Rollbacks all open transcations
     * Then closes all active databases
     * Finally Closes the underlying connector, which in turn closes all open connections. Once called, this Mongo instance can no longer be used.
     * 
     */
    @Override
    public void close(){
        
        Iterator it = this.openTransactionalDatabases.entrySet().iterator();
        while(it.hasNext()) {
            AbstractMongoDatabaseImpl db = (AbstractMongoDatabaseImpl) ((Map.Entry) it.next()).getValue();
            try {
                db.rollback();
            } catch (Exception ex) {
                Log.warn("Database " + db.getName() + " could not be rollbacked");
            }
        }
        this.openTransactionalDatabases.clear();
        
        it = this.databases.entrySet().iterator();
        while(it.hasNext()) {
            AbstractMongoDatabaseImpl db = (AbstractMongoDatabaseImpl) ((Map.Entry) it.next()).getValue();
            db._close();
        }
        this.databases.clear();
        
        
        mongoClient.close();
        mongoClient = null;
    }
    
    
    //CODE COMPATIBLE FOR MongoClient v3.0.x
    
    
    /**
     * Gets the default codec registry.  It includes the following providers:
     * <ul>
     * <li>{@link org.bson.codecs.ValueCodecProvider}</li>
     * <li>{@link org.bson.codecs.DocumentCodecProvider}</li>
     * <li>{@link com.mongodb.DBObjectCodecProvider}</li>
     * <li>{@link org.bson.codecs.BsonValueCodecProvider}</li>
     * <li>{@link com.mongodb.client.model.geojson.codecs.GeoJsonCodecProvider}</li>
     * </ul>
     *
     * @return the default codec registry
     * @see MongoClientOptions#getCodecRegistry()
     * @since 3.0
     */
    public static CodecRegistry getDefaultCodecRegistry() {
        return DeprecatedMongoClient.getDefaultCodecRegistry();
    }
    
    
    
    /**
     * Get a list of the database names
     *
     * @mongodb.driver.manual reference/commands/listDatabases List Databases
     * @return an iterable containing all the names of all the databases
     * @since 3.0
     */
    public MongoIterable<String> listDatabaseNames() {
        return mongoClient.listDatabaseNames();
    }
    
    
    /**
     * Gets the list of databases
     *
     * @return the list of databases
     * @since 3.0
     */
    public ListDatabasesIterable<Document> listDatabases() {
        return mongoClient.listDatabases();
    }
    
    
    /**
     * Gets the list of databases
     *
     * @param clazz the class to cast the database documents to
     * @param <T>   the type of the class to use instead of {@code Document}.
     * @return the list of databases
     * @since 3.0
     */
    public <T> ListDatabasesIterable<T> listDatabases(final Class<T> clazz) {
        return mongoClient.listDatabases(clazz);
    }
    
    
    /**
     * @param databaseName the name of the database to retrieve
     * @return a {@code MongoDatabase} representing the specified database
     */
    public MongoDatabase getDatabase(final String databaseName) {
        /*
        AbstractMongoDatabaseImpl db = this.databases.get(databaseName);
        if(db!=null) {
            if(db instanceof CpMongoDatabaseImpl)
                return (CpMongoDatabaseImpl) db;
            else
                throw new IllegalStateException(databaseName + " has already been initialized and it is not type of " + MongoDatabase.class.getName());
        }
        
        MongoDatabase deprecatedDb = this.mongoClient.getDatabase(databaseName);
        CpMongoDatabaseImpl database = new CpMongoDatabaseImpl(databaseName, mongoClient, this, deprecatedDb);
        CpMongoDatabaseImpl temp = (CpMongoDatabaseImpl) this.databases.putIfAbsent(databaseName, database);
        if (temp != null) {
            return temp;
        }
        return database;
        */
        
   
        
        long dbID = this.dbCounter.incrementAndGet();
        MongoDatabase deprecatedDb = MongoClient.mongoClient.getDatabase(databaseName);
        CpMongoDatabaseImpl database = new CpMongoDatabaseImpl(dbID, databaseName, mongoClient, this, deprecatedDb);
        Log.trace("Putting database " + databaseName + " with id: " + dbID);
        this.databases.put(dbID, database);
        Log.trace("Now we have database instances: " + this.databases.size());
        return database;
    }
    
    
    
    //@Override
    public void applyWS() {
        
        MongoTransactionContext mongoTxnCtx = new MongoTransactionContext();
        
        try {
            mongoTxnCtx.setTid(LTMClient.getInstance().getConnection().getCtxId());
            mongoTxnCtx.setStartTimestamp(LTMClient.getInstance().getConnection().getStartTimestamp());
            mongoTxnCtx.setCommitTimestamp(LTMClient.getInstance().getConnection().getCommitTimestamp());
            
            applyWS(mongoTxnCtx);
        } catch (TransactionManagerException ex) {
            Log.error("Could not applyWS " + ex.getClass().getName() + ": " + ex.getMessage());
        }
    }
    
    @Override
    public long applyWS(TxnCtx txnctx) {
        long startApply = System.currentTimeMillis();
        Log.debug("Ordered to applyWS for tid: " + txnctx.getCtxId() + " at: " + txnctx.getCommitTimestamp());
        MongoTransactionContext mongoTxnCtx = new MongoTransactionContext(
                txnctx.getCtxId(), 
                txnctx.getStartTimestamp(), 
                txnctx.getCommitTimestamp());
        
        long result = applyWS(mongoTxnCtx);
        long endApply = System.currentTimeMillis();
        Log.debug("Total time to applyWS: {} msecs", (endApply-startApply));
        return result;
    }
    
    private long applyWS(MongoTransactionContext mongoTxnCtx) {
        long tid = mongoTxnCtx.getTid();
        try {
            if(this.openTransactionalDatabases.containsKey(tid)) {
                this.openTransactionalDatabases.get(tid)._applyWS(mongoTxnCtx);
                this.openTransactionalDatabases.remove(tid);
            } else {
                Log.warn("There is no open transaction associated with tid " + tid);
            }
        } catch (TransactionManagerException ex) {
            Log.error("Could not applyWS " + " for transaction tid: " + tid + " ... " + ex.getClass().getName() + ": " + ex.getMessage());
        } 
        
        //FIX WHAT TO RETURN
        return tid;
    }

    
    
    public void rollback() {
        MongoTransactionContext mongoTxnCtx = new MongoTransactionContext();        
        try {
            mongoTxnCtx.setTid(LTMClient.getInstance().getConnection().getCtxId());
            mongoTxnCtx.setStartTimestamp(LTMClient.getInstance().getConnection().getStartTimestamp());
            mongoTxnCtx.setCommitTimestamp(LTMClient.getInstance().getConnection().getCommitTimestamp());
            
            rollback(mongoTxnCtx);
        } catch (TransactionManagerException ex) {
            Log.error("Could not rollback " + ex.getClass().getName() + ": " + ex.getMessage());
        }
    }
    
    @Override
    public void rollback(TxnCtx txnctx) {
        MongoTransactionContext mongoTxnCtx = new MongoTransactionContext(
                txnctx.getCtxId(), 
                txnctx.getStartTimestamp(), 
                txnctx.getCommitTimestamp());
        
        rollback(mongoTxnCtx);
    }

    private void rollback(MongoTransactionContext mongoTxnCtx) {
        Log.info("MongoClient is invoked to rollback for: " + mongoTxnCtx.toString());
        Long tid = null;
        
        tid = mongoTxnCtx.getTid();
        if(this.openTransactionalDatabases.containsKey(tid)) {
            this.openTransactionalDatabases.get(tid)._rollback(mongoTxnCtx);
            this.openTransactionalDatabases.remove(tid);
        } else {
            Log.warn("There is no open transaction associated with tid " + tid);
        }
    }
    
    //@Override
    public byte[] getWS() throws DataStoreException {
        byte [] bytes = null;
        MongoTransactionContext mongoTxnCtx = new MongoTransactionContext();        
        try {
            mongoTxnCtx.setTid(LTMClient.getInstance().getConnection().getCtxId());
            mongoTxnCtx.setStartTimestamp(LTMClient.getInstance().getConnection().getStartTimestamp());
            mongoTxnCtx.setCommitTimestamp(LTMClient.getInstance().getConnection().getCommitTimestamp());
            
            bytes = getWS(mongoTxnCtx);
        } catch (TransactionManagerException ex) {
            Log.error("Could not get transactional context for getting the transaction's context");
        }
        return bytes;
    }
    
    @Override
    public byte[] getWS(TxnCtx txnctx) throws DataStoreException {
        if(Log.isDebugEnabled())
            Log.debug("Ordered to getWS for tid: " + txnctx.getCtxId() + " at: " + txnctx.getCommitTimestamp());
        
        //long startTmp = System.nanoTime();
        
        MongoTransactionContext mongoTxnCtx = new MongoTransactionContext(
                txnctx.getCtxId(), 
                txnctx.getStartTimestamp(), 
                txnctx.getCommitTimestamp());
        //long intermTmp = System.nanoTime();
        
        
        byte [] bytes = getWS(mongoTxnCtx);
        
        //long endTmp = System.nanoTime();
        //Log.info("Time to getBytes: {} and time to get ctx: {}", (endTmp-intermTmp), (intermTmp-startTmp));
        
        return bytes;
    }
    
    private byte[] getWS(MongoTransactionContext mongoTxnCtx) throws DataStoreException {

        byte [] bytes = null;
        DataBaseRecoveryWriteset dataBaseRecoveryWriteset = null;
        
        long tid = mongoTxnCtx.getTid();
        
        try {
            if(this.openTransactionalDatabases.containsKey(tid)) {
                AbstractMongoDatabaseImpl database = this.openTransactionalDatabases.get(tid);
                List<CollectionRecoveryWriteset> list = database._getWS();
                if(!list.isEmpty())
                    dataBaseRecoveryWriteset = new DataBaseRecoveryWriteset(database.databaseName, list);

            } else {
                Log.warn("There is no open transaction associated with tid " + tid);
            }

            //if emmpty writeset, then return null for the HTM to continue
            if(dataBaseRecoveryWriteset==null) 
                return null;

            bytes = SerializationUtils.serialize(dataBaseRecoveryWriteset);
        
        } catch (Exception ex) {
            String message = "Could not get writeset for transaction: " + tid + " ... " + ex.getClass().getName() + ": " + ex.getMessage();
            Log.error(message);
            throw new DataStoreException(message);
        }
        
        return bytes;
    }
    
    

    @Override
    public DataStoreId getDataStoreID() {
        return DataStoreId.MONGODB;
    }


    @Override
    public void redoWS(long l, byte[] bytes) throws DataStoreException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
        
        /*
        List<byte []> listWritesets = new ArrayList<>();
        listWritesets = ByteOperators.getBytesFromWriteSet(bytes);
        for(byte [] collectionWritesetBytes : listWritesets ) {
            ByteArrayInputStream byteIn = new ByteArrayInputStream(collectionWritesetBytes);
            ObjectInputStream in;
            try {
                in = new ObjectInputStream(byteIn);
                Map<String, Bson> collectionWriteset = (Map<String, Bson>) in.readObject();
                
                String databaseName = null;
                String collectionName = null;
                Map<ObjectId, Bson> writeset = new HashMap<>();
                Iterator<Entry<String, Bson>> it = collectionWriteset.entrySet().iterator();
                while(it.hasNext()) {
                    Entry<String, Bson> entry = it.next();
                    String [] values = entry.getKey().split(MongoMVCCFields.STRING_SEPARATOR);
                    if(databaseName==null) {
                        databaseName = values[0];
                        collectionName = values[1];
                    }
                    
                    ObjectId objId = new ObjectId(values[2]);
                    writeset.put(objId, entry.getValue());
                }
                
                
                MongoDatabase db = this.getDatabase(databaseName);
                CpMongoCollectionImpl<Document> coll = (CpMongoCollectionImpl<Document>) db.getCollection(collectionName);
                coll.recover(l, writeset);
                
            } catch (IOException | ClassNotFoundException ex) {
                throw new DataStoreException(ex.getMessage());
            }
        }
        */
        
    }

    @Override
    public void unleash(TxnCtx txnctx) throws DataStoreException {
        if(Log.isDebugEnabled())
            Log.debug("Mongoclient is invoked to unleash for: " + txnctx.toString());
        
        long tid = txnctx.getCtxId();
        try {
            if(this.openTransactionalDatabases.containsKey(tid)) {
                this.openTransactionalDatabases.get(tid)._unleash(txnctx);
                this.openTransactionalDatabases.remove(tid);
            }
        } catch(Exception ex) {
            Log.error("Exception when trying to unleash transactionid: {}. {}: {}", tid, ex.getClass().getName(), ex.getMessage());
            throw new DataStoreException(ex.getMessage());
        }
    }
 
    @Override
    public void notify(TxnCtx txnctx) throws DataStoreException {
        if(Log.isDebugEnabled())
            Log.debug("MongoClient is invoked to notify for: " + txnctx.toString());
    }

    @Override
    public void garbageCollect(long l) {
        //System.out.println("Garbage collection was invoked with timestamp: " + l);
        if(Log.isDebugEnabled())
            Log.debug("Garbage collection was invoked with timestamp: {}", l);
    }

    
    
}