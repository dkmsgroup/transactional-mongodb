package com.mongodb;

import static com.mongodb.BulkWriteHelper.translateBulkWriteResult;
import static com.mongodb.BulkWriteHelper.translateWriteRequestsToNew;
import com.mongodb.MapReduceCommand.OutputType;
import com.mongodb.binding.ReadBinding;
import com.mongodb.binding.WriteBinding;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.operation.MixedBulkTransactionalWriteOperation;
import com.mongodb.operation.OperationExecutor;
import com.mongodb.operation.ReadOperation;
import com.mongodb.operation.WriteOperation;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import eu.coherentpaas.mongodb.exceptions.MongoMVCCException;
import eu.coherentpaas.mongodb.utils.ByteOperators;
import eu.coherentpaas.mongodb.utils.MongoMVCCFindModifier;
import eu.coherentpaas.mongodb.utils.MongoMVCCRemover;
import eu.coherentpaas.mongodb.utils.MongoMVCCUpdator;
import eu.coherentpaas.mongodb.utils.MongoTransactionContext;
import eu.coherentpaas.mongodb.utils.VersionQueryUtils;
import eu.coherentpaas.mongodb.utils.WrapperMethodResponseType;
import eu.coherentpaas.mongodb.utils.WrapperSupportedMethod;
import eu.coherentpaas.mongodb.utils.WriteSetToApply;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.exception.CoherentPaaSException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.bson.codecs.Codec;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public final class DBCollection extends AbstractMongoCollectionImpl implements IDBCollection {
    private static final Logger Log = LoggerFactory.getLogger(DBCollection.class);
    
    private final DeprecatedDBCollection _dbDepracedCollection;
    private List<String> _createdIndexes;
    private volatile CompoundDBObjectCodec objectCodec;
    private DBCollectionObjectFactory objectFactory;
   
    
    
    /**
     * constructor of this class
     * sets the collection's name and the DBCollection object of MongoDB driver
     *
     * @param name
     * @param depracedCollection
     * @param mongoTransactionalContext
     * @param mongoMVCCDB
     * @dochub MongoMVCCCollection
     */
    protected DBCollection(String name, DeprecatedDBCollection depracedCollection, MongoTransactionContext mongoTransactionalContext, AbstractMongoDatabaseImpl mongoMVCCDB) {
        super(mongoMVCCDB, name, mongoTransactionalContext);
        this._dbDepracedCollection = depracedCollection;      
        this.objectFactory = new DBCollectionObjectFactory();
        this.objectCodec = new CompoundDBObjectCodec(getDefaultDBObjectCodec());
    }
    
    
    /**
     * constructor of this class
     * sets the collection's name and the DBCollection object of MongoDB driver
     *
     * @param name
     * @param depracedCollection
     * @param mongoTransactionalContext
     * @param mongoMVCCDB
     * @param checkForDefaultIndex
     * @dochub MongoMVCCCollection
     */
    protected DBCollection(String name, DeprecatedDBCollection depracedCollection, MongoTransactionContext mongoTransactionalContext, AbstractMongoDatabaseImpl mongoMVCCDB, boolean checkForDefaultIndex) {
        super(mongoMVCCDB, name, mongoTransactionalContext);
        this._dbDepracedCollection = depracedCollection;
        
        if(checkForDefaultIndex)
            createDefaultIndex();        
    }
    
    
    /**
     *   Creates the default compound index on dataID, cmtTmstmp and nxtCmtTimstmp
     *
     */
    private void createDefaultIndex() {
        List<DBObject> list = this._dbDepracedCollection.getIndexInfo();
        boolean hasDefaultIndex = false;
        
        //check if default index already existis
        for(DBObject index : list) {
            if(((BasicDBObject) index).get("name").equals(MongoMVCCFields.DEFAULT_COMPOUND_INDEX)) {
                hasDefaultIndex = true;
                break;
            }
        }
        
        //if not, then create a new default index on dataID, commitTimestamp and nextCommitTimestamp
        if(!hasDefaultIndex) {
            BasicDBObject indexes = new BasicDBObject(MongoMVCCFields.DATA_ID, 1)
                    .append(MongoMVCCFields.COMMIT_TIMESTAMP, 1)
                    .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, 1);
            
            this._dbDepracedCollection.createIndex(indexes, MongoMVCCFields.DEFAULT_COMPOUND_INDEX);
        }
        
    }
    
    /*
    @Override
    protected ConcurrentMap _getWriteSet() throws MongoException {
        
        _checkClose();
        try {
            _checkTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        try {
            this.writeset.clear();
            try (DeprecatedDBCursor cursor = this._dbDepracedCollection.find(VersionQueryUtils.getAllPrivateVersions(this.mongoTransactionalContext.getTid()))) {
                while(cursor.hasNext()) {
                    BasicDBObject object = (BasicDBObject) cursor.next();
                    this.writeset.putIfAbsent(object.get(MongoMVCCFields.DATA_ID), object);
                }
            }
        } catch(Exception ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        return this.writeset;
    }
    */
    
    @Override
    protected void _applyWriteSet(long tid, long commitTmp) throws MongoException {
        
        //_checkClose();
        //no check. commit is asyncrhonous and collection might be already closed when ordered to apply the WS
        
        //get the ids of the documents to by 'applied' from the local cache
        WriteSetToApply writeSetToApply = pendingWritesetsToApply.get(tid);
        if((writeSetToApply==null)||(writeSetToApply.getDbList().isEmpty()))
            return; //nothing to apply
        
        //FIRST UPDATE THE OLD AND THEN INSERT THE NEW
        //otherwise the new created versions will be updated with the new nextCommitTimestamp
        BasicDBList dbList = writeSetToApply.getDbList();
        BasicDBObject query;
        BasicDBObject updatePrivate;
        WriteResult writeResult;
        try {
            
            if(writeSetToApply.isWithUpdates()) {
                //if has modifications, then it must update older versions
                query = new BasicDBObject(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, "")
                                            .append(MongoMVCCFields.COMMIT_TIMESTAMP, new BasicDBObject("$ne", ""))
                                            .append(MongoMVCCFields.DATA_ID, new BasicDBObject("$in", dbList));
                updatePrivate = new BasicDBObject("$set", new BasicDBObject(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, commitTmp));
                writeResult = this._dbDepracedCollection.updateMulti(query, updatePrivate);
                if(Log.isDebugEnabled())
                    Log.debug("Older versions affected: " + writeResult.getN() + "");
            }

            query = VersionQueryUtils.getAllPrivateVersions(tid);
            updatePrivate = new BasicDBObject("$set", new BasicDBObject(MongoMVCCFields.COMMIT_TIMESTAMP, commitTmp));

            writeResult = this._dbDepracedCollection.updateMulti(query, updatePrivate);
            if(Log.isDebugEnabled())
                Log.debug("New versions added: " + writeResult.getN() + "");
            
            //remove now from the local cache
            pendingWritesetsToApply.remove(tid);
            
        } catch(Exception ex) {
            Log.error("Error applying writeset. {}: {}", ex.getClass().getName(), ex.getMessage());
            throw new MongoException(ex.getMessage(), ex);
        }
    }
    
    @Override
    protected void _rollback(long tid) throws MongoException {
        
        //_checkClose();
        
        try {
            BasicDBObject query = VersionQueryUtils.getAllPrivateVersions(tid);
            WriteResult writeResult = this._dbDepracedCollection.remove(query);
            if(Log.isDebugEnabled())
                Log.debug("Private versions removed: " + writeResult.getN() + "");
            
            //remove now from local cache
            //writeset was not cleared via the getWS, as the getWS was never invoked due to the rollback
            this.writeset.clear();
            
        } catch(Exception ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
    }
    
    /**
     * Inserts a document temporarily to the transaction's private write-set 
     * if doc have an _id, it will be ignored
     * the _id is auto-generated and can be stored at the "data_id" key of the document
     *
     * @param object
     * @return number of items currently at the write-set
     * @dochub insert
     */
    private WriteResult _insert(List<DBObject> objects, WriteConcern concern, InsertOptions options, DBEncoder encoder) {
        
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        } 
        
        WriteResult writeResult = null;
        
        Iterator<DBObject> it = objects.iterator();
        while(it.hasNext()) {
            BasicDBObject document = (BasicDBObject) it.next();
        
            //automatically set the primary key (default type of ObjectId)
            if(document.containsField(MongoMVCCFields._ID)) {
                //if already contains in _id, put it to its right place
                Object _id = document.get(MongoMVCCFields._ID);
                document.append(MongoMVCCFields.DATA_ID, _id);
            } else {
                //otherwise put a new one
                document.append(MongoMVCCFields.DATA_ID, new ObjectId());
            }

            //add the tid and null timestamps
            document.append(MongoMVCCFields.TID, this.mongoTransactionalContext.getTid())
                    .append(MongoMVCCFields.COMMIT_TIMESTAMP, "")
                    .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, "");

            //add the concatenation of its key+tid
            document.append(MongoMVCCFields._ID, ByteOperators.getBytes(document.get(MongoMVCCFields.DATA_ID), this.mongoTransactionalContext.getTid()));
            
            this.writeset.putIfAbsent(document.get(MongoMVCCFields.DATA_ID), document);
        }
        
        try {
            if(options!=null)
                writeResult = this._dbDepracedCollection.insert(objects, options);
            else {
                if(encoder!=null) {
                    writeResult = this._dbDepracedCollection.insert(objects, concern, encoder);
                } else {
                    if(concern!=null)
                        writeResult = this._dbDepracedCollection.insert(objects, concern);
                    else
                        writeResult = this._dbDepracedCollection.insert(objects);
                }
            }
        } catch(Exception ex) {
            Log.error("exception when trying to insert document(s)");
            try {
                //rollback the transaction
                LTMClient.getInstance().getConnection().rollback();
            } catch (TransactionManagerException ex1) {
                throw new MongoException(ex1.getMessage(), ex1);
            }
        }
        
        if(this.isAutoCommit.get())
            autoCommit();
        
        return writeResult;
        
    }
    
    
    
    protected WriteResult _update(DBObject q, DBObject o, boolean upsert, boolean multi, WriteConcern concern, DBEncoder encoder) {
        
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        q = fixDataModificationQuery(q);
        
        MongoMVCCUpdator updator = new MongoMVCCUpdator(
                _dbDepracedCollection, 
                this.writeset,
                this.mongoTransactionalContext,
                new BasicDBObject(q.toMap()), 
                new BasicDBObject(o.toMap()), 
                upsert, multi, concern, encoder);
        
        WriteResult result = null;
        try {
            result = updator.update();
        } catch (MongoMVCCException | TransactionManagerException ex) {
            if(ex instanceof TransactionManagerException) {
                throw new MongoException(ex.getMessage(), ex);
                /*
                try {
                    fireConflictResolutionDetectedEvent();
                } catch (TransactionManagerException ex1) {
                    throw new MongoException(ex1.getMessage(), ex1);
                }
                */
            }
            
            if(ex instanceof MongoMVCCException) {
                Log.error("Error when trying to update: " + ex.getMessage());
                try {
                    //rollback the transaction
                    LTMClient.getInstance().getConnection().rollback();
                } catch (TransactionManagerException ex1) {
                    throw new MongoException(ex1.getMessage(), ex1);
                }
            }
        } 
        
        if(!this.hasUpdates)
            this.hasUpdates = true;
        
        if(this.isAutoCommit.get())
            autoCommit();
        
        
        return result;
    }
    
    protected WriteResult _remove(DBObject q, WriteConcern concern, DBEncoder encoder) {
        
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        //no need to remove the id, the user should provide the whole document
        q = fixDataModificationQuery(q);
        
        MongoMVCCRemover remover = new MongoMVCCRemover(
                _dbDepracedCollection, 
                this.writeset,
                this.mongoTransactionalContext,
                new BasicDBObject(q.toMap()),  
                concern, encoder);
        
        WriteResult result = null;
        try {
            result = remover.remove();
        } catch (TransactionManagerException ex) {
            if(ex instanceof TransactionManagerException) {
                throw new MongoException(ex.getMessage(), ex);
                /*
                try {
                    fireConflictResolutionDetectedEvent();
                } catch (TransactionManagerException ex1) {
                    throw new MongoException(ex1.getMessage(), ex1);
                }
                */
            }
            
        } 
        
        if(!this.hasUpdates)
            this.hasUpdates = true;
        
        if(this.isAutoCommit.get())
            autoCommit();
        
        return result;
    }

    
    protected DBObject _findAndModify(DBObject query, DBObject fields, DBObject sort, boolean remove, DBObject update, boolean returnNew, boolean upsert, long maxTime, TimeUnit maxTimeUnit) {
        
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        if (remove && !(update == null || update.keySet().isEmpty() || returnNew))
            throw new MongoException("FindAndModify: Remove cannot be mixed with the Update, or returnNew params!");

        final MongoMVCCFindModifier findModifier = new MongoMVCCFindModifier(
                writeset, 
                fixDataModificationQuery(query), 
                fields, 
                sort, 
                update, remove, upsert, returnNew, 
                this.mongoTransactionalContext,
                _dbDepracedCollection);
        BasicDBObject result = null;
        if(maxTime>0) {
            ExecutorService executor = Executors.newSingleThreadExecutor();
            Callable<BasicDBObject> worker = new  Callable<BasicDBObject>() {
                @Override
                public BasicDBObject call() throws Exception {
                    try {
                        return (BasicDBObject) findModifier.findAndMofify();
                    } catch (MongoMVCCException | TransactionManagerException ex) {
                        if(ex instanceof TransactionManagerException) {
                            Log.warn("Conflict Resolution Detected");
                            throw new MongoException(ex.getMessage(), ex);
                            /*
                            try {
                                fireConflictResolutionDetectedEvent();
                            } catch (TransactionManagerException ex1) {
                                throw new MongoException(ex1.getMessage(), ex1);
                            }
                            */
                        }

                        if(ex instanceof MongoMVCCException) {
                            Log.error("Error when trying to update: " + ex.getMessage());
                            try {
                                //rollback the transaction
                                LTMClient.getInstance().getConnection().rollback();
                            } catch (TransactionManagerException ex1) {
                                throw new MongoException(ex1.getMessage(), ex1);
                            }
                        }
                    }
                    
                    return null;
                }
            };
            
            Future<BasicDBObject> future = executor.submit(worker);
            try {
                result = future.get(maxTime, maxTimeUnit);
            } catch (    InterruptedException | ExecutionException | TimeoutException ex) {
                if(ex instanceof InterruptedException) {
                    Log.error("Interrupted exception: " + ex.getMessage());
                    Thread.currentThread().interrupt();
                } else {
                    Log.error(ex.getClass().getName() + " thrown." + ex.getMessage());
                    throw new MongoException(ex.getMessage(), ex);
                }
            } finally {
                if(!executor.isShutdown())
                    executor.shutdownNow();
            }
            
            
        } else {
            try {
                result = (BasicDBObject) findModifier.findAndMofify();
            } catch (MongoMVCCException | TransactionManagerException ex) {
                if(ex instanceof TransactionManagerException) {
                    Log.warn("Conflict Resolution Detected");
                    throw new MongoException(ex.getMessage(), ex);
                    /*
                    try {
                        fireConflictResolutionDetectedEvent();
                    } catch (TransactionManagerException ex1) {
                        throw new MongoException(ex1.getMessage(), ex1);
                    }
                    */
                }

                if(ex instanceof MongoMVCCException) {
                    Log.error("Error when trying to update: " + ex.getMessage());
                    try {
                        //rollback the transaction
                        LTMClient.getInstance().getConnection().rollback();
                    } catch (TransactionManagerException ex1) {
                        throw new MongoException(ex1.getMessage(), ex1);
                    }
                }
            } 
        }
        
        if(!this.hasUpdates)
            this.hasUpdates = true;
        
        if(this.isAutoCommit.get())
            autoCommit();
        
        return result;
    }
    
    protected long _getCount(DBObject query, DBObject fields, long limit, long skip, ReadPreference readPreference) {
        
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        long result = 0;
        
        if((fields==null)&&(readPreference==null))
            result = this._dbDepracedCollection.getCount(query);
        else if ((fields==null)&&(readPreference!=null))
            result = this._dbDepracedCollection.getCount(readPreference);
        else if((limit<1)&&(readPreference==null))
            result = this._dbDepracedCollection.getCount(query, fields);
        else if((limit<1)&&(readPreference!=null))
            result = this._dbDepracedCollection.getCount(query, fields, readPreference);
        else 
            result = this._dbDepracedCollection.getCount(query, fields, limit, skip, readPreference);
        
        /*
        if(limit<=0)
            limit = Long.MAX_VALUE;
        
        DBObject queryObject = fixSelectQuery(query);
        try (DeprecatedDBCursor cursor = this._dbDepracedCollection.find(queryObject)) {
            while(cursor.hasNext()) {
                BasicDBObject documentFound = (BasicDBObject) cursor.next();
                //if document is already in the writeset, then check if this version is outdated or current (private)
                if(this.writeset.containsKey(documentFound.getObjectId(MongoMVCCFields.DATA_ID))) {
                    if(documentFound.getLong(MongoMVCCFields.TID)==this.mongoTransactionalContext.getTid())
                        result++;
                    else 
                        //there's an updated version for this document, do not count it
                        continue;
                }
                else
                    //there is no updated versin for this document
                    result++;
                
                //skip the first 'skip'
                if(skip>0) {
                    result--;
                    skip--;
                }
                
                //find until the limit
                if(result==limit)
                    break;
            }
        }
        */
        
        if(this.isAutoCommit.get())
            autoCommit();
        
        return result;
    }
    
    
    @SuppressWarnings("empty-statement")
    protected List _distinct(String keys, DBObject q) {
        
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        List result = new BasicDBList();
        
        if((keys==null)||keys.equals(""))
            throw new MongoException("keys does not contain any value");
        
        DBObject queryObject = fixSelectQuery(q);
        BasicDBObject queryKeys = new BasicDBObject(keys, 1)
                                        .append(MongoMVCCFields.DATA_ID, 1)
                                        .append(MongoMVCCFields.TID, 1);
        try (DeprecatedDBCursor cursor = this._dbDepracedCollection.find(queryObject, queryKeys)) {
            while(cursor.hasNext()) {
                BasicDBObject documentFound = (BasicDBObject) cursor.next();
                //if document is already in the writeset, then check if this version is outdated or current (private)
                if(this.writeset.containsKey(documentFound.getObjectId(MongoMVCCFields.DATA_ID))) {
                    if(documentFound.getLong(MongoMVCCFields.TID)==this.mongoTransactionalContext.getTid())
                        if(!result.contains(documentFound.get(keys)))
                            result.add(documentFound.get(keys));
                    else 
                        //there's an updated version for this document, do not count it
                        ;//continue;/
                } else {
                    //document is from the public versions --no updated version found in the private writeset
                    if(!result.contains(documentFound.get(keys)))
                            result.add(documentFound.get(keys));
                }
                
            }
        }
        
        if(this.isAutoCommit.get())
            autoCommit();
        
        return result;
    }
    
    /**
     * Find a collection that is prefixed with this collection's name. A typical use of this might be
     * <pre>{@code
     *    DepracedDBCollection users = mongo.getCollection( "wiki" ).getCollection( "users" );
     * }</pre>
     * Which is equivalent to
     * <pre>{@code
     *   DepracedDBCollection users = mongo.getCollection( "wiki.users" );
     * }</pre>
     *
     * @param n the name of the collection to find
     * @return the matching collection
     */
    @Override
     public IDBCollection getCollection(String n) {
        return  ((DB) this.abstractDB).getCollection(this.collectionName + "." + n);
    }
    
    
    /**
     * returns the name of this collection
     *
     * @return the name of this collection
     * @dochub getName
     */
    @Override
    public String getName() {
        return this.collectionName;
    }
    
    /**
     * Get the full name of a collection, with the database name as a prefix.
     *
     * @return the name of a collection
     * @mongodb.driver.manual reference/glossary/#term-namespace Namespace
     */
    @Override
    public String getFullName(){
        return getDabaseName() + "." + getName();
    }
    
        
    /**
     * Returns the database this collection is a member of.
     *
     * @return this collection's database
     * @mongodb.driver.manual reference/glossary/#term-database Database
     */ 
    @Override
    public IDB getDB(){
        return (DB) this.abstractDB;
    }
    
    
    /**
     * Set the {@link WriteConcern} for this collection. Will be used for writes to this collection. Overrides any setting of write concern
     * at the DB level.
     *
     * @param writeConcern WriteConcern to use
     * @mongodb.driver.manual core/write-concern/ Write Concern
     */
    @Override
    public void setWriteConcern(final WriteConcern writeConcern) {
        _dbDepracedCollection.setWriteConcern(writeConcern);
    }
    
    
    /**
     * Get the {@link WriteConcern} for this collection.
     *
     * @return the default write concern for this collection
     * @mongodb.driver.manual core/write-concern/ Write Concern
     */
    @Override
    public WriteConcern getWriteConcern(){
        return _dbDepracedCollection.getWriteConcern();
    }
    
    
    /**
     * Sets the {@link ReadPreference} for this collection. Will be used as default for reads from this collection; overrides DB and
     * Connection level settings. See the documentation for {@link ReadPreference} for more information.
     *
     * @param preference ReadPreference to use
     * @mongodb.driver.manual core/read-preference/ Read Preference
     */
    @Override
    public void setReadPreference(final ReadPreference preference) {
        _dbDepracedCollection.setReadPreference(preference);
    }
    
    /**
     * Gets the {@link ReadPreference}.
     *
     * @return the default read preference for this collection
     * @mongodb.driver.manual core/read-preference/ Read Preference
     */
    @Override
    public ReadPreference getReadPreference(){ 
        return _dbDepracedCollection.getReadPreference();
    }
    
        
    /**
     * Adds the given flag to the default query options.
     *
     * @param option value to be added
     * @mongodb.driver.manual reference/method/cursor.addOption/#flags Query Flags
     */
    @Override
    public void addOption(final int option) {
        _dbDepracedCollection.addOption(option);
    }
    
    /**
     * Sets the default query options, overwriting previous value.
     *
     * @param options bit vector of query options
     * @mongodb.driver.manual reference/method/cursor.addOption/#flags Query Flags
     */
    @Override
    public void setOptions(final int options) {
        _dbDepracedCollection.setOptions(options);
    }

    /**
     * Resets the default query options
     *
     * @mongodb.driver.manual reference/method/cursor.addOption/#flags Query Flags
     */
    @Override
    public void resetOptions(){
        _dbDepracedCollection.resetOptions();
    }

    /**
     * Gets the default query options
     *
     * @return bit vector of query options
     * @mongodb.driver.manual reference/method/cursor.addOption/#flags Query Flags
     */
    @Override
    public int getOptions(){
        return _dbDepracedCollection.getOptions();
    }

    /**
     * Set a customer decoder factory for this collection.  Set to null to use the default from MongoOptions.
     *
     * @param fact the factory to set.
     */
    @Override
    public synchronized void setDBDecoderFactory(DBDecoderFactory fact) {
        _dbDepracedCollection.setDBDecoderFactory(fact);
    }

    /**
     * Get the decoder factory for this collection.  A null return value means that the default from MongoOptions is being used.
     *
     * @return the factory
     */
    @Override
    public synchronized DBDecoderFactory getDBDecoderFactory() {
        return _dbDepracedCollection.getDBDecoderFactory();
    }

    /**
     * Set a customer encoder factory for this collection.  Set to null to use the default from MongoOptions.
     *
     * @param fact the factory to set.
     */
    @Override
    public synchronized void setDBEncoderFactory(DBEncoderFactory fact) {
        _dbDepracedCollection.setDBEncoderFactory(fact);
    }

    /**
     * Get the encoder factory for this collection.  A null return value means that the default from MongoOptions is being used.
     *
     * @return the factory
     */
    @Override
    public synchronized DBEncoderFactory getDBEncoderFactory() {
        return _dbDepracedCollection.getDBEncoderFactory();
    }
    
    
    
    /**
     * Sets a default class for objects in this collection; null resets the class to nothing.
     *
     * @param c the class
     * @throws IllegalArgumentException if {@code c} is not a DBObject
     */
    @Override
    public void setObjectClass(Class c) {
        _dbDepracedCollection.setObjectClass(c);
    }

    
    /**
     * Sets the internal class for the given path in the document hierarchy
     *
     * @param path the path to map the given Class to
     * @param c    the Class to map the given path to
     */
    @Override
    public void setInternalClass(String path, Class c) {
        _dbDepracedCollection.setInternalClass(path, c);
    }
    
    
    /**
     * Forces creation of an ascending index on a field with the default options.
     *
     * @param name name of field to index on
     * @throws MongoException
     * @mongodb.driver.manual /administration/indexes-creation/ Index Creation Tutorials
     */
    @Override
    public void createIndex( final String name ){
        createIndex( new BasicDBObject( name , 1 ) );
    }
    
    
    /**
     * calls {@link DBCollection#createIndex(com.mongodb.DBObject, com.mongodb.DBObject)} with default index options
     * It does exactly what the MongoBD Java driver uses to do
     * @param keys an object with a key set of the fields desired for the index
     * @throws MongoException
     */
    @Override
    public void createIndex(final DBObject keys)  {
        _checkClose();
        _dbDepracedCollection.createIndex(keys);
    }

    
    /**
     * Forces creation of an index on a set of fields, if one does not already exist.
     * It does exactly what the MongoBD Java driver uses to do
     * @param keys
     * @param options
     * @throws MongoException
     */
    @Override
    
    public void createIndex(DBObject keys, DBObject options) {
        _checkClose();
        _dbDepracedCollection.createIndex(keys, options);
    }
    
    /**
     * Forces creation of an index on a set of fields, if one does not already exist.
     *
     * @param keys   a document that contains pairs with the name of the field or fields to index and order of the index
     * @param name   an identifier for the index. If null or empty, the default name will be used.
     * @throws MongoException
     * @mongodb.driver.manual /administration/indexes-creation/ Index Creation Tutorials
     */
    @Override
    public void createIndex( DBObject keys , String name ){
        createIndex( keys , name,  false);
    }
    
    
    /**
     * Forces creation of an index on a set of fields, if one does not already exist.
     *
     * @param keys   a document that contains pairs with the name of the field or fields to index and order of the index
     * @param name   an identifier for the index. If null or empty, the default name will be used.
     * @param unique if the index should be unique
     * @throws MongoException
     * @mongodb.driver.manual /administration/indexes-creation/ Index Creation Tutorials
     */
    @Override
    public void createIndex( DBObject keys , String name , boolean unique ){
        _checkClose();
        
        DBObject options = defaultOptions( keys );
        if (name != null && name.length()>0)
            options.put( "name" , name );
        if ( unique )
            options.put( "unique" , Boolean.TRUE );
        createIndex( keys , options );
    }
    
    DBObject defaultOptions( DBObject keys ){
        DBObject o = new BasicDBObject();
        o.put( "name" , genIndexName( keys ) );
        o.put( "ns" , this.abstractDB.getName() + "." + this.collectionName );
        return o;
    }
    
    /**
     * Convenience method to generate an index name from the set of fields it is over.
     * @param keys the names of the fields used in this index
     * @return a string representation of this index's fields
     *
     * @deprecated This method is NOT a part of public API and will be dropped in 3.x versions.
     */
    @Deprecated
    public static String genIndexName( DBObject keys ){
        StringBuilder name = new StringBuilder();
        for ( String s : keys.keySet() ){
            if ( name.length() > 0 )
                name.append( '_' );
            name.append( s ).append( '_' );
            Object val = keys.get( s );
            if ( val instanceof Number || val instanceof String )
                name.append( val.toString().replace( ' ', '_' ) );
        }
        return name.toString();
    }
    
    
    
    
    
    /**
     * gets the collections statistics ("collstats" command)
     * @param name
     * @throws MongoException
     */
    @Override
    public void dropIndex(String name) {
        _checkClose();
        _dbDepracedCollection.dropIndex(name);
    }
    
    /**
     * Drops an index from this collection.  The DBObject index parameter must match the specification of the index to drop, i.e. correct
     * key name and type must be specified.
     *
     * @param keys the specification of the index to drop
     * @throws MongoException if the index does not exist
     * @mongodb.driver.manual core/indexes/ Indexes
     */
     @Override
    public void dropIndex(DBObject keys) {
         _checkClose();
        _dbDepracedCollection.dropIndex(keys);
    }

    
    /**
     * Drops all indices from this collection
     * @throws MongoException
     */
     @Override
    public void dropIndexes() {
         _checkClose();
        _dbDepracedCollection.dropIndexes();
    }

    
    /**
     * Drops an index from this collection
     * @param name the index name
     * @throws MongoException
     */
     @Override
    public void dropIndexes(String name) {
         _checkClose();
        _dbDepracedCollection.dropIndexes(name);
    }


    /**
     *   Return a list of the indexes for this collection.  Each object
     *   in the list is the "info document" from MongoDB
     *
     *   @return list of index documents
     *   @throws MongoException
     */
     @Override
    public List<DBObject> getIndexInfo() {
         _checkClose();
        return _dbDepracedCollection.getIndexInfo();
    }
    
    
    /**
     * Override MongoDB's default index selection and query optimization process.
     *
     * @param indexes list of indexes to "hint" or force MongoDB to use when performing the query.
     * @mongodb.driver.manual reference/operator/meta/hint/ $hint
     */
     @Override
    public void setHintFields(final List<DBObject> indexes) {
        _dbDepracedCollection.setHintFields(indexes);
    }
    
    
    @Override
    public void drop(){ 
         _checkClose();
        this._dbDepracedCollection.drop();
        close();
    }
    
    /**
     * Get the number of documents in the collection.
     *
     * @return the number of documents
     * @throws MongoException
     * @mongodb.driver.manual reference/command/count/ Count
     */
     @Override
     @WrapperSupportedMethod(name="count", arguments = "0", responseType = WrapperMethodResponseType.LONG)
     public long count(){         
          return (count(null));
     }
    
    /**
     * Get the count of documents in collection that would match a criteria.
     *
     * @param query specifies the selection criteria
     * @return the number of documents that matches selection criteria
     * @throws MongoException
     * @mongodb.driver.manual reference/command/count/ Count
     */
     @Override
     @WrapperSupportedMethod(name="count", arguments = "1", responseType = WrapperMethodResponseType.LONG)
     public long count(DBObject query){
          return count(query, null);        
     }
    
    /**
     * Get the count of documents in collection that would match a criteria.
     *
     * @param query     specifies the selection criteria
     * @param readPrefs {@link ReadPreference} to be used for this operation --this will be ignored
     * @return the number of documents that matches selection criteria
     * @throws MongoException
     * @mongodb.driver.manual reference/command/count/ Count
     */
     @Override
    public long count(DBObject query, ReadPreference readPrefs ){
        return getCount(query, null, 0, 0);
    }
    
    /**
     * Get the count of documents in a collection.  Calls {@link DepracedDBCollection#getCount(com.mongodb.DBObject, com.mongodb.DBObject)} with an
     * empty query and null fields.
     *
     * @return the number of documents in the collection
     * @throws MongoException
     * @mongodb.driver.manual reference/command/count/ Count
     */
     @Override
     @WrapperSupportedMethod(name="getCount", arguments = "0", responseType = WrapperMethodResponseType.LONG)
     public long getCount(){
        return getCount(new BasicDBObject());
    }
    
    /**
     * Get the count of documents in a collection. Calls {@link DepracedDBCollection#getCount(com.mongodb.DBObject, com.mongodb.DBObject,
     * com.mongodb.ReadPreference)} with empty query and null fields.
     *
     * @param readPrefs {@link ReadPreference} to be used for this operation --tihs will be ignored
     * @return the number of documents that matches selection criteria
     * @throws MongoException
     * @mongodb.driver.manual reference/command/count/ Count
     */
     @Override
    public long getCount(ReadPreference readPrefs){ 
        return getCount(new BasicDBObject(), null);
    }
    
    /**
     * Get the count of documents in collection that would match a criteria. Calls {@link DepracedDBCollection#getCount(com.mongodb.DBObject,
     * com.mongodb.DBObject)} with null fields.
     *
     * @param query specifies the selection criteria
     * @return the number of documents that matches selection criteria
     * @throws MongoException
     * @mongodb.driver.manual reference/command/count/ Count
     */
     @Override
     @WrapperSupportedMethod(name="getCount", arguments = "1", responseType = WrapperMethodResponseType.LONG)
     public long getCount(DBObject query){
         return _getCount(query, null, 0, 0, null);
     }
    
    /**
     * Get the count of documents in collection that would match a criteria. Calls {@link DepracedDBCollection#getCount(com.mongodb.DBObject,
     * com.mongodb.DBObject, long, long)} with limit=0 and skip=0
     *
     * @param query  specifies the selection criteria
     * @param fields this is ignored
     * @return the number of documents that matches selection criteria
     * @throws MongoException
     * @mongodb.driver.manual reference/command/count/ Count
     */
     @Override
    public long getCount(DBObject query, DBObject fields) {
        return _getCount(query, fields, 0, 0, null);        
    }
    
    /**
     * Get the count of documents in collection that would match a criteria.  Calls {@link DepracedDBCollection#getCount(com.mongodb.DBObject,
     * com.mongodb.DBObject, long, long, com.mongodb.ReadPreference)} with limit=0 and skip=0
     *
     * @param query     specifies the selection criteria
     * @param fields    this is ignored
     * @param readPrefs {@link ReadPreference} to be used for this operation   --this will be ignored
     * @return the number of documents that matches selection criteria
     * @throws MongoException
     * @mongodb.driver.manual reference/command/count/ Count
     */
     @Override
    public long getCount(DBObject query, DBObject fields, ReadPreference readPrefs){
        return _getCount(query, fields, 0, 0, readPrefs);       
    }
    
    
    /**
     * Get the count of documents in collection that would match a criteria.  Calls {@link DepracedDBCollection#getCount(com.mongodb.DBObject,
     * com.mongodb.DBObject, long, long, com.mongodb.ReadPreference)} with the DepracedDBCollection's ReadPreference
     *
     * @param query          specifies the selection criteria
     * @param fields     this is ignored
     * @param limit          limit the count to this value
     * @param skip           number of documents to skip
     * @return the number of documents that matches selection criteria
     * @throws MongoException
     * @mongodb.driver.manual reference/command/count/ Count
     */
     @Override
    public long getCount(DBObject query, DBObject fields, long limit, long skip){
        return _getCount(query, fields, limit, skip, null);       
    }
    
    /**
     * Get the count of documents in collection that would match a criteria.
     *
     * @param query     specifies the selection criteria
     * @param fields    this is ignored
     * @param limit     limit the count to this value
     * @param skip      number of documents to skip
     * @param readPrefs {@link ReadPreference} to be used for this operation  --this will be ignored
     * @return the number of documents that matches selection criteria
     * @throws MongoException
     * @mongodb.driver.manual reference/command/count/ Count
     */
     @Override
    public long getCount(DBObject query, DBObject fields, long limit, long skip, ReadPreference readPrefs ){
        return _getCount(query, fields, limit, skip, readPrefs); 
    }
    
    
    /**
     * Calls {@link DepracedDBCollection#rename(java.lang.String, boolean)} with dropTarget=false
     * @param newName new collection name (not a full namespace)
     * @return the new collection
     * @throws MongoException
     */
     @Override
     @Deprecated
    public IDBCollection rename( String newName ){
        throw new UnsupportedOperationException(("cannot rename collection. collection's name is stored at logs"));
    }
    
    @Deprecated
    @Override
    public IDBCollection rename(String newName, boolean dropTarget) {
        throw new UnsupportedOperationException(("cannot rename collection. collection's name is stored at logs"));
    }
     
    
    /**
     * The collStats command returns a variety of storage statistics for a given collection
     *
     * @return a CommandResult containing the statistics about this collection
     * @mongodb.driver.manual /reference/command/collStats/ collStats command
     */
    @Override
    public CommandResult getStats() {
        return _dbDepracedCollection.getStats();
    }
    
    
    /**
     * Checks whether this collection is capped
     *
     * @return true if this is a capped collection
     * @throws MongoException
     * @mongodb.driver.manual /core/capped-collections/#check-if-a-collection-is-capped Capped Collections
     */
    @Override
    public boolean isCapped() {
        return _dbDepracedCollection.isCapped();
    }
    
    /**
     * Queries for an object in this collection.
     * @return an iterator over the results
     * @dochub find
     */
     @Override
     @WrapperSupportedMethod(name="find", arguments = "0", responseType = WrapperMethodResponseType.CURSOR)
     public DBCursor find()  {                 
         return _find(new BasicDBObject(), null);
     }
    
    /**
     * Queries for an object in this collection.
     * @param query
     * @return an iterator over the results
     * @dochub find
     */
    @Override
    @WrapperSupportedMethod(name="find", arguments = "1", responseType = WrapperMethodResponseType.CURSOR)
    public DBCursor find(DBObject query) {
        return _find(query, null);
    }

    
    /**
     * Queries for an object in this collection.
     * @param ref object for which to search
     * @param keys fields to return
     * @return an iterator over the results
     * @dochub find
     */
    @Override
    @WrapperSupportedMethod(name="find", arguments = "2", responseType = WrapperMethodResponseType.CURSOR)
    public DBCursor find(DBObject ref, DBObject keys)  {
        return _find(ref, keys);

    }
    
    /**
     * internally implements the find() operation
     * 
     * @param query
     * @param fields
     * @param numToSkip
     * @param batchSize
     * @return 
     */
    @Override
    @Deprecated
    public DBCursor find( DBObject query , DBObject fields , int numToSkip , int batchSize ) {
        return find(query, fields);
    }
    
    
    /**
     * internally implements the find() operation
     * 
     * @param query
     * @param fields
     * @param options
     * @param batchSize
     * @param numToSkip
     * @return 
     */
    @Override
    @Deprecated
    public DBCursor find( DBObject query , DBObject fields , int numToSkip , int batchSize , int options ) {
        return find(query, fields);
    }
    
    //gets the query parameter, if it is null, then it creates one
    //if a user has aleady provided a _id for searching, then move it to its corresponding place
    //it does not appends the timestamps, but lets the corresponding implementations deal with it
    private DBObject fixDataModificationQuery(DBObject query) {
        if(query==null) ///if no query, then create an empty: it will be needed to append the timestamps related
            query = new BasicDBObject();
        else {
            //check if user has provided a find on the identifier, if so, then move the _id to _dataID
            if(query.containsField(MongoMVCCFields._ID)) {
                Object _id = query.get(MongoMVCCFields._ID);
                query.put(MongoMVCCFields.DATA_ID, _id);
                query.removeField(MongoMVCCFields._ID);
            }
        }
        
        return query;
    }
    
    
    //gets the query parameter, if it is null, then it creates one
    //if a user has aleady provided a _id for searching, then move it to its corresponding place
    //at the end, append the timestamp/tid related part of the query
    private DBObject fixSelectQuery(DBObject query) {
        query = fixDataModificationQuery(query);
        
        BasicDBList listQueries = new BasicDBList();
        if(!this.writeset.isEmpty())
            listQueries.add(VersionQueryUtils.getQueryPrivateVersion((BasicDBObject) query, this.mongoTransactionalContext.getTid()));
        listQueries.add(VersionQueryUtils.getQueryVersion((BasicDBObject) query, this.mongoTransactionalContext.getStartTimestamp()));
        BasicDBObject queryObject = new BasicDBObject("$or", listQueries);
        
        return queryObject;
    }
    
    /**
     * internally implements the find() operation
     * 
     */
    private DBCursor _find(DBObject query, DBObject keys) {
        
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        
        DeprecatedDBCursor depracedCursor;
        DBObject queryObject = fixSelectQuery(query);
        
        Log.debug("Driver will execute the: {}", queryObject);
        if(keys==null) {
            depracedCursor = this._dbDepracedCollection.find(queryObject);
        } else {
            depracedCursor = this._dbDepracedCollection.find(queryObject, keys);
        }
        
        DBCursor cursor;
        if(!this.isAutoCommit.get()) {
            cursor = new DBCursor(new CollectionBean(this, mongoTransactionalContext, writeset), depracedCursor, (BasicDBObject) query, (BasicDBObject) keys, 0, 0);
            //this.clientCursors.add(cursor);
        } else { //if autocommit, set writeset to null (no writeset cause no private versions exists
            cursor = new DBCursor(new CollectionBean(this, mongoTransactionalContext, null), depracedCursor, (BasicDBObject) query, (BasicDBObject) keys, 0, 0);
        }
        
        if(this.isAutoCommit.get())
            autoCommit();
        
        return cursor;
    }
    
    private DBObject _findOne(DBObject o, DBObject fields, DBObject orderBy, ReadPreference readPref) {
        
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        BasicDBObject result = null;
        BasicDBObject query;
        if(o!=null)
            query = new BasicDBObject(o.toMap());
        else
            query = new BasicDBObject();
        DBObject queryObject = fixSelectQuery(query);
        
        DeprecatedDBCursor cursor = this._dbDepracedCollection.find(queryObject, fields);
        if(readPref!=null)
            cursor.setReadPreference(readPref);
        if(orderBy!=null)
            cursor.sort(orderBy);
        
        while(cursor.hasNext()) {
            
            result = (BasicDBObject) cursor.next();
            
            //if document is already in the writeset, then check if this version is outdated or current (private)
            if(this.writeset.containsKey(result.getObjectId(MongoMVCCFields.DATA_ID))) {
                if(result.getLong(MongoMVCCFields.TID)==this.mongoTransactionalContext.getTid())
                    break;
                else 
                    result = null;
            }
            
            //if not found (previous document was outdated, then check the next one
            if(result!=null)
                break;
        }
        
        if(this.isAutoCommit.get())
            autoCommit();
        
        return moveDataIdTOId(result);
    }
    
    private BasicDBObject moveDataIdTOId(BasicDBObject dbObject) {
        if((dbObject!=null)&&(dbObject.containsField(MongoMVCCFields.DATA_ID))) {
            Object obj = dbObject.get(MongoMVCCFields.DATA_ID);
            dbObject.put(MongoMVCCFields._ID, obj);
            dbObject.removeField(MongoMVCCFields.DATA_ID);
        }
        
        return dbObject;
    }
            
       
    
    
    
    
    /**
     * Inserts a list of documents to database with the TID and null timestamps
     * if doc have an _id, it will be moved to _dataID
     * the _id is auto-generated and can be stored at the "data_id" key of the document
     * 
     * @param o
     * @return number of items currently at the write-set
     * @dochub insert
     */
    @WrapperSupportedMethod(name="insert", arguments = "1", responseType = WrapperMethodResponseType.NULL)
    public WriteResult insert(DBObject o)  {
        return insert(Arrays.asList(o));
    }
    
    

    /**
     * Inserts a list of documents to database with the TID and null timestamps
     * if doc have an _id, it will be moved to _dataID
     * the _id is auto-generated and can be stored at the "data_id" key of the document
     *
     * @param arr
     * @return number of items currently at the write-set
     * @dochub insert
     */
    @Override
    @WrapperSupportedMethod(name="insert", arguments = "-1", responseType = WrapperMethodResponseType.NULL)
    public WriteResult insert(DBObject[] arr)   {
        return insert(Arrays.asList(arr));
    }

    
    /**
     * Inserts a list of documents to database with the TID and null timestamps
     * if doc have an _id, it will be moved to _dataID
     * the _id is auto-generated and can be stored at the "data_id" key of the document
     *
     * @param list
     * @return null
     * @dochub insert
     */
    @Override
    public WriteResult insert(List<DBObject> list)  {
        return _insert(list, null, null, null);
    }
    
    
    @Override
    public WriteResult insert(DBObject[] arr , WriteConcern concern ){
        return _insert(Arrays.asList(arr), concern, null, null);
    }
    
    
    @Override
    public WriteResult insert(DBObject[] arr , WriteConcern concern, DBEncoder encoder) {
        return _insert(Arrays.asList(arr), concern, null, encoder); 
    }
    
    
    @Override
    public WriteResult insert(List<DBObject> list, WriteConcern concern, DBEncoder encoder) {
        return _insert(list, concern, null, encoder); 
    }
    
    
    @Override
    public WriteResult insert(DBObject o , WriteConcern concern ){
        return insert(concern, o);
    }
    
    
    @Override
    public WriteResult insert(List<DBObject> list, WriteConcern concern ){
        return _insert(list, concern, null, null);
    }
    
    @Override
    public WriteResult insert(WriteConcern concern, DBObject ... arr){
        return insert(arr, concern, null);
    }
    
    @Override
    public WriteResult insert(final List<DBObject> documents, final InsertOptions insertOptions) {
        return _insert(documents, null, insertOptions, null);
    }

    
    
    
    
    /**
     * Update an existing document or insert a document depending on the parameter. If the document does not contain an '_id' field, then
     * the method performs an insert with the specified fields in the document as well as an '_id' field with a unique objectId value. If
     * the document contains an '_id' field, then the method performs an upsert querying the collection on the '_id' field: 
     * <ul>
     *     <li>If a document does not exist with the specified '_id' value, the method performs an insert with the specified fields in 
     *     the document.</li>
     *     <li>If a document exists with the specified '_id' value, the method performs an update, 
     *     replacing all field in the existing record with the fields from the document.</li> 
     * </ul>
     *
     * @param jo      {@link DBObject} to save to the collection.
     * @return the result of the operation
     * @throws MongoException if the operation fails
     * @mongodb.driver.manual tutorial/modify-documents/#modify-a-document-with-save-method Save
     */
    @Override
    @WrapperSupportedMethod(name="save", arguments = "1", responseType = WrapperMethodResponseType.NULL)
    public WriteResult save(DBObject jo) throws MongoException {
        return save(jo, null);
    }
    
    /**
     * Update an existing document or insert a document depending on the parameter. If the document does not contain an '_id' field, then
     * the method performs an insert with the specified fields in the document as well as an '_id' field with a unique objectId value. If
     * the document contains an '_id' field, then the method performs an upsert querying the collection on the '_id' field: 
     * <ul>
     *     <li>If a document does not exist with the specified '_id' value, the method performs an insert with the specified fields in 
     *     the document.</li>
     *     <li>If a document exists with the specified '_id' value, the method performs an update, 
     *     replacing all field in the existing record with the fields from the document.</li> 
     * </ul>
     *
     * @param jo      {@link DBObject} to save to the collection.
     * @param concern {@code WriteConcern} to be used during operation
     * @return the result of the operation
     * @throws MongoException if the operation fails
     * @mongodb.driver.manual tutorial/modify-documents/#modify-a-document-with-save-method Save
     */
    @Override
    public WriteResult save(DBObject jo, WriteConcern concern) {
        
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }

        
        //if id does not exists, then perform an insert
        Object id = jo.get(MongoMVCCFields.DATA_ID);
        if(id==null)
            return insert(jo, concern);
        
        //else perform an upsert on the 'dataid'
        BasicDBObject query = new BasicDBObject(MongoMVCCFields.DATA_ID, id);
        return update(query, jo, true, false, concern);
    }
    
    
    //findAndModify section ...
    // -------------------
    @Override
    public DBObject findAndModify(DBObject query, DBObject fields, DBObject sort, boolean remove, DBObject update, boolean returnNew, boolean upsert) {
        return _findAndModify(query, fields, sort, remove, update, returnNew, upsert, 0, TimeUnit.MINUTES);
    }

    @Override
    public DBObject findAndModify(DBObject query, DBObject fields, DBObject sort, boolean remove, DBObject update, boolean returnNew, boolean upsert, long maxTime, TimeUnit maxTimeUnit) {
        return _findAndModify(query, fields, sort, remove, update, returnNew, upsert, maxTime, maxTimeUnit);
    }

    @Override
    @WrapperSupportedMethod(name="findAndModify", arguments = "3", responseType = WrapperMethodResponseType.DBOBJECT)
    public DBObject findAndModify(DBObject query, DBObject sort, DBObject update) {
        return _findAndModify(query, null, sort, false, update, false, false, 0, TimeUnit.MINUTES);
    }

    @Override
    @WrapperSupportedMethod(name="findAndModify", arguments = "2", responseType = WrapperMethodResponseType.DBOBJECT)
    public DBObject findAndModify(DBObject query, DBObject update) {
        return _findAndModify(query, null, null, false, update, false, false, 0, TimeUnit.MINUTES);
    }
    

    @Override
    @WrapperSupportedMethod(name="findAndRemove", arguments = "1", responseType = WrapperMethodResponseType.DBOBJECT)
    public DBObject findAndRemove(DBObject query) {
        return _findAndModify(query, null, null, true, null, false, false, 0, TimeUnit.MINUTES);
    }
    //end of findandModify section
    
    
    
    /**
     * updates the documents that match the ref query parameter with the values specified in the object parameter
     * if object is a document, and the first element's key does not start with the $set keyword, then all matched documents 
     * will take the value of this parameter
     * @param ref
     * @param object
     * @return number of rows affected
     * @throws MongoException
     * @dochub update
     */
    @Override
    @WrapperSupportedMethod(name="update", arguments = "2", responseType = WrapperMethodResponseType.NULL)
    public WriteResult update(DBObject ref, DBObject object) {
        return update(ref, object, false, true);
    }

    
    /**
     * updates the documents that match the ref query parameter with the values specified in the object parameter
     * if object is a document, and the first element's key does not start with the $set keyword, then all matched documents 
     * will take the value of this parameter
     * @param ref search query for old object to update
     * @param object object with which to update <tt>q</tt>
     * @param upsert if the database should create the element if it does not exist
     * @return number of rows affected
     * @throws MongoException
     * @dochub update
     */
    public WriteResult update(DBObject ref, DBObject object, boolean upsert) {
        return update(ref, object, upsert, false);
    }
    
    @Deprecated
    @Override
    public WriteResult update(DBObject q, DBObject o, boolean upsert, boolean multi) {
        return update(q, o, upsert, multi, null);
    }
    
    @Deprecated
    @Override
    public WriteResult update( DBObject q , DBObject o , boolean upsert , boolean multi , WriteConcern concern ) {
        return update(q, o, upsert, multi, concern, null);
    }
    
    @Override
    @Deprecated
    public WriteResult update(DBObject q, DBObject o, boolean upsert, boolean multi, WriteConcern concern, DBEncoder encoder) {
        return _update(q, o, upsert, multi, concern, encoder);
    }

    

    @Deprecated
    @Override
    public WriteResult updateMulti(DBObject q, DBObject o) {
        return update(q, o, false, true);
    }
    
    
    /**
     * removes the documents that match the query parameter. If the query parameter is a document 
     * (the first element's key does not start with '$', then simply remove this document)
     * @param o the object that documents to be removed must match
     * @return null
     * @throws MongoException
     * @dochub remove
     * 
     */
    @Override
    @WrapperSupportedMethod(name="remove", arguments = "1", responseType = WrapperMethodResponseType.NULL)
    public WriteResult remove(DBObject o) throws MongoException {
        return remove(o, null);
    }
    
    
    @Deprecated
    @Override
    public WriteResult remove( DBObject o , WriteConcern concern ) {
        return remove(o, concern, null);
    }
    
    @Deprecated
    @Override
    public WriteResult remove(DBObject o, WriteConcern concern, DBEncoder encoder) {
        return _remove(o, concern, encoder);
    }
    
    
    
    
    
    

    

    //find one section ...
    // -------------------
    @Override
    public DBObject findOne(Object id) {
        return _findOne(new BasicDBObject(MongoMVCCFields.DATA_ID, id), null, null, null);
    }

    @Override
    public DBObject findOne(Object id, DBObject projection) {
        return _findOne(new BasicDBObject(MongoMVCCFields.DATA_ID, id), projection, null, null);
    }
    
    @Override
    @WrapperSupportedMethod(name="findOne", arguments = "0", responseType = WrapperMethodResponseType.DBOBJECT)
    public DBObject findOne() {
        return _findOne(null, null, null, null);
    }

    @Override
    @WrapperSupportedMethod(name="findOne", arguments = "1", responseType = WrapperMethodResponseType.DBOBJECT)
    public DBObject findOne(DBObject o) {
        return _findOne(o, null, null, null);
    }

    @Override
    @WrapperSupportedMethod(name="findOne", arguments = "2", responseType = WrapperMethodResponseType.DBOBJECT)
    public DBObject findOne(DBObject o, DBObject fields) {
        return _findOne(o, fields, null, null);
    }

    @Override
    @WrapperSupportedMethod(name="findOne", arguments = "3", responseType = WrapperMethodResponseType.DBOBJECT)
    public DBObject findOne(DBObject o, DBObject fields, DBObject orderBy) {
        return _findOne(o, fields, orderBy, null);
    }

    @Override
    public DBObject findOne(DBObject o, DBObject fields, ReadPreference readPref) {
        return _findOne(o, fields, null, readPref);
    }

    @Override
    public DBObject findOne(DBObject o, DBObject fields, DBObject orderBy, ReadPreference readPref) {
        return _findOne(o, fields, orderBy, readPref);
    }
    // -------------------
    //end of find one section ...
    
    

    

    //distinct section ...
    // -------------------
     /**
     * Find the distinct values for a specified field across a collection and returns the results in an array.
     *
     * @param key Specifies the field for which to return the distinct values
     * @return A {@code List} of the distinct values
     * @throws MongoException
     * @mongodb.driver.manual reference/command/distinct Distinct Command
     */
    @Override
    public List distinct(String key) {
        return distinct(key, new BasicDBObject());
    }

    /**
     * Find the distinct values for a specified field across a collection and returns the results in an array.
     *
     * @param key       Specifies the field for which to return the distinct values
     * @param readPrefs {@link ReadPreference} to be used for this operation  -- this is ignored
     * @return A {@code List} of the distinct values
     * @throws MongoException
     * @mongodb.driver.manual reference/command/distinct Distinct Command
     */
    @Override
    public List distinct(String key, ReadPreference readPrefs) {
        return distinct(key, null, readPrefs);
    }

    /**
     * Find the distinct values for a specified field across a collection and returns the results in an array.
     *
     * @param key   Specifies the field for which to return the distinct values
     * @param query specifies the selection query to determine the subset of documents from which to retrieve the distinct values
     * @return A {@code List} of the distinct values
     * @throws MongoException
     * @mongodb.driver.manual reference/command/distinct Distinct Command
     */
    @Override
    public List distinct(String key, DBObject query) {
        return _distinct(key, query);
    }

    
    /**
     * Find the distinct values for a specified field across a collection and returns the results in an array.
     *
     * @param key       Specifies the field for which to return the distinct values
     * @param query     specifies the selection query to determine the subset of documents from which to retrieve the distinct values
     * @param readPrefs {@link ReadPreference} to be used for this operation  --this to be ignored
     * @return A {@code List} of the distinct values
     * @throws MongoException
     * @mongodb.driver.manual reference/command/distinct Distinct Command
     */
    @Override
    public List distinct(String key, DBObject query, ReadPreference readPrefs) {
        return distinct(key, query);
    }
    // -------------------
    //end of distinct section ...

    

    
    //group section ...
    // -------------------
    @Override
    @Deprecated
    public DBObject group(DBObject key, DBObject cond, DBObject initial, String reduce) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    @Deprecated
    public DBObject group(DBObject key, DBObject cond, DBObject initial, String reduce, String finalize) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    @Deprecated
    public DBObject group(DBObject key, DBObject cond, DBObject initial, String reduce, String finalize, ReadPreference readPrefs) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    @Deprecated
    public DBObject group(GroupCommand cmd) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    @Deprecated
    public DBObject group(GroupCommand cmd, ReadPreference readPrefs) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    // -------------------
    //end of group section ...
        

    
    //mapReduce section ...
    // -------------------
    @Override
    public MapReduceOutput mapReduce(String map, String reduce, String outputTarget, DBObject query) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public MapReduceOutput mapReduce(String map, String reduce, String outputTarget, OutputType outputType, DBObject query) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public MapReduceOutput mapReduce(String map, String reduce, String outputTarget, OutputType outputType, DBObject query, ReadPreference readPrefs) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public MapReduceOutput mapReduce(MapReduceCommand command) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    // -------------------
    //end of mapReduce section ...
    

    
    //aggregate section ...
    // -------------------
    @Override
    @WrapperSupportedMethod(name="aggregate", arguments = "1", responseType = WrapperMethodResponseType.AGGREGATION_OUTPUT)
    public AggregationOutput aggregate(List<DBObject> pipeline) {
        return aggregate(pipeline, getReadPreference());
    }

    @Override
    public AggregationOutput aggregate(List<DBObject> pipeline, ReadPreference readPreference) {
        
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        AggregationOutput output = this._dbDepracedCollection.aggregate(fixPipeline(pipeline), readPreference);
        return output;
    }

    @Override
    public Cursor aggregate(List<DBObject> pipeline, AggregationOptions options) {
        return aggregate(pipeline, options, getReadPreference());
    }

    @Override
    public Cursor aggregate(List<DBObject> pipeline, AggregationOptions options, ReadPreference readPreference) {
        
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        Cursor result = this._dbDepracedCollection.aggregate(fixPipeline(pipeline), options, readPreference);
        
        if(this.isAutoCommit.get())
            autoCommit();
        
        return result;
    }

    @Override
    public CommandResult explainAggregate(List<DBObject> pipeline, AggregationOptions options) {
        
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        return this._dbDepracedCollection.explainAggregate(pipeline, options);
    }
    
    
    private List<DBObject> fixPipeline(List<DBObject> pipeline) {
        List<DBObject> fixPipelineList = new ArrayList<>();
        boolean findMatch = false;
        for(DBObject object : pipeline) {
            Iterator<String> it = object.keySet().iterator();
            while(it.hasNext()) {
                String key = it.next();
                
                if(key.equals("$match")) {
                    findMatch = true;
                    Object objectValue = object.get(key);
                    BasicDBObject obj;
                    if(objectValue instanceof DBObject)
                        obj = new BasicDBObject("$match", fixMatchPipeline((DBObject) objectValue));
                    else if(objectValue instanceof LinkedHashMap)
                        obj = new BasicDBObject("$match", fixMatchPipeline((LinkedHashMap) objectValue));
                    else 
                        throw new MongoException("Invalid class type of $match operator. Must be either DBOjbect or LinkedHashMap but instead, it is " + objectValue.getClass().getName());
                    
                    fixPipelineList.add(obj);
                    break;
                } else {
                    fixPipelineList.add(object);
                    break;
                }
            }
        }
        
        if(!findMatch) {
            fixPipelineList.add(0, new BasicDBObject("$match", fixMatchPipeline((DBObject) null)));
        }
        
        return fixPipelineList;
    }
    
    private DBObject fixMatchPipeline(DBObject obj) {
        
        BasicDBObject newMatch;
        if(obj==null)
            newMatch = new  BasicDBObject();
        else 
            newMatch = new BasicDBObject(obj.toMap());
        
        newMatch = VersionQueryUtils.getQueryVersion((BasicDBObject) newMatch, this.mongoTransactionalContext.getStartTimestamp());
        
        return newMatch;
        
    }
    
    private DBObject fixMatchPipeline(LinkedHashMap obj) {
        
        BasicDBObject newMatch;
        if(obj==null)
            newMatch = new  BasicDBObject();
        else 
            newMatch = new BasicDBObject(obj);
        
        newMatch = VersionQueryUtils.getQueryVersion((BasicDBObject) newMatch, this.mongoTransactionalContext.getStartTimestamp());
        
        return newMatch;
        
    }
    // -------------------
    //end of aggregate section ...

    
    

    
    
    
    /*
    @Override
    @SuppressWarnings("unchecked")
    public List<Cursor> parallelScan(ParallelScanOptions options) {
        
        _checkClose();
        
        CommandResult res = this.mongoMVCCDB.command(new BasicDBObject("parallelCollectionScan", getName())
                                        .append("numCursors", options.getNumCursors()),
                                        options.getReadPreference());
        res.throwOnError();

        List<Cursor> cursors = new ArrayList<>();
        for (DBObject cursorDocument : (List<DBObject>) res.get("cursors")) {
            QueryResultIterator qri = new QueryResultIterator(cursorDocument, ((MongoClient) this.mongoMVCCDB.getMongo()).getDepracedMongoClient(), options.getBatchSize(), getDecoder(), res.getServerUsed());
            MVCCQueryResultIterator mvccqri = new MVCCQueryResultIterator(qri, tid, version, writeset);
            cursors.add(mvccqri);
        }

        return cursors;
    }
    */
    
    
    
    
    /**
     * <p>Creates a builder for an ordered bulk write operation, consisting of an ordered collection of write requests, which can be any
     * combination of inserts, updates, replaces, or removes. Write requests included in the bulk operations will be executed in order, and
     * will halt on the first failure.</p>
     *
     * <p>Note: While this bulk write operation will execute on MongoDeprecatedDB 2.4 servers and below, the writes will be performed one at a time,
 as that is the only way to preserve the semantics of the value returned from execution or the exception thrown.</p>
     *
     * <p>Note: While a bulk write operation with a mix of inserts, updates, replaces, and removes is supported, the implementation will
     * batch up consecutive requests of the same type and send them to the server one at a time.  For example, if a bulk write operation
     * consists of 10 inserts followed by 5 updates, followed by 10 more inserts, it will result in three round trips to the server.</p>
     *
     * @return the builder
     * @mongodb.driver.manual reference/method/db.collection.initializeOrderedBulkOp/ initializeOrderedBulkOp()
     * @since 2.12
     */
    @Override
    public BulkWriteOperation initializeOrderedBulkOperation() {
        return new BulkWriteOperation(true, this);
    }

    
    /**
     * <p>Creates a builder for an unordered bulk operation, consisting of an unordered collection of write requests, which can be any
     * combination of inserts, updates, replaces, or removes. Write requests included in the bulk operation will be executed in an undefined
     * order, and all requests will be executed even if some fail.</p>
     *
     * <p>Note: While this bulk write operation will execute on MongoDeprecatedDB 2.4 servers and below, the writes will be performed one at a time,
 as that is the only way to preserve the semantics of the value returned from execution or the exception thrown.</p>
     *
     * @return the builder
     * @since 2.12
     * @mongodb.driver.manual reference/method/db.collection.initializeUnorderedBulkOp/ initializeUnorderedBulkOp()
     */
    @Override
    public BulkWriteOperation initializeUnorderedBulkOperation() {
        return new BulkWriteOperation(false, this);
    }
    
    
    BulkWriteResult executeBulkWriteOperation(final boolean ordered, final List<WriteRequest> writeRequests) {
        return executeBulkWriteOperation(ordered, writeRequests, getWriteConcern());
    }
    
    BulkWriteResult executeBulkWriteOperation(final boolean ordered, final List<WriteRequest> writeRequests,
                                              final WriteConcern writeConcern) {
        
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        
        try {
            BulkWriteResult result = translateBulkWriteResult(createOperationExecutor().execute(new MixedBulkTransactionalWriteOperation(this._dbDepracedCollection.getNamespace(),
                                                                                         translateWriteRequestsToNew(writeRequests,
                                                                                                                     getObjectCodec()),
                                                                                         ordered, writeConcern, translateToMongoCollection())),
                                            getObjectCodec());
            
            if(this.isAutoCommit.get())
                autoCommit();
                
            return result;
        } catch (MongoBulkWriteException e) {
            throw BulkWriteHelper.translateBulkWriteException(e, DeprecatedMongoClient.getDefaultCodecRegistry().get(DBObject.class));
        }

    }
    
    
    
    
    
    

    @Override
    public List<Cursor> parallelScan(ParallelScanOptions options) {
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
    
    
   // Only create a new decoder if there is a decoder factory explicitly set on the collection.  Otherwise return null
    // so that DBPort will use a cached decoder from the default factory.
    private DBDecoder getDecoder() {
        return getDBDecoderFactory() != null ? getDBDecoderFactory().create() : null;
    }

    
    Codec<DBObject> getObjectCodec() {
        return objectCodec;
    }
    
    DBObjectCodec getDefaultDBObjectCodec() {
        return new DBObjectCodec(DeprecatedMongoClient.getDefaultCodecRegistry(),
                                 DBObjectCodec.getDefaultBsonTypeClassMap(),
                                 getObjectFactory());
    }
    
    synchronized DBObjectFactory getObjectFactory() {
        return this.objectFactory;
    }
    
    
    
    OperationExecutor createOperationExecutor() {
        return new OperationExecutor() {
            @Override
            public <T> T execute(final ReadOperation<T> operation, final ReadPreference readPreference) {
                return DBCollection.this.execute(operation, readPreference);
            }

            @Override
            public <T> T execute(final WriteOperation<T> operation) {
                return DBCollection.this.execute(operation);
            }
        };
    }
    
    <T> T execute(final ReadOperation<T> operation, final ReadPreference readPreference) {
        ReadBinding binding = getReadBinding(readPreference);
        try {
            return operation.execute(binding);
        } finally {
            binding.release();
        }
    }
    
    
    
    <T> T execute(final WriteOperation<T> operation) {
        WriteBinding binding = getWriteBinding();
        try {
            return operation.execute(binding);
        } finally {
            binding.release();
        }
    }
    
    
    private MongoCollection<DBObject> translateToMongoCollection() {
        
        MongoDatabase mongoDatabase = this.abstractDB._depracedMongoClient.getDatabase(this.abstractDB.databaseName);
        MongoCollection<DBObject> mongoCollectionDeprecated = mongoDatabase.getCollection(this.collectionName, DBObject.class);
        CpMongoCollectionImpl<DBObject> cpMongoCollection = new CpMongoCollectionImpl(collectionName, DBObject.class, mongoCollectionDeprecated, this.mongoTransactionalContext, abstractDB, writeset, pendingWritesetsToApply);
        
        return cpMongoCollection;
    }

}