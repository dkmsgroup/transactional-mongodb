package com.mongodb;

import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/** 
 * NOT A THREAD-SAFE. Only one thread can iterate results from a query. 
 * If not, then have a thread writing at a cache, and let others read from it
 *
 * @author idezol
 */
public class DBCursor extends AbstractMongoCursorImpl implements IDBCursor, Iterable<DBObject>,  Closeable {
    private static final Logger Log = LoggerFactory.getLogger(DBCursor.class);
    
    protected final DeprecatedDBCursor _cursor;
    private final BasicDBObject _query;
    private final BasicDBObject _fields;
    private DBObject _curObj = null;
    private DBObject _nextObj = null;
    List<DBObject> arrayResults = null;
    
    static enum CursorType {ITERATOR , ARRAY}
    private CursorType _cursorType = null;
    
    void _checkType( CursorType type ){
        if ( _cursorType == null ){
            _cursorType = type;
            return;
        }

        if ( type == _cursorType )
            return;

        throw new IllegalArgumentException( "can't switch cursor access methods" );
    }
    
    
    public DBCursor(CollectionBean dbCollection, DeprecatedDBCursor cursor,  BasicDBObject query, BasicDBObject fields, int skip, int limit) {
        super(dbCollection, skip, limit);
        this._cursor = cursor;
        this._query = query;
        this._fields = fields;
    }
    
   
    
    @Override
    protected void _close() {
        this._cursor.close();
        this._curObj = null;
        this._nextObj = null;
        this.isClosed.set(true);
    }
    
     /**
     * Checks if there is another object available
     * @return
     * @throws MongoException
     */
    @Override
    public boolean hasNext() {
        _checkType(CursorType.ITERATOR);
        return _hasNext();
    }

    
    /**
     * Returns the object the cursor is at and moves the cursor ahead by one.
     * @return the next element
     * @throws MongoException
     */
    @Override
    public DBObject next() {     
        _checkType(CursorType.ITERATOR);
        return _next();
    }
    
    
    /**
     * Returns the element the cursor is at.
     * @return the current element
     */
    @Override
    public DBObject curr(){
        _checkType(CursorType.ITERATOR);
        return _curr();
    }

    
    /**
     * Not implemented by the official MongoDN driver
     * We'll not implement this method either
     */
    @Override
    public void remove(){
        throw new UnsupportedOperationException( "can't remove from a cursor" );
    }
    
    
    /**
     * creates a copy of this cursor object that can be iterated.
     * Note:
     * - you can iterate the DBCursor itself without calling this method
     * - no actual data is getting copied.
     *
     * @return
     */
    @Override
    public Iterator<DBObject> iterator(){
        return (Iterator<DBObject>) this.copy();
    }
    
    
    /**
     * Creates a copy of an existing database cursor.
     *
     * @return the new cursor
     */
    
    //@Override
    @Override
    public IDBCursor copy() {
        
        DBCursor c = new DBCursor((CollectionBean) dbCollection, _cursor, _query, _fields, skip, limit);
        c._curObj = _curObj;
        c._cursorType = _cursorType;
        c._nextObj = _nextObj;
        c.arrayResults = arrayResults;
        c.isClosed = isClosed;
        c.totalScanned = totalScanned;
        
        return c;
    }
    
    
    /**
     * Firstly checks if the nextObj is already set. If yes, then there's a next Object
     * If no, then searches for the next document
     * Firstly checks the private writeset and validates against the initial query
     * Then checks the datastore's result, and checks if the documents have already been updated 
     * (this means that they will have been found first in the private writeset
     * If a next document is found, it's been copied to the nextObj
     * @return 
     */
    protected boolean _hasNext() {
        
        if(isClosed.get())
            throw new IllegalStateException("Iterator has been closed");
        
        //chech if there is a nextObj
        if(_nextObj!=null)
            return true;
        
        
        if((limit>0)&&(totalScanned>=limit))
            return false;
        
        while(_cursor.hasNext()) {
             BasicDBObject documentFound = (BasicDBObject) _cursor.next();
            //if document is already in the writeset, then check if this version is outdated or current (private)
            if((this.writeset!=null) && (this.writeset.containsKey(documentFound.get(MongoMVCCFields.DATA_ID)))) {
                if(documentFound.getLong(MongoMVCCFields.TID)==this.mongoTransactionalContext.getTid()) {
                    //discard the first 'skip' documents
                    if(skip>0) {
                        skip--;
                        continue;
                    }
                    //_nextObj = documentFound;
                    _nextObj = fixIdentifier(documentFound);
                    totalScanned++;
                    return true;
                }
                else 
                    //there's an updated version for this document, do not count it
                    continue;
            } else {
                //document is from the public versions --no updated version found in the private writeset
                //discard the first 'skip' documents
                if(skip>0) {
                    skip--;
                    continue;
                }
                //_nextObj = documentFound;
                _nextObj = fixIdentifier(documentFound);
                totalScanned++;
                return true;
            }
        }
        
        return false;
    }
    
    
    private BasicDBObject fixIdentifier(BasicDBObject dbObject) {
        Object objID = dbObject.get(MongoMVCCFields.DATA_ID);
        dbObject.put(MongoMVCCFields._ID, objID);
        dbObject.remove(MongoMVCCFields.DATA_ID);
        return dbObject;
    }
   
    //returns the _nextObject and sets it to be the current (while set the _nextObj to null
    //if nextObj is null, then checks a hasNext to find the next one
    protected DBObject _next() {
        if(isClosed.get())
            throw new IllegalStateException("Iterator has been closed");
       
        if(_nextObj!=null) {
            _curObj = _nextObj;
            _nextObj = null;
            return _curObj;
        }
        else {
            if(_hasNext()) {
                _next();
            }
        }
        
        //couldn't find any
        return null;
    }
    
    
    protected DBObject _curr() {        
       if(isClosed.get())
            throw new IllegalStateException("Iterator has been closed");
       
       return _curObj;
    }
    
    
    
    /**
     * Adds a comment to the query to identify queries in the database profiler output.
     * 
     * @param comment the comment that is to appear in the profiler output
     * @return {@code this} so calls can be chained
     * @mongodb.driver.manual reference/operator/meta/comment/ $comment
     * @since 2.12
     */
    @Override
    public IDBCursor comment(String comment) {
        this._cursor.comment(comment);
        return this;
    }
    
    
    /**
     * Limits the number of documents a cursor will return for a query.
     * 
     * @param max the maximum number of documents to return
     * @return {@code this} so calls can be chained
     * @mongodb.driver.manual reference/operator/meta/maxScan/ $maxScan
     * @see #limit(int) 
     * @since 2.12
     */
    @Override
    public DBCursor maxScan(int max) {
        this._cursor.maxScan(max);
        return this;
    }

    
    /**
     * Specifies an <em>exclusive</em> upper limit for the index to use in a query.
     *
     * @param max a document specifying the fields, and the upper bound values for those fields
     * @return {@code this} so calls can be chained
     * @mongodb.driver.manual reference/operator/meta/max/ $max
     * @since 2.12
     */
    @Override
    public DBCursor max(DBObject max) {
        this._cursor.max(max);
        return this;
    }

    
    /**
     * Specifies an <em>inclusive</em> lower limit for the index to use in a query. 
     *
     * @param min a document specifying the fields, and the lower bound values for those fields
     * @return {@code this} so calls can be chained
     * @mongodb.driver.manual reference/operator/meta/min/ $min
     * @since 2.12
     */
    @Override
    public DBCursor min(DBObject min) {
        this._cursor.min(min);
        return this;
    }

    /**
     * Forces the cursor to only return fields included in the index.
     *
     * @return {@code this} so calls can be chained
     * @mongodb.driver.manual reference/operator/meta/returnKey/ $returnKey
     * @since 2.12
     */
    @Override
    public DBCursor returnKey() {
        this._cursor.returnKey();
        return this;
    }

    
    /**
     * Modifies the documents returned to include references to the on-disk location of each document.  The location will be returned in a
     * property named {@code $diskLoc}
     *
     * @return {@code this} so calls can be chained
     * @mongodb.driver.manual reference/operator/meta/showDiskLoc/ $showDiskLoc
     * @since 2.12
     */
    @Override
    public DBCursor showDiskLoc() {
        this._cursor.showDiskLoc();
        return this;
    }

    
    /**
     * Adds a special operator like $maxScan or $returnKey. For example:
     * <pre>
     *    addSpecial("$returnKey", 1)
     *    addSpecial("$maxScan", 100)
     * </pre>
     *
     * @param name the name of the special query operator
     * @param o    the value of the special query operator
     * @return {@code this} so calls can be chained
     * @mongodb.driver.manual reference/operator Special Operators
     */
    @Override
    public IDBCursor addSpecial(String name, Object o) {
        this._cursor.addSpecial(name, o);
        return this;
    }

    
    /**
     * Informs the database of indexed fields of the collection in order to improve performance.
     *
     * @param indexKeys a {@code DBObject} with fields and direction
     * @return same DepracedDBCursor for chaining operations
     * @mongodb.driver.manual reference/operator/meta/hint/ $hint
     */
    @Override
    public DBCursor hint(DBObject indexKeys) {
        this._cursor.hint(indexKeys);
        return this;
    }

    
    /**
     * Informs the database of an indexed field of the collection in order to improve performance.
     *
     * @param indexName the name of an index
     * @return same DepracedDBCursor for chaining operations
     * @mongodb.driver.manual reference/operator/meta/hint/ $hint
     */
    @Override
    public DBCursor hint(String indexName) {
        this._cursor.hint(indexName);
        return this;
    }

    
    /**
     * Set the maximum execution time for operations on this cursor.
     *
     * @param maxTime  the maximum time that the server will allow the query to run, before killing the operation. A non-zero value requires
     *                 a server version &gt;= 2.6
     * @param timeUnit the time unit
     * @return same DepracedDBCursor for chaining operations
     * @mongodb.server.release 2.6
     * @mongodb.driver.manual reference/operator/meta/maxTimeMS/ $maxTimeMS
     * @since 2.12.0
     */
    @Override
    public DBCursor maxTime(long maxTime, TimeUnit timeUnit) {
        this._cursor.maxTime(maxTime, timeUnit);
        return this;
    }

    
    /**
     * Use snapshot mode for the query. Snapshot mode prevents the cursor from returning a document more than once because an intervening
     * write operation results in a move of the document. Even in snapshot mode, documents inserted or deleted during the lifetime of the
     * cursor may or may not be returned.  Currently, snapshot mode may not be used with sorting or explicit hints.
     *
     * @return {@code this} so calls can be chained
     *
     * @see com.mongodb.DepracedDBCursor#sort(DBObject)
     * @see com.mongodb.DepracedDBCursor#hint(DBObject)
     * @mongodb.driver.manual reference/operator/meta/snapshot/ $snapshot
     */
    @Override
    public IDBCursor snapshot() {
        Log.info("MongoMVCC Driver always returns snapshot of a database");
        return this;
    }

    
    /**
     * Returns an object containing basic information about the execution of the query that created this cursor. This creates a {@code
     * DBObject} with a number of fields, including but not limited to: 
     * <ul>
     *     <li><i>cursor:</i> cursor type</li>
     *     <li><i>nScanned:</i> number of records examined by the database for this query </li>
     *     <li><i>n:</i> the number of records that the database returned</li>
     *     <li><i>millis:</i> how long it took the database to execute the query</li>
     * </ul>
     *
     * @return a {@code DBObject} containing the explain output for this DepracedDBCursor's query
     * @throws MongoException
     * @mongodb.driver.manual reference/explain Explain Output
     */
    @Override
    public DBObject explain() {
        return _cursor.explain();
    }

    
    
    /**
     * <p>Limits the number of elements returned in one batch. A cursor typically fetches a batch of result objects and store them
     * locally.</p>
     *
     * <p>If {@code batchSize} is positive, it represents the size of each batch of objects retrieved. It can be adjusted to optimize
     * performance and limit data transfer.</p>
     *
     * <p>If {@code batchSize} is negative, it will limit of number objects returned, that fit within the max batch size limit (usually
     * 4MB), and cursor will be closed. For example if {@code batchSize} is -10, then the server will return a maximum of 10 documents and
     * as many as can fit in 4MB, then close the cursor. Note that this feature is different from limit() in that documents must fit within
     * a maximum size, and it removes the need to send a request to close the cursor server-side.</p>
     *
     * @param numberOfElements the number of elements to return in a batch
     * @return {@code this} so calls can be chained
     */
    @Override
    public IDBCursor batchSize(int numberOfElements) {
        this._cursor.batchSize(numberOfElements);
        return this;
    }

    
    
    /**
     * Adds a query option. See Bytes.QUERYOPTION_* for list.
     *
     * @param option the option to be added
     * @return {@code this} so calls can be chained
     * @see Bytes
     * @mongodb.driver.manual ../meta-driver/latest/legacy/mongodb-wire-protocol/#op-query Query Flags
     */
    @Override
    public IDBCursor addOption(int option) {
        this._cursor.addOption(option);
        return this;
    }

    
    /**
     * Sets the query option - see Bytes.QUERYOPTION_* for list.
     *
     * @param options the bitmask of options
     * @return {@code this} so calls can be chained
     * @see Bytes
     * @mongodb.driver.manual ../meta-driver/latest/legacy/mongodb-wire-protocol/#op-query Query Flags
     */
    @Override
    public DBCursor setOptions(int options) {
        this._cursor.setOptions(options);
        return this;
    }

    
    /**
     * Resets the query options.
     *
     * @return {@code this} so calls can be chained
     * @mongodb.driver.manual ../meta-driver/latest/legacy/mongodb-wire-protocol/#op-query Query Flags
     */
    @Override
    public DBCursor resetOptions() {
        this._cursor.resetOptions();
        return this;
    }

    
    /**
     * Gets the query options.
     *
     * @return the bitmask of options
     * @mongodb.driver.manual ../meta-driver/latest/legacy/mongodb-wire-protocol/#op-query Query Flags
     */
    @Override
    public int getOptions() {
        return _cursor.getOptions();
    }

    

    /**
     * Gets the batch size.
     *
     * @return the batch size
     */
    @Override
    public int getBatchSize() {
        return _cursor.getBatchSize();
    }

    
    

    /**
     * Non blocking check for tailable cursors to see if another object is available.
     *
     * <p>Returns the object the cursor is at and moves the cursor ahead by one or
     * return null if no documents is available.</p>
     *
     * @return the next element or null
     * @throws MongoException
     * @mongodb.driver.manual /core/cursors/#cursor-batches Cursor Batches
     */
    @Override
    public DBObject tryNext() {
        _checkType(CursorType.ITERATOR);
        _hasNext();
        return  _curObj;
    }

    
    
    
    /**
     * For testing only! Iterates cursor and counts objects
     *
     * @return num objects
     * @throws MongoException
     * @see #count()
     */
    @Override
    public int itcount() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    
    
    /**
     * Returns the first document that matches the query.
     *
     * @return the first matching document
     * @since 2.12
     */
    @Override
    public DBObject one() {
        return ((DBCollection) this.dbCollection.collection).findOne(_query, _fields, _cursor.getReadPreference());        
    }

    /**
     * Counts the number of objects matching the query this does take limit/skip into consideration
     *
     * @return the number of objects
     * @throws MongoException
     * @see #count()
     */
    @Override
    public int size() {
        return (int) ((DBCollection) this.dbCollection.collection).getCount(_query, _fields, limit, skip, _cursor.getReadPreference());
    }

    
    /**
     * Gets the fields to be returned.
     *
     * @return the field selector that cursor used
     */
    @Override
    public DBObject getKeysWanted() {
        return this._fields;
    }

    /**
     * Gets the query.
     *
     * @return the query that cursor used
     */
    @Override
    public DBObject getQuery() {
        return this._query;
    }

    
    /**
     * Gets the collection.
     *
     * @return the collection that data is pulled from
     */
    @Override
    public DBCollection getCollection() {
        return ((DBCollection) this.dbCollection.collection);
    }

    
    /**
     * Gets the Server Address of the server that data is pulled from. Note that this information may not be available until hasNext() or
     * next() is called.
     *
     * @return the address of the server
     */
    @Override
    public ServerAddress getServerAddress() {
        return _cursor.getServerAddress();
    }

    
    /**
     * Sets the read preference for this cursor. See the documentation for {@link ReadPreference} for more information.
     *
     * @param readPreference read preference to use
     * @return {@code this} so calls can be chained
     */
    @Override
    public DBCursor setReadPreference(ReadPreference readPreference) {
        this._cursor.setReadPreference(readPreference);
        return this;
    }

    
    /**
     * Gets the default read preference.
     *
     * @return the readPreference used by this cursor
     */
    @Override
    public ReadPreference getReadPreference() {
        return _cursor.getReadPreference();
    }

    
    /**
     * Sets the factory that will be used create a {@code DBDecoder} that will be used to decode BSON documents into DBObject instances.
     *
     * @param fact the DBDecoderFactory
     * @return {@code this} so calls can be chained
     */
    @Override
    public DBCursor setDecoderFactory(DBDecoderFactory fact) {
        this._cursor.setDecoderFactory(fact);
        return this;
    }

    
    /**
     * Gets the decoder factory that creates the decoder this cursor will use to decode objects from MongoDB. 
     *
     * @return the decoder factory.
     */
    @Override
    public DBDecoderFactory getDecoderFactory() {
        return _cursor.getDecoderFactory();
    }
    
    
    /**
     * Sorts this cursor's elements. This method must be called before getting any object from the cursor.
     *
     * @param orderBy the fields by which to sort
     * @return a cursor pointing to the first element of the sorted results
     */
    @Override
    public DBCursor sort(DBObject orderBy) {
        if(this._cursor.checkInternalQueryIteratorOpen())
            throw new IllegalStateException( "can't set limit after executing query" );
        
        this._cursor.sort(orderBy);
        return this;
    }
    
    /**
     * Limits the number of elements returned. Note: parameter {@code limit} should be positive, although a negative value is
     * supported for legacy reason. Passing a negative value will call {@link DepracedDBCursor#batchSize(int)} which is the preferred method.
     *
     * @param limit the number of elements to return
     * @return a cursor to iterate the results
     * @mongodb.driver.manual reference/method/cursor.limit Limit
     */
    @Override
    public DBCursor limit(int limit) {
        this._cursor.limit(limit);
        if(this._cursor.checkInternalQueryIteratorOpen())
            throw new IllegalStateException( "can't set limit after executing query" );
        this.limit = limit;
        return this;
    }
    
    
    
    /**
     * Gets the query limit.
     *
     * @return the limit, or 0 if no limit is set
     */
    @Override
    public int getLimit() {
        return this.limit;
    }
    
    /**
     * Discards a given number of elements at the beginning of the cursor.
     *
     * @param numberOfElements the number of elements to skip
     * @return a cursor pointing to the new first element of the results
     * @throws IllegalStateException if the cursor has started to be iterated through
     */
    @Override
    public DBCursor skip(int numberOfElements) {
        if(this._cursor.checkInternalQueryIteratorOpen())
            throw new IllegalStateException( "can't set limit after executing query" );
        this.skip = numberOfElements;
        return this;
    }
    
    /**
     * Returns the number of objects through which the cursor has iterated.
     *
     * @return the number of objects seen
     */
    @Override
    public int numSeen() {
        return totalScanned;
    }
    
    /**
     * Pulls back all items into an array and returns the number of objects. Note: this can be resource intensive.
     *
     * @return the number of elements in the array
     * @throws MongoException
     * @see #count()
     * @see #size()
     */
    @Override
    public int length() {
        _checkType(CursorType.ARRAY);
        fillAllArray();
        return this.arrayResults.size();
    }

    
    /**
     * Converts this cursor to an array.
     *
     * @return an array of elements
     * @throws MongoException
     */
    @Override
    public List<DBObject> toArray() {
        return toArray(Integer.MAX_VALUE);
    }

    
    /**
     * Converts this cursor to an array.
     *
     * @param max the maximum number of objects to return
     * @return an array of objects
     * @throws MongoException
     */
    @Override
    public List<DBObject> toArray(int max) {
        _checkType(CursorType.ARRAY);
        fillArray(max);
        return this.arrayResults;
    }
    
    
    private void fillArray(int max) {
        
        if(max==0)
            fillAllArray();
        else {
            int count = 0;
            if(this.arrayResults==null)
                this.arrayResults = new ArrayList<>();
            
            while(_hasNext()) {
                count++;
                this.arrayResults.add(_next());
                if(count==max)
                    break;
            }
        }
    }
    
    private void fillAllArray() {
        if(this.arrayResults==null)
            this.arrayResults = new ArrayList<>();
        while(_hasNext())
            this.arrayResults.add(_next());
    }
    

    
    /**
     * Counts the number of objects matching the query. This does not take limit/skip into consideration, and does initiate a call to the
     * server.
     *
     * @return the number of objects
     * @throws MongoException
     * @see DepracedDBCursor#size
     */
    @Override
    public int count() {
        return (int) ((DBCollection) this.dbCollection.collection).count(_query);
    }

    
}
