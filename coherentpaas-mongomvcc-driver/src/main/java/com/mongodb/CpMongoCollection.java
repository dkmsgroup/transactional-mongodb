package com.mongodb;

import java.io.Closeable;

/**
 *
 * @author idezol
 */
public interface CpMongoCollection extends Closeable {
    
    public String getName();
    
}
