package com.mongodb;

import eu.coherentpaas.transactionmanager.datastore.TransactionalDSClient;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author stavroula
 */
public interface IMongoClient extends TransactionalDSClient {
    
    public List<MongoCredential> getCredentialsList();
    
    public MongoClientOptions getMongoClientOptions();
    
    //from mongo
    
    public IDB getDB( String dbname );
    
    public Collection<IDB> getUsedDatabases();
    
    public List<String> getDatabaseNames();
    
    public void dropDatabase(String dbName);
    
    public String getConnectPoint();
    
    public ReplicaSetStatus getReplicaSetStatus();
    
    public ServerAddress getAddress();
    
    public List<ServerAddress> getAllAddress();
    
    public List<ServerAddress> getServerAddressList();
    
    public void close();
    
    public void setWriteConcern( WriteConcern concern );
    
    public WriteConcern getWriteConcern();
    
    public void setReadPreference(ReadPreference preference);
    
    public ReadPreference getReadPreference();
    
    public void addOption( int option );
    
    public void setOptions( int options );
    
    public void resetOptions();
    
    public int getOptions();
    
    public int getMaxBsonObjectSize();
    
    public CommandResult fsync(boolean async);
    
    public CommandResult fsyncAndLock();
    
    public DBObject unlock();
    
    public boolean isLocked();
  
}