package com.mongodb;

import eu.coherentpaas.mongodb.events.DatabaseTransactionBeginEvent;
import eu.coherentpaas.mongodb.events.DatabaseTransactionBeginListener;
import eu.coherentpaas.mongodb.events.DatabaseTransactionCommitEvent;
import eu.coherentpaas.mongodb.events.DatabaseTransactionCommitListener;
import eu.coherentpaas.mongodb.events.DatabaseTransactionRollbackEvent;
import eu.coherentpaas.mongodb.events.DatabaseTransactionRollbackListener;
import eu.coherentpaas.mongodb.utils.MongoTransactionContext;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author idezol
 */
public class DBWrapper extends DB {
    
    protected DBWrapper(long dbID, String databaseName, DeprecatedMongoClient _depracedMongoClient, MongoClient mongoClient, DeprecatedDB deprecatedDb) {
        super(dbID, databaseName, _depracedMongoClient, mongoClient, deprecatedDb);
    }
    
    protected List<DatabaseTransactionCommitListener> listCommitListeners = Collections.synchronizedList(new ArrayList<DatabaseTransactionCommitListener>());
    protected List<DatabaseTransactionRollbackListener> listRollbackListeners = Collections.synchronizedList(new ArrayList<DatabaseTransactionRollbackListener>());
    protected List<DatabaseTransactionBeginListener> listBeginListeners = Collections.synchronizedList(new ArrayList<DatabaseTransactionBeginListener>());
    
    public void registerDatabaseTransactionCommitListener(DatabaseTransactionCommitListener listener) {
        this.listCommitListeners.add(listener);
    }
    public void unRegisterDatabaseTransactionCommitListener(DatabaseTransactionCommitListener listener) {
        this.listCommitListeners.remove(listener);
    }
    public void registerDatabaseTransactionRollbackListener(DatabaseTransactionRollbackListener listener) {
        this.listRollbackListeners.add(listener);
    }
    public void unRegisterDatabaseTransactionRollbackListener(DatabaseTransactionRollbackListener listener) {
        this.listRollbackListeners.remove(listener);
    }
    public void registerDatabaseTransactionBeginListener(DatabaseTransactionBeginListener listener) {
        this.listBeginListeners.add(listener);
    }
    public void unRegisterDatabaseTransactionBeginListener(DatabaseTransactionBeginListener listener) {
        this.listBeginListeners.remove(listener);
    }
    
    
    protected void fireTransactionBeginEvent() {
        for(DatabaseTransactionBeginListener listener : this.listBeginListeners)
            listener.databaseTransactionBegin(new DatabaseTransactionBeginEvent(this));
    }
    
    protected void fireTransactionRollbackEvent() {
        for(DatabaseTransactionRollbackListener listener : this.listRollbackListeners)
            listener.databaseTransactionRollbackDetected(new DatabaseTransactionRollbackEvent(this));
    }
    
    protected void fireTransactionCommitEvent() {
        for(DatabaseTransactionCommitListener listener : this.listCommitListeners)
            listener.databaseTransactionCommitDetected(new DatabaseTransactionCommitEvent(this));
    }
    
    
    @Override
    protected void _rollback(MongoTransactionContext txnctx) {        
        super._rollback(txnctx);
        fireTransactionRollbackEvent();
    }
    
    
    @Override
    protected void _applyWS(MongoTransactionContext txnctx) throws TransactionManagerException {
        super._applyWS(txnctx);
        fireTransactionCommitEvent();
    }
    
    
    /*
    public void startTransaction(TxnCtx txnCtx) throws TransactionManagerException {
        
        //LTMClient.getInstance().getConnection().startTransaction();
        //TxnCtx txnContext = LTMClient.getInstance().getConnection().getTxnCtx();
        //LTMClient.getInstance().associate(DataStoreId.MONGODB, txnContext);
        this.tid = txnCtx.getCtxId();
        this.version = txnCtx.getStartTimestamp();
        this.mongoMVCCClient.addToOpenTransactions(this, tid);
    }
    */
}
