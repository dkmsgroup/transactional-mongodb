package com.mongodb;

import com.mongodb.client.ListCollectionsIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.CreateCollectionOptions;
import com.mongodb.client.model.IndexOptions;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class CpMongoDatabaseImpl extends AbstractMongoDatabaseImpl implements MongoDatabase {
    private static final Logger Log = LoggerFactory.getLogger(CpMongoDatabaseImpl.class);
    
    private final MongoDatabase _deprecatedDb;
    
    protected CpMongoDatabaseImpl(long dbID, String databaseName, DeprecatedMongoClient _depracedMongoClient, MongoClient mongoClient, MongoDatabase _deprecatedDb) {
        super(dbID, _depracedMongoClient, mongoClient, databaseName);
        this._deprecatedDb = _deprecatedDb;
    }
    
   

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public CodecRegistry getCodecRegistry() {
        return this._deprecatedDb.getCodecRegistry();
    }

    @Override
    public ReadPreference getReadPreference() {
        return this._deprecatedDb.getReadPreference();
    }

    @Override
    public WriteConcern getWriteConcern() {
        return this._deprecatedDb.getWriteConcern();
    }

    @Override
    public MongoDatabase withCodecRegistry(CodecRegistry codecRegistry) {
        this._deprecatedDb.withCodecRegistry(codecRegistry);
        return this;
    }

    @Override
    public MongoDatabase withReadPreference(ReadPreference readPreference) {
        this._deprecatedDb.withReadPreference(readPreference);
        return this;
    }

    @Override
    public MongoDatabase withWriteConcern(WriteConcern writeConcern) {
        this._deprecatedDb.withWriteConcern(writeConcern);
        return this;
    }
    
    

    @Override
    public MongoCollection<Document> getCollection(String collectionName) {
        return getCollection(collectionName, Document.class);
    }

    @Override
    public <TDocument> MongoCollection<TDocument> getCollection(String collectionName, Class<TDocument> documentClass) {
        if(!this.dbOpenCollections.containsKey(collectionName)) {
            MongoCollection<TDocument> deprecatedCollection = this._deprecatedDb.getCollection(collectionName, documentClass);
            CpMongoCollectionImpl mongoCollection = new CpMongoCollectionImpl(collectionName, Document.class, deprecatedCollection, this.mongoTransactionalContexct, this);
            this.dbOpenCollections.putIfAbsent(collectionName, mongoCollection);
        }
        return (CpMongoCollectionImpl<TDocument>) this.dbOpenCollections.get(collectionName);
    }
    
    

    @Override
    public Document runCommand(Bson command) {
        return this._deprecatedDb.runCommand(command);
    }

    @Override
    public Document runCommand(Bson command, ReadPreference readPreference) {
        return this._deprecatedDb.runCommand(command, readPreference);
    }

    @Override
    public <TResult> TResult runCommand(Bson command, Class<TResult> resultClass) {
        return this._deprecatedDb.runCommand(command, resultClass);
    }

    @Override
    public <TResult> TResult runCommand(Bson command, ReadPreference readPreference, Class<TResult> resultClass) {
        return this.runCommand(command, readPreference, resultClass);
    }

    
    @Override
    public void drop() {
        _close();
        this._deprecatedDb.drop();
    }

    @Override
    public MongoIterable<String> listCollectionNames() {
        return this._deprecatedDb.listCollectionNames();
    }

    @Override
    public ListCollectionsIterable<Document> listCollections() {
        return this._deprecatedDb.listCollections();
    }

    @Override
    public <TResult> ListCollectionsIterable<TResult> listCollections(Class<TResult> resultClass) {
        return this._deprecatedDb.listCollections(resultClass);
    }

    @Override
    public void createCollection(String collectionName) {
        this._deprecatedDb.createCollection(collectionName);
        MongoCollection<Document> collection = this._deprecatedDb.getCollection(collectionName);
        if(collection!=null)
            createDefaultIndex(collection);
    }

    @Override
    public void createCollection(String collectionName, CreateCollectionOptions createCollectionOptions) {
        this._deprecatedDb.createCollection(collectionName, createCollectionOptions);
        MongoCollection<Document> collection = this._deprecatedDb.getCollection(collectionName);
        if(collection!=null)
            createDefaultIndex(collection);        
    }

    
    
    private void createDefaultIndex(final MongoCollection<Document> collection) {
        
        Document indexes = new Document(MongoMVCCFields.DATA_ID, 1)
                    .append(MongoMVCCFields.COMMIT_TIMESTAMP, 1)
                    .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, 1);
        
        //Document indexes =  new Document(MongoMVCCFields.DATA_ID, 1); 
        collection.createIndex(indexes, new IndexOptions().unique(true).name(MongoMVCCFields.DEFAULT_COMPOUND_INDEX));
        
        
        indexes = new Document(MongoMVCCFields.TID, 1)
                .append(MongoMVCCFields.COMMIT_TIMESTAMP, 1);
        collection.createIndex(indexes, new IndexOptions().unique(false).name(MongoMVCCFields.DEFAULT_COMPOUND_INDEX_TID));
        
    }
}
