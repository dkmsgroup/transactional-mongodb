package com.mongodb.utils;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.model.ReturnDocument;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.UpdateResult;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import eu.coherentpaas.mongodb.exceptions.MongoMVCCException;
import eu.coherentpaas.mongodb.utils.ByteOperators;
import eu.coherentpaas.mongodb.utils.MongoTransactionContext;
import eu.coherentpaas.mongodb.utils.VersionQueryUtils;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentMap;
import org.bson.BsonBinary;
import org.bson.BsonBinarySubType;
import org.bson.BsonDocument;
import org.bson.BsonDocumentReader;
import org.bson.BsonInt64;
import org.bson.BsonObjectId;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 * @param <TDocument>
 */
public class CpUpdator<TDocument> {
    private static final Logger Log = LoggerFactory.getLogger(CpUpdator.class);
    
    protected final ConcurrentMap<Object, Bson> writeset;
    protected final Bson filter;
    protected final MongoTransactionContext mongoTransactionContext;
    protected final MongoCollection _depracedDBCollection;
    protected final Class<TDocument> documentClass;
    protected final CodecRegistry codecRegistry;
    protected final boolean multi;
    protected boolean foundAny = true;
    protected VersionQueryUtils versionQueryUtils;
    
    public CpUpdator(ConcurrentMap<Object, Bson> writeset, Bson filter, MongoTransactionContext mongoTransactionContext, MongoCollection _depracedDBCollection, Class<TDocument> documentClass, CodecRegistry codecRegistry, boolean multi, VersionQueryUtils versionQueryUtils) {
        this.writeset = writeset;
        this.filter = filter;
        this.mongoTransactionContext = mongoTransactionContext;
        this._depracedDBCollection = _depracedDBCollection;
        this.documentClass = documentClass;
        this.codecRegistry = codecRegistry;
        this.multi = multi;
        this.versionQueryUtils = versionQueryUtils;
    }
    
    
    public UpdateResult updateSet(Bson updateSetBson, UpdateOptions options) throws MongoMVCCException, TransactionManagerException {
        long countTotal = 0;
        BsonDocument query = filter.toBsonDocument(documentClass, codecRegistry);
        BsonDocument updateSet = updateSetBson.toBsonDocument(documentClass, codecRegistry);
        
        //check if starts with '$set' operator
        String setKey = updateSet.keySet().iterator().next();
        if(!setKey.equals("$set")) {
            IllegalArgumentException ex = new IllegalArgumentException("update query does not start with the '$set' operator");
            throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
        }
        
        
        //first update the private versions
        if(!this.writeset.isEmpty()) {
            BsonDocument queryVersioned = VersionQueryUtils
                    .getQueryPrivateVersion(
                            BsonDocument.parse(query.toJson()), mongoTransactionContext.getTid(), codecRegistry);

            //do not upsert at this phase --will check at the end
            UpdateOptions _options = new UpdateOptions().upsert(false);
            UpdateResult intermediateRes;
            if(multi)
                intermediateRes = this._depracedDBCollection.updateMany(queryVersioned, updateSet, _options);
            else
                intermediateRes = this._depracedDBCollection.updateOne(queryVersioned, updateSet, _options);

            if((intermediateRes!=null)&&(!intermediateRes.wasAcknowledged()))
                throw new MongoMVCCException("Could not remove from transaction's private versions");

            if(intermediateRes!=null)
                countTotal = countTotal + intermediateRes.getModifiedCount();
            if((countTotal>0)&&(!multi)) { //update only one -- found one, now return
                return UpdateResult.acknowledged(1, 1L, null);
            }
        }

        
        //now update the old ones that are not in the private writeset
        List<TDocument> versionsToInsert = new ArrayList<>();
        try (MongoCursor<TDocument> cursor = this._depracedDBCollection
                .find(versionQueryUtils.getQueryVersion(BsonDocument.parse(query.toJson()), mongoTransactionContext.getTid()), documentClass)
                .iterator()) {
            while(cursor.hasNext()) {
                TDocument obj = cursor.next();
                
                if(!(obj instanceof Bson)) {
                    IllegalStateException ex = new IllegalStateException("Documents in database are not in BSON format");
                    throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
                }
                
                BsonDocument dbObject = ((Bson)obj).toBsonDocument(documentClass, codecRegistry);
                BsonValue currentObjectId = dbObject.get(MongoMVCCFields.DATA_ID);
                
                //if there's not in the private writeset (if it was, then the new one has already been updated
                if(!writeset.containsKey(currentObjectId)) {
                    //get the key in byte [] to be checked for conflicts
                    byte [] key = ByteOperators.getKeyBytes(MongoMVCCFields.STRING_SEPARATOR, currentObjectId);

                    //if transaction fails, then a TransactionManagerException will be thrown which will lead to a rollback
                    this.mongoTransactionContext.checkForConflicts(key);

                    countTotal++;
                    versionsToInsert.add(updateDocument(obj, updateSet, options));
                    this.writeset.putIfAbsent(currentObjectId, dbObject);
                    
                    if((countTotal>0)&&(!multi)) //update one only
                        break;
                }
            }
        }
        
        //is upsert and nothing found so far
        ObjectId objId = null;
        if((countTotal==0)&&(options.isUpsert())) {
            //add a new 
            objId = new ObjectId();
            TDocument newDoc = createNewFoUpsert(updateSet, objId);
            versionsToInsert.add(newDoc);
            this.writeset.putIfAbsent(objId, ((Bson)newDoc).toBsonDocument(documentClass, codecRegistry));
            countTotal++;
        }

        
        //now add the new versions
        try {
            if(!versionsToInsert.isEmpty())
                this._depracedDBCollection.insertMany(versionsToInsert);
        } catch(Exception ex) {
            throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
        }
        
        if(objId==null) //no upsert took place
            return UpdateResult.acknowledged(countTotal, countTotal, null);
        else
            return UpdateResult.acknowledged(0, 1L, new BsonObjectId(objId));
    }
    
    
    public TDocument findAndupdateSet(Bson updateSetBson, FindOneAndUpdateOptions options) throws MongoMVCCException, TransactionManagerException {
        long countTotal = 0;
        BsonDocument query = filter.toBsonDocument(documentClass, codecRegistry);
        BsonDocument updateSet = updateSetBson.toBsonDocument(documentClass, codecRegistry);
        
        //check if starts with '$set' operator
        String setKey = updateSet.keySet().iterator().next();
        if(!setKey.equals("$set")) {
            IllegalArgumentException ex = new IllegalArgumentException("update query does not start with the '$set' operator");
            throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
        }
        
        
        if(!this.writeset.isEmpty()) {
            //first update the private versions
            BsonDocument queryVersioned = VersionQueryUtils
                    .getQueryPrivateVersion(
                            BsonDocument.parse(query.toJson()), mongoTransactionContext.getTid(), codecRegistry);

            //do not upsert at this phase --will check at the end
            boolean sort = options.isUpsert();
            options.upsert(false);


            TDocument res = null;
            try {
                res = (TDocument) this._depracedDBCollection.findOneAndUpdate(queryVersioned, updateSet, options);
            } catch(Exception ex) {
                throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
            }

            //updated one, return it
            if(res!=null) {
                return fixIdentifier(res);
            }


            options.upsert(sort);
        }
        
        //now update the old ones that are not in the private writeset
        List<TDocument> versionsToInsert = new ArrayList<>();
        TDocument obj = null;
        try (MongoCursor<TDocument> cursor = this._depracedDBCollection
                .find(versionQueryUtils.getQueryVersion(BsonDocument.parse(query.toJson()), mongoTransactionContext.getTid()), documentClass)
                .sort(options.getSort())
                .iterator()) {
            while(cursor.hasNext()) {
                obj = cursor.next();
                
                if(!(obj instanceof Bson)) {
                    IllegalStateException ex = new IllegalStateException("Documents in database are not in BSON format");
                    throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
                }
                
                BsonDocument dbObject = ((Bson)obj).toBsonDocument(documentClass, codecRegistry);
                BsonValue currentObjectId = dbObject.get(MongoMVCCFields.DATA_ID);
                
                //if there's not in the private writeset (if it was, then the new one has already been updated
                if(!writeset.containsKey(currentObjectId)) {
                    //get the key in byte [] to be checked for conflicts
                    byte [] key = ByteOperators.getKeyBytes(MongoMVCCFields.STRING_SEPARATOR, currentObjectId);

                    //if transaction fails, then a TransactionManagerException will be thrown which will lead to a rollback
                    this.mongoTransactionContext.checkForConflicts(key);

                    countTotal++;
                    versionsToInsert.add(updateDocument(obj, updateSet, options));
                    this.writeset.putIfAbsent(currentObjectId, dbObject);
                    
                    //update one only
                    break;
                }
            }
        }
        
        //is upsert and nothing found so far
        ObjectId objId = null;
        if((countTotal==0)&&(options.isUpsert())) {
            //add a new 
            objId = new ObjectId();
            TDocument newDoc = createNewFoUpsert(updateSet, objId);
            versionsToInsert.add(newDoc);
            this.writeset.putIfAbsent(objId, ((Bson)newDoc).toBsonDocument(documentClass, codecRegistry));
            countTotal++;
        }

        
        //now add the new versions
        try {
            if(!versionsToInsert.isEmpty())
                this._depracedDBCollection.insertMany(versionsToInsert);
        } catch(Exception ex) {
            throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
        }
        
        if(objId==null) { //no upsert took place
            if(!versionsToInsert.isEmpty()) {
                //have added one
                if(options.getReturnDocument().equals(ReturnDocument.AFTER))
                    return fixIdentifier(versionsToInsert.get(0));
                else
                    return fixIdentifier(obj);
            }
        } else {//upsert --added one just now
            if(!versionsToInsert.isEmpty()) {
                //have added one
                if(options.getReturnDocument().equals(ReturnDocument.AFTER))
                    return versionsToInsert.get(0);
                else
                    return null;
            }
            
            Log.error("no document found, even if there was one document added by upsert");
        }
            
        //unreacheable statement
        return null;
    }
    
    
    
    public UpdateResult updateReplace(TDocument document, UpdateOptions options) throws MongoMVCCException, TransactionManagerException {
        long countTotal = 0;
        
        BsonDocument query = filter.toBsonDocument(documentClass, codecRegistry);
        if(!(document instanceof Bson)) {
            IllegalStateException ex = new IllegalStateException("Replace document is not in BSON format");
            throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
        }
        
        BsonDocument objectReplaceQuery = ((Bson)document).toBsonDocument(documentClass, codecRegistry);
        
        if(!this.writeset.isEmpty()) {
            //first update the private versions
            BsonDocument queryVersioned = VersionQueryUtils
                    .getQueryPrivateVersion(
                            BsonDocument.parse(query.toJson()), mongoTransactionContext.getTid(), codecRegistry);
            try (MongoCursor<TDocument> cursor = this._depracedDBCollection.find(queryVersioned, documentClass).iterator()) {
                if(cursor.hasNext()) {
                    TDocument toReplace = cursor.next();
                    if(!(toReplace instanceof Bson)) {
                        IllegalStateException ex = new IllegalStateException("Documents in the database are not in BSON format");
                        throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
                    }
                    BsonDocument toReplaceDoc = ((Bson)toReplace).toBsonDocument(documentClass, codecRegistry);
                    BsonValue objectId = toReplaceDoc.get(MongoMVCCFields.DATA_ID);

                    //append the previous DATA_ID value
                    objectReplaceQuery.append(MongoMVCCFields.DATA_ID, objectId);

                    //append the new _ID
                    objectReplaceQuery.append(MongoMVCCFields._ID, new BsonBinary(
                            BsonBinarySubType.USER_DEFINED,
                            ByteOperators.getBytes(objectReplaceQuery.get(MongoMVCCFields.DATA_ID), this.mongoTransactionContext.getTid())));               

                    //add the correspoding values (if there are missing)
                    objectReplaceQuery.append(MongoMVCCFields.TID, new BsonInt64(mongoTransactionContext.getTid()))
                            .append(MongoMVCCFields.COMMIT_TIMESTAMP, new BsonString(""))
                            .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, new BsonString(""));

                    //and update
                    queryVersioned = queryVersioned.append(MongoMVCCFields.DATA_ID, objectId);
                    UpdateResult res = null; 
                    try {
                        res = this._depracedDBCollection.replaceOne(queryVersioned, objectReplaceQuery, options);
                    } catch(Exception ex) {
                        throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
                    }

                    //one found, one updated, now return
                    return res;
                }
            }
        }
        
        
        //no private found for replace, replace with one new
        List<TDocument> versionsToInsert = new ArrayList<>();
        try (MongoCursor<TDocument> cursor = this._depracedDBCollection
                .find(versionQueryUtils.getQueryVersion(BsonDocument.parse(query.toJson()), mongoTransactionContext.getTid()), documentClass)
                .iterator()) {
            while(cursor.hasNext()) {
                TDocument toReplace = cursor.next();
                if(!(toReplace instanceof Bson)) {
                    IllegalStateException ex = new IllegalStateException("Documents in the database are not in BSON format");
                    throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
                }
                BsonDocument toReplaceDoc = ((Bson)toReplace).toBsonDocument(documentClass, codecRegistry);
                BsonValue currentObjectId = toReplaceDoc.get(MongoMVCCFields.DATA_ID);
                
                //if there's not in the private writeset (if it was, then the new one has already been updated
                if(!writeset.containsKey(currentObjectId)) {
                    //get the key in byte [] to be checked for conflicts
                    byte [] key = ByteOperators.getKeyBytes(MongoMVCCFields.STRING_SEPARATOR, currentObjectId);

                    //if transaction fails, then a TransactionManagerException will be thrown which will lead to a rollback
                    this.mongoTransactionContext.checkForConflicts(key);
                    
                    
                    //append the previous DATA_ID value
                    objectReplaceQuery.append(MongoMVCCFields.DATA_ID, currentObjectId);

                    //append the new _ID
                    objectReplaceQuery.append(MongoMVCCFields._ID, new BsonBinary(
                            BsonBinarySubType.USER_DEFINED,
                            ByteOperators.getBytes(objectReplaceQuery.getObjectId(MongoMVCCFields.DATA_ID).getValue(), this.mongoTransactionContext.getTid())));               
                    
                     objectReplaceQuery.append(MongoMVCCFields._ID, 
                                            new BsonBinary(
                                                    BsonBinarySubType.USER_DEFINED,
                                                    ByteOperators.getBytes(currentObjectId, this.mongoTransactionContext.getTid())))
                                 .append(MongoMVCCFields.TID, new BsonInt64(mongoTransactionContext.getTid()))
                                 .append(MongoMVCCFields.COMMIT_TIMESTAMP, new BsonString(""))
                                 .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, new BsonString(""));
                     
                     countTotal++;
                     this.writeset.putIfAbsent(currentObjectId, objectReplaceQuery);
                     versionsToInsert.add(codecRegistry.get(documentClass).decode(
                        new BsonDocumentReader(objectReplaceQuery.toBsonDocument(documentClass, codecRegistry)), null));
                     
                     //found one to replace, break now
                     break;
                }
            }
        }
        
        //is upsert and nothing found so far
        ObjectId objId = null;
        if((countTotal==0)&&(options.isUpsert())) {
            //add a new 
            objId = new ObjectId();
            TDocument newDoc = createNewFoUpsert(objectReplaceQuery, objId);
            versionsToInsert.add(newDoc);
            this.writeset.putIfAbsent(objId, ((Bson)newDoc).toBsonDocument(documentClass, codecRegistry));
            countTotal++;
        }
        
        //now add the new versions
        try {
            if(!versionsToInsert.isEmpty())
                this._depracedDBCollection.insertMany(versionsToInsert);
        } catch(Exception ex) {
            throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
        }
        
        if(objId==null) //no upsert took place
            return UpdateResult.acknowledged(countTotal, countTotal, null);
        else
            return UpdateResult.acknowledged(1, 1L, new BsonObjectId(objId));
        
    }
    
    
    public TDocument findAndReplace(TDocument document, FindOneAndReplaceOptions options) throws MongoMVCCException, TransactionManagerException {
        long countTotal = 0;
        
        BsonDocument query = filter.toBsonDocument(documentClass, codecRegistry);
        if(!(document instanceof Bson)) {
            IllegalStateException ex = new IllegalStateException("Replace document is not in BSON format");
            throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
        }
        
        BsonDocument objectReplaceQuery = ((Bson)document).toBsonDocument(documentClass, codecRegistry);
        
        if(!this.writeset.isEmpty()) {
            //first update the private versions
            BsonDocument queryVersioned = VersionQueryUtils
                    .getQueryPrivateVersion(
                            BsonDocument.parse(query.toJson()), mongoTransactionContext.getTid(), codecRegistry);
            try (MongoCursor<TDocument> cursor = this._depracedDBCollection.find(queryVersioned, documentClass).sort(options.getSort()).iterator()) {
                if(cursor.hasNext()) {
                    TDocument toReplace = cursor.next();
                    if(!(toReplace instanceof Bson)) {
                        IllegalStateException ex = new IllegalStateException("Documents in the database are not in BSON format");
                        throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
                    }
                    BsonDocument toReplaceDoc = ((Bson)toReplace).toBsonDocument(documentClass, codecRegistry);
                    BsonValue objectId = toReplaceDoc.get(MongoMVCCFields.DATA_ID);

                    //append the previous DATA_ID value
                    objectReplaceQuery.append(MongoMVCCFields.DATA_ID, objectId);

                    //append the new _ID
                    objectReplaceQuery.append(MongoMVCCFields._ID, new BsonBinary(
                            BsonBinarySubType.USER_DEFINED,
                            ByteOperators.getBytes(objectReplaceQuery.get(MongoMVCCFields.DATA_ID), this.mongoTransactionContext.getTid())));               

                    //add the correspoding values (if there are missing)
                    objectReplaceQuery.append(MongoMVCCFields.TID, new BsonInt64(mongoTransactionContext.getTid()))
                            .append(MongoMVCCFields.COMMIT_TIMESTAMP, new BsonString(""))
                            .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, new BsonString(""));

                    //and replace
                    queryVersioned = queryVersioned.append(MongoMVCCFields.DATA_ID, objectId);

                    options.upsert(false);

                    TDocument res = null; 
                    try {
                        res = (TDocument) this._depracedDBCollection.findOneAndReplace(queryVersioned, objectReplaceQuery, options);
                    } catch(Exception ex) {
                        throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
                    }

                    //one found, one updated, now return
                    return fixIdentifier(res);
                }
            }
        }
        
        //no private found for replace, replace with one new
        List<TDocument> versionsToInsert = new ArrayList<>();
        TDocument toReplace = null;
        try (MongoCursor<TDocument> cursor = this._depracedDBCollection
                .find(versionQueryUtils.getQueryVersion(BsonDocument.parse(query.toJson()), mongoTransactionContext.getTid()), documentClass)
                .sort(options.getSort())
                .iterator()) {
            while(cursor.hasNext()) {
                toReplace = cursor.next();
                if(!(toReplace instanceof Bson)) {
                    IllegalStateException ex = new IllegalStateException("Documents in the database are not in BSON format");
                    throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
                }
                BsonDocument toReplaceDoc = ((Bson)toReplace).toBsonDocument(documentClass, codecRegistry);
                BsonValue currentObjectId = toReplaceDoc.get(MongoMVCCFields.DATA_ID);
                
                //if there's not in the private writeset (if it was, then the new one has already been updated
                if(!writeset.containsKey(currentObjectId)) {
                    //get the key in byte [] to be checked for conflicts
                    byte [] key = ByteOperators.getKeyBytes(MongoMVCCFields.STRING_SEPARATOR, currentObjectId);

                    //if transaction fails, then a TransactionManagerException will be thrown which will lead to a rollback
                    this.mongoTransactionContext.checkForConflicts(key);
                    
                    
                    //append the previous DATA_ID value
                    objectReplaceQuery.append(MongoMVCCFields.DATA_ID, currentObjectId);

                    //append the new _ID
                    objectReplaceQuery.append(MongoMVCCFields._ID, new BsonBinary(
                            BsonBinarySubType.USER_DEFINED,
                            ByteOperators.getBytes(objectReplaceQuery.getObjectId(MongoMVCCFields.DATA_ID).getValue(), this.mongoTransactionContext.getTid())));               
                    
                     objectReplaceQuery.append(MongoMVCCFields._ID, 
                                            new BsonBinary(
                                                    BsonBinarySubType.USER_DEFINED,
                                                    ByteOperators.getBytes(currentObjectId, this.mongoTransactionContext.getTid())))
                                 .append(MongoMVCCFields.TID, new BsonInt64(mongoTransactionContext.getTid()))
                                 .append(MongoMVCCFields.COMMIT_TIMESTAMP, new BsonString(""))
                                 .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, new BsonString(""));
                     
                     countTotal++;
                     this.writeset.putIfAbsent(currentObjectId, objectReplaceQuery);
                     versionsToInsert.add(codecRegistry.get(documentClass).decode(
                        new BsonDocumentReader(objectReplaceQuery.toBsonDocument(documentClass, codecRegistry)), null));
                     
                     //found one to replace, break now
                     break;
                }
            }
        }
        
        //is upsert and nothing found so far
        ObjectId objId = null;
        if((countTotal==0)&&(options.isUpsert())) {
            //add a new 
            objId = new ObjectId();
            TDocument newDoc = createNewFoUpsert(objectReplaceQuery, objId);
            versionsToInsert.add(newDoc);
            this.writeset.putIfAbsent(objId, ((Bson)newDoc).toBsonDocument(documentClass, codecRegistry));
            countTotal++;
        }
        
        //now add the new versions
        try {
            if(!versionsToInsert.isEmpty())
                this._depracedDBCollection.insertMany(versionsToInsert);
        } catch(Exception ex) {
            throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
        }
        
        if(objId==null) { //no upsert took place
            if(!versionsToInsert.isEmpty()) {
                //have added one
                if(options.getReturnDocument().equals(ReturnDocument.AFTER))
                    return fixIdentifier(versionsToInsert.get(0));
                else
                    return fixIdentifier(toReplace);
            }
        } else {//upsert --added one just now
            if(!versionsToInsert.isEmpty()) {
                //have added one
                if(options.getReturnDocument().equals(ReturnDocument.AFTER))
                    return versionsToInsert.get(0);
                else
                    return null;
            }
            
            Log.error("no document found, even if there was one document added by upsert");
        }
            
        //unreacheable statement
        return null;
        
    }
    
    
    
    private TDocument updateDocument(TDocument bson, BsonDocument querySet, UpdateOptions options) throws MongoMVCCException {
        return _updateDocument(bson, querySet, options.isUpsert());
    }
    
    private TDocument updateDocument(TDocument bson, BsonDocument querySet, FindOneAndUpdateOptions options) throws MongoMVCCException {
        return _updateDocument(bson, querySet, options.isUpsert());
    }
    
    
    private TDocument _updateDocument(TDocument bson, BsonDocument querySet, boolean isUpsert) throws MongoMVCCException {
        Document dbObject = Document.parse(
                ((Bson)bson).toBsonDocument(documentClass, codecRegistry).toJson());
        
        
        String key = querySet.keySet().iterator().next();
        if(!key.equals("$set"))
            throw new MongoMVCCException("not a valid set parameter");
        
        querySet.values().iterator().next();
        
        BsonDocument values = (BsonDocument) querySet.values().iterator().next();
        for (Entry entry : values.entrySet()) {
            String documentField = (String) entry.getKey();
            Object documentFieldValue = entry.getValue();
            if(isUpsert) {
                //put the value, even if the specific field does not exist
                dbObject.put(documentField, documentFieldValue);
            } else {
                //check if the field exists and update only if exists
                if(dbObject.containsKey(documentField))
                    dbObject.put(documentField, documentFieldValue);
            }
        }
        
        //remove its _id and timestamps and add the new ones
        dbObject.remove(MongoMVCCFields._ID);
        dbObject.remove(MongoMVCCFields.TID);
        dbObject.remove(MongoMVCCFields.COMMIT_TIMESTAMP);
        dbObject.remove(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP);
        
        dbObject.append(MongoMVCCFields.TID, this.mongoTransactionContext.getTid())
                .append(MongoMVCCFields.COMMIT_TIMESTAMP, "")
                .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, "")
                .append(MongoMVCCFields._ID, ByteOperators.getBytes(dbObject.get(MongoMVCCFields.DATA_ID), this.mongoTransactionContext.getTid()));
        
        
        return codecRegistry.get(documentClass).decode(
                new BsonDocumentReader(dbObject.toBsonDocument(documentClass, codecRegistry)), null);
    }
    
    private TDocument createNewFoUpsert(BsonDocument querySet, ObjectId objectId) throws MongoMVCCException {
        Document dbObject = new Document();
        
        String key = querySet.keySet().iterator().next();
        if(!key.equals("$set"))
            throw new MongoMVCCException("not a valid set parameter");
        
        querySet.values().iterator().next();
        
        BsonDocument values = (BsonDocument) querySet.values().iterator().next();
        for (Entry entry : values.entrySet()) {
            String documentField = (String) entry.getKey();
            Object documentFieldValue = entry.getValue();
        }
        
        dbObject.append(MongoMVCCFields.DATA_ID, objectId);
                
        dbObject.append(MongoMVCCFields.TID, this.mongoTransactionContext.getTid())
                .append(MongoMVCCFields.COMMIT_TIMESTAMP, "")
                .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, "")
                .append(MongoMVCCFields._ID, ByteOperators.getBytes(dbObject.getObjectId(MongoMVCCFields.DATA_ID), this.mongoTransactionContext.getTid()));
        
        
        return codecRegistry.get(documentClass).decode(
                new BsonDocumentReader(dbObject.toBsonDocument(documentClass, codecRegistry)), null);
    }
    
    /**
     * 
     * Receives a TDocument document. It then moves the DATA_ID to the ID, where it is expected to be found by the user
     * 
     * @param bson
     * @return TDocument
     */
    private TDocument fixIdentifier(final TDocument bson) {
        BsonDocument dbObject = ((Bson)bson).toBsonDocument(documentClass, codecRegistry);
        BsonValue objID = dbObject.get(MongoMVCCFields.DATA_ID);
        dbObject.put(MongoMVCCFields._ID, objID);
        dbObject.remove(MongoMVCCFields.DATA_ID);
        
        return codecRegistry.get(documentClass).decode(new BsonDocumentReader(dbObject), null);
    }
    
}
