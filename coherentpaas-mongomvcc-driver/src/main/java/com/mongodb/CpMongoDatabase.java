package com.mongodb;

import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.datastore.DataStoreId;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.io.Closeable;

/**
 *
 * @author idezol
 */
public interface CpMongoDatabase extends Closeable {
    
    public String getName();
    
    
    public void startTransaction() throws TransactionManagerException;
    
    public void commit() throws TransactionManagerException, DataStoreException;
    
    public void rollback() throws TransactionManagerException;
      
    
}
