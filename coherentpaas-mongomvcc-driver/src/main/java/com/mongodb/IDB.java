package com.mongodb;

import java.util.Set;

/**
 *
 * @author stavroula
 */
public interface IDB extends CpMongoDatabase {
    public IDBCollection getCollection( String name );
    
    public IDBCollection createCollection(final String name, final DBObject options);
    
    public IDBCollection getCollectionFromString(String s);
    
    public CommandResult command(final DBObject cmd);
    
    public CommandResult command(final DBObject cmd, final DBEncoder encoder);
    
    public CommandResult command(final DBObject cmd, final ReadPreference readPreference, final DBEncoder encoder);
    
    public CommandResult command(final DBObject cmd, final ReadPreference readPreference);
    
    public CommandResult command(final String cmd);
    
    public CommandResult command(final String cmd, final ReadPreference readPreference);
    
    public CommandResult doEval( String code , Object ... args );
    
    public Object eval( String code , Object ... args );
    
    public CommandResult getStats();
    
    public Set<String> getCollectionNames();
    
    public boolean collectionExists(String collectionName);
    
    public void setWriteConcern(final WriteConcern writeConcern);
    
    public WriteConcern getWriteConcern();
    
    public void setReadPreference(final ReadPreference readPreference);
    
    public ReadPreference getReadPreference();
    
    public void dropDatabase();
    
    public IDB getSisterDB( String name );
    
    public void addOption( int option );
    
    public void setOptions( int options );
    
    public void resetOptions();
    
    public int getOptions();
    
    public WriteResult addUser( String username , char[] passwd, boolean readOnly );
    
    public WriteResult removeUser( String username );
    
    public void cleanCursors( boolean force );
    
    
}