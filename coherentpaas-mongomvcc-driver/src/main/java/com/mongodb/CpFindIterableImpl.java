package com.mongodb;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoIterable;
import eu.coherentpaas.mongodb.utils.VersionQueryUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonValue;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;

/**
 *
 * @author idezol
 */
final class CpFindIterableImpl<TDocument, TResult> implements FindIterable<TResult> {
    private final Class<TDocument> documentClass;
    private final Class<TResult> resultClass;
    private final FindIterableImpl findIterable;
    private final CollectionBean cpCollection;
    private final CodecRegistry codecRegistry;
    private Bson filter;
    private int skip = 0;
    private int limit = 0;

    protected CpFindIterableImpl(Class<TDocument> documentClass, Class<TResult> resultClass, FindIterableImpl findIterable, CollectionBean cpCollection, Bson filter, CodecRegistry codecRegistry) {
        this.documentClass = documentClass;
        this.resultClass = resultClass;
        this.findIterable = findIterable;
        this.cpCollection = cpCollection;
        this.filter = filter;
        this.codecRegistry = codecRegistry;
    }
    
    
    @Override
    public FindIterable<TResult> filter(Bson filter) {
        this.filter = applyVersionFilters(filter);
        this.findIterable.filter(this.filter);
        return this;
    }
    

    @Override
    public FindIterable<TResult> limit(int limit) {
        this.limit = limit;
        this.findIterable.limit(limit);
        return this;
    }

    @Override
    public FindIterable<TResult> skip(int skip) {
        this.skip = skip;
        this.findIterable.skip(skip);
        return this;
    }

    @Override
    public FindIterable<TResult> maxTime(long maxTime, TimeUnit timeUnit) {
        this.findIterable.maxTime(maxTime, timeUnit);
        return this;
    }

    @Override
    public FindIterable<TResult> modifiers(Bson modifiers) {
        this.findIterable.modifiers(modifiers);
        return this;
    }

    @Override
    public FindIterable<TResult> projection(Bson projection) {
        this.findIterable.projection(projection);
        return this;
    }

    @Override
    public FindIterable<TResult> sort(Bson sort) {
        this.findIterable.sort(sort);
        return this;
    }

    @Override
    public FindIterable<TResult> noCursorTimeout(boolean noCursorTimeout) {
        this.findIterable.noCursorTimeout(noCursorTimeout);
        return this;
    }

    @Override
    public FindIterable<TResult> oplogReplay(boolean oplogReplay) {
        this.findIterable.oplogReplay(oplogReplay);
        return this;
    }

    @Override
    public FindIterable<TResult> partial(boolean partial) {
        this.findIterable.partial(partial);
        return this;
    }

    @Override
    public FindIterable<TResult> cursorType(CursorType cursorType) {
        this.findIterable.cursorType(cursorType);
        return this;
    }

    @Override
    public FindIterable<TResult> batchSize(int batchSize) {
        this.findIterable.batchSize(batchSize);
        return this;
    }
    

    @Override
    public MongoCursor<TResult> iterator() {        
        MongoCursor<TResult> mongoCursor = this.findIterable.iterator();
        CpMongoCursorImpl cpCursor = new CpMongoCursorImpl(mongoCursor, resultClass, cpCollection, this.codecRegistry, this.skip, this.limit);
        //this.cpCollection.collection.clientCursors.add(cpCursor);
        return cpCursor;
    }

    @Override
    public TResult first() {
        TResult result;
        try (MongoCursor<TResult> mongoCursor = this.iterator()) {
            result = mongoCursor.hasNext() ? mongoCursor.next() : null;
        }
        return result;
    }

    
    @Override
    public <U> MongoIterable<U> map(Function<TResult, U> mapper) {
        return new MappingIterable<>(this, mapper);
    }

    @Override
    public void forEach(Block<? super TResult> block) {
        try (MongoCursor<TResult> cursor = iterator()) {
            while(cursor.hasNext()) {
                block.apply(cursor.next());
            }
        }
    }

    @Override
    public <A extends Collection<? super TResult>> A into(final A target) {
        forEach(new Block<TResult>() {
            @Override
            public void apply(TResult t) {
                target.add(t);
            }
        });
        
        return target;
    }
    
    
    private BsonDocument applyVersionFilters(Bson filter) {
        VersionQueryUtils versionQueryUtils = new VersionQueryUtils();
        BsonDocument bsonDocument = filter.toBsonDocument(documentClass, codecRegistry);
        List<BsonValue> listQuerys = new ArrayList<>();
        listQuerys.add(VersionQueryUtils.getQueryPrivateVersion(BsonDocument.parse(bsonDocument.toJson()), this.cpCollection.mongoTransactionalContext.getTid(), codecRegistry));
        listQuerys.add(versionQueryUtils.getQueryVersion(BsonDocument.parse(bsonDocument.toJson()), this.cpCollection.mongoTransactionalContext.getStartTimestamp()));
        BsonDocument query = new BsonDocument("$or", new BsonArray(listQuerys)); 
        return query;
    }
    
}
