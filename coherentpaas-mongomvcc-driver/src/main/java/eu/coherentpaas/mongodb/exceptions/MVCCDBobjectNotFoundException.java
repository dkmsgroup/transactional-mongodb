package eu.coherentpaas.mongodb.exceptions;

/**
 *
 * @author idezol
 */
public class MVCCDBobjectNotFoundException extends Exception {
    
    public MVCCDBobjectNotFoundException(String message, Exception ex) {
        super("Document with id " + message + "does not exists in the collection", ex); 
    }
    
    public MVCCDBobjectNotFoundException(String message) {
        super("Document with id " + message + "does not exists in the collection"); 
    }
}
