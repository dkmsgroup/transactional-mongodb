package eu.coherentpaas.mongodb.exceptions;

/**
 *
 * @author idezol
 */
public class MVCCNotActiveCollectionException extends Exception {
    
    public MVCCNotActiveCollectionException(String collectionName) {
        super("Collection " + collectionName + " is not opened in the context of this transaction.");
    }
     public MVCCNotActiveCollectionException(String collectionName, Exception ex) {
        super("Collection " + collectionName + " is not opened in the context of this transaction.", ex);
    }
}
