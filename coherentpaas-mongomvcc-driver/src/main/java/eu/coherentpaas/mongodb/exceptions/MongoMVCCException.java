package eu.coherentpaas.mongodb.exceptions;

/**
 *
 * @author idezol
 */
public class MongoMVCCException extends Exception {
    
    public enum Severity { Creation, DatabaseConnection, Collection, Execution, Transaction }

    private Severity severity;;
    
     public MongoMVCCException(String message) {
            super(message);
    }

    public MongoMVCCException(Severity severity, String message, Throwable cause) {
            super(message, cause);
            this.severity = severity;
    }

    public Severity getSeverity() {
            return severity;
    }

}
