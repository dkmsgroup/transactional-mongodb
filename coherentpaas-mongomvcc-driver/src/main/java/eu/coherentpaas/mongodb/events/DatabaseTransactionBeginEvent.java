package eu.coherentpaas.mongodb.events;

import java.util.EventObject;

/**
 *
 * @author idezol
 * 
 * This event is fired when an already open database connection starts a new transaction. 
 * It has to inform the mongoClient, so as to include it at his list and be able to manage it
 */
public class DatabaseTransactionBeginEvent extends EventObject {
    
    public DatabaseTransactionBeginEvent(Object source) {
        super(source);
    }
    
}
