package eu.coherentpaas.mongodb.events;

/**
 *
 * @author idezol
 */
public interface DatabaseTransactionRollbackListener {
    
    public void databaseTransactionRollbackDetected(DatabaseTransactionRollbackEvent event);
    
}
