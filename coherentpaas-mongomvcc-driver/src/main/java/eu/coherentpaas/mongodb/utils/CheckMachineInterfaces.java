package eu.coherentpaas.mongodb.utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.Enumeration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class CheckMachineInterfaces {
    private static final Logger Log = LoggerFactory.getLogger(CheckMachineInterfaces.class);
    
    public static void main(String [] args) {
        getMyIp();
    }
    
    public static String getMyIp(){
        String result = "127.0.0.1";
        try {
          Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
                for (NetworkInterface netint : Collections.list(nets)){
                  for (InetAddress inetA : Collections.list(netint.getInetAddresses())){
                      Log.info(netint.getName());
                      Log.info(inetA.getHostAddress());
                      Log.info("");
                      System.out.println(netint.getName());
                      System.out.println(inetA.getHostAddress());
                      System.out.println("");
                   if (!inetA.isAnyLocalAddress())
                    result = inetA.getHostAddress();
                  }
                }
                Log.info("result: " + result);
                System.out.println("result: " + result);
        } catch (Exception e) {
         e.printStackTrace();
        }
        return result;
       }
    
}
