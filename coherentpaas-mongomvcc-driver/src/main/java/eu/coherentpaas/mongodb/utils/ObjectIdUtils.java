package eu.coherentpaas.mongodb.utils;

import java.nio.ByteBuffer;
import org.bson.types.ObjectId;

/**
 *
 * @author idezol
 */
public class ObjectIdUtils {
    
    
    public static ObjectId toObjectId(long value) {
        byte [] bytes = ByteBuffer.allocate(12).putLong(value).put(new byte[4]).array();  
        return new ObjectId(bytes);
    }
    
    
    public static long objectIdToLong(ObjectId objectId) {
        ByteBuffer bf = ByteBuffer.wrap(objectId.toByteArray(), 0, 8);
        return bf.asLongBuffer().get();
    }
    
}
