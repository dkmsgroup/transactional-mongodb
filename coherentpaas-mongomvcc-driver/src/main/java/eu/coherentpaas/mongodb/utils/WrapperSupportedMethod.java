package eu.coherentpaas.mongodb.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author idezol
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface WrapperSupportedMethod {
    String name();
    String arguments();
    WrapperMethodResponseType responseType();
}
