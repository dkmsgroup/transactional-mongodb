package eu.coherentpaas.mongodb.utils;

import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.datastore.DataStoreId;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;

/**
 *
 * @author idezol
 */
public class MongoTransactionContext {
    
    private long tid = 0;
    private long startTimestamp = 0;
    private long commitTimestamp = 0;
    
    public MongoTransactionContext() {}

    public MongoTransactionContext(long tid, long startTimestamp, long commitTimestamp) {
        this.tid = tid;
        this.startTimestamp = startTimestamp;
        this.commitTimestamp = commitTimestamp;
    }
    
    public long getTid() {
        return tid;
    }

    public void setTid(long tid) {
        this.tid = tid;
    }

    public long getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(long startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public long getCommitTimestamp() {
        return commitTimestamp;
    }

    public void setCommitTimestamp(long commitTimestamp) {
        this.commitTimestamp = commitTimestamp;
    }
    
    
    public void checkForConflicts(byte [] key) throws TransactionManagerException {
        LTMClient.getInstance().getConnection().hasConflict(key, DataStoreId.MONGODB);
    }

    @Override
    public String toString() {
        return "MongoTransactionContext{" + "tid=" + tid + ", startTimestamp=" + startTimestamp + ", commitTimestamp=" + commitTimestamp + '}';
    }
    
}
