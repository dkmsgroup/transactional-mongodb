package eu.coherentpaas.mongodb.utils;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.bson.BsonArray;
import org.bson.BsonBoolean;
import org.bson.BsonDocument;
import org.bson.BsonInt64;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.bson.codecs.configuration.CodecRegistry;

/**
 *
 * @author idezol
 */
public class VersionQueryUtils {
    
    //private VersionQueryUtils() {}
    
    /**
     * It generates the appropriate query for fetching the appropriate version
     * In SQL it is similar to a WHERE predicate as follows:
     * (_cmtTmstmp < version) AND ((_nextCmtTmstmp>version)OR(_nextCmtTmstmp IS NULL)) AND (_isDeleted IS NULL)
     * @param query
     * @param version
     * @return 
     */
    public static BasicDBObject getQueryVersion(BasicDBObject query, long version) {
    
        //BasicDBObject queryVersioned = new BasicDBObject(query.toMap());
        List<DBObject> list = new ArrayList<>();
        list.add(new BasicDBObject(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, ""));
        list.add(new BasicDBObject(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, new BasicDBObject("$gt", version)));
        /*
        BasicDBObject queryDistinctVersions = new BasicDBObject();
        queryDistinctVersions.append(MongoMVCCFields.COMMIT_TIMESTAMP, new BasicDBObject("$lte", version))
                .append("$or", list)
                .append(MongoMVCCFields.DELETED_DOCUMENT, new BasicDBObject("$exists", false));
        */
        if(query.containsField("$or")) {
            //if it already contains or, then the versioned or will overwrite the query or
            BasicDBList andList = new BasicDBList();
            andList.add(new BasicDBObject(MongoMVCCFields.COMMIT_TIMESTAMP, new BasicDBObject("$lte", version)));
            andList.add(new BasicDBObject("$or", list));
            andList.add(new BasicDBObject(MongoMVCCFields.DELETED_DOCUMENT, new BasicDBObject("$exists", false)));
            query.append("$and", andList);
        } else {
            query.append(MongoMVCCFields.COMMIT_TIMESTAMP, new BasicDBObject("$lte", version))
                .append("$or", list)
                .append(MongoMVCCFields.DELETED_DOCUMENT, new BasicDBObject("$exists", false));
        }
        
        return query;
    }
    
    private static final BsonDocument emptyNextCommitTimestampElement = new BsonDocument(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, new BsonString(""));
    private static final BsonDocument existsFalseElement =  new BsonDocument("$exists", new BsonBoolean(false));
    private final BsonDocument nextCommitTimestampElement = new BsonDocument(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, new BsonDocument("$gt", new BsonInt64(0)));
    private final BsonDocument commitTimestampElement = new BsonDocument("$lte", new BsonInt64(0));
    /**
     * It generates the appropriate query for fetching the appropriate version
     * In SQL it is similar to a WHERE predicate as follows:
     * (_cmtTmstmp < version) AND ((_nextCmtTmstmp>version)OR(_nextCmtTmstmp IS NULL)) AND (_isDeleted IS NULL)
     * @param query
     * @param version
     * @return 
     */
    public BsonDocument getQueryVersion(BsonDocument query, long version) {
    
        //BsonDocument queryVersioned = query.toBsonDocument(query.getClass(), codecRegistry);
        List<BsonValue> list = new ArrayList<>(2);
        list.add(emptyNextCommitTimestampElement);
        //list.add(new BsonDocument(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, new BsonDocument("$gt", new BsonInt64(version))));
        ((BsonDocument) ((Map.Entry<String, BsonValue>) nextCommitTimestampElement.entrySet().toArray()[0]).getValue()).put("$gt", new BsonInt64(version));
        list.add(nextCommitTimestampElement);
        
        
        if(query.containsKey("$or")) {
            //if it already contains or, then the versioned or will overwrite the query or
            List<BsonValue> andList = new ArrayList();
            andList.add(new BsonDocument(MongoMVCCFields.COMMIT_TIMESTAMP, new BsonDocument("$lte", new BsonInt64(version))));
            andList.add(new BsonDocument("$or", new BsonArray(list)));
            andList.add(new BsonDocument(MongoMVCCFields.DELETED_DOCUMENT, existsFalseElement));
            query.append("$and", new BsonArray(andList));
        } else {
            //query.append(MongoMVCCFields.COMMIT_TIMESTAMP, new BsonDocument("$lte", new BsonInt64(version)))
            commitTimestampElement.put("$lte", new BsonInt64(version));
            query.append(MongoMVCCFields.COMMIT_TIMESTAMP, commitTimestampElement)
                .append("$or", new BsonArray(list))
                .append(MongoMVCCFields.DELETED_DOCUMENT, existsFalseElement);
        }
        
        return query;
    }
    
    
    /*
    public static BsonDocument getQueryVersion(BsonDocument query, long version) {
    
        //BsonDocument queryVersioned = query.toBsonDocument(query.getClass(), codecRegistry);
        List<BsonValue> list = new ArrayList<>(2);
        list.add(emptyNextCommitTimestampElement);
        list.add(new BsonDocument(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, new BsonDocument("$gt", new BsonInt64(version))));
        
        
        
        if(query.containsKey("$or")) {
            //if it already contains or, then the versioned or will overwrite the query or
            List<BsonValue> andList = new ArrayList();
            andList.add(new BsonDocument(MongoMVCCFields.COMMIT_TIMESTAMP, new BsonDocument("$lte", new BsonInt64(version))));
            andList.add(new BsonDocument("$or", new BsonArray(list)));
            andList.add(new BsonDocument(MongoMVCCFields.DELETED_DOCUMENT, existsFalseElement));
            query.append("$and", new BsonArray(andList));
        } else {
            query.append(MongoMVCCFields.COMMIT_TIMESTAMP, new BsonDocument("$lte", new BsonInt64(version)))
                .append("$or", new BsonArray(list))
                .append(MongoMVCCFields.DELETED_DOCUMENT, existsFalseElement);
        }
        
        return query;
    }
    */
    
    
    /**
     * It generates the appropriate query for fetching the transactions non-deleted private versions
     * In SQL it is similar to a WHERE predicate as follows:
     * (_cmtTmstmp IS NULL) AND (_nextCmtTmstmp IS NULL) AND (_tid = tid) AND (isDELETED IS NULL)
     * @param query
     * @param tid
     * @return 
     */
    public static BasicDBObject getQueryPrivateVersion(BasicDBObject query, long tid) {
    
        BasicDBObject queryPrivateVersioned = new BasicDBObject(query.toMap());
        queryPrivateVersioned.append(MongoMVCCFields.TID, tid)
                             .append((MongoMVCCFields.COMMIT_TIMESTAMP), "")
                             .append((MongoMVCCFields.NEXT_COMMIT_TIMESTAMP), "")
                             .append(MongoMVCCFields.DELETED_DOCUMENT, new BasicDBObject("$exists", false));
        
        return queryPrivateVersioned;
    }
    
    
    /**
     * It generates the appropriate query for fetching the transactions non-deleted private versions
     * In SQL it is similar to a WHERE predicate as follows:
     * (_cmtTmstmp IS NULL) AND (_nextCmtTmstmp IS NULL) AND (_tid = tid) AND (isDELETED IS NULL)
     * @param query
     * @param tid
     * @param codecRegistry
     * @return 
     */
    public static BsonDocument getQueryPrivateVersion(BsonDocument query, long tid, CodecRegistry codecRegistry) {
    
        //BsonDocument queryPrivateVersioned = query.toBsonDocument(query.getClass(), codecRegistry);
        
        query.append(MongoMVCCFields.TID, new BsonInt64(tid))
                             .append((MongoMVCCFields.COMMIT_TIMESTAMP), new BsonString(""))
                             .append((MongoMVCCFields.NEXT_COMMIT_TIMESTAMP), new BsonString(""))
                             .append(MongoMVCCFields.DELETED_DOCUMENT, new BsonDocument("$exists", new BsonBoolean(false)));
        
        return query;
    }
    
    
    
    
    
    /**
     * It generates the appropriate query for fetching the transactions private versions. To be used upon commit
     * In SQL it is similar to a WHERE predicate as follows:
     * (_cmtTmstmp IS NULL) AND (_nextCmtTmstmp IS NULL) AND (_tid = tid)
     * @param tid
     * @return 
     */
    public static BasicDBObject getAllPrivateVersions(long tid) {
    
        BasicDBObject queryPrivateVersioned = new BasicDBObject(MongoMVCCFields.TID, tid)
                             .append((MongoMVCCFields.COMMIT_TIMESTAMP), "");
        
        return queryPrivateVersioned;
    }
    
    
    
    /**
     * It generates the appropriate query for fetching the transactions private versions. To be used upon commit
     * In SQL it is similar to a WHERE predicate as follows:
     * (_cmtTmstmp IS NULL) AND (_nextCmtTmstmp IS NULL) AND (_tid = tid)
     * @param query
     * @param tid
     * @param codecRegistry
     * @return 
     */
    public static BsonDocument getAllPrivateVersions(BsonDocument query, long tid, CodecRegistry codecRegistry) {
    
        //BsonDocument queryPrivateVersioned = query.toBsonDocument(query.getClass(), codecRegistry);
        query.append(MongoMVCCFields.TID, new BsonInt64(tid))
                             .append((MongoMVCCFields.COMMIT_TIMESTAMP), new BsonString(""))
                             .append((MongoMVCCFields.NEXT_COMMIT_TIMESTAMP), new BsonString(""));
        
        return query;
    }
}
