package eu.coherentpaas.mongodb.utils;

import com.mongodb.BasicDBObject;
import com.mongodb.DBEncoder;
import com.mongodb.DBObject;
import com.mongodb.DeprecatedDBCollection;
import com.mongodb.DeprecatedDBCursor;
import com.mongodb.WriteConcern;
import com.mongodb.WriteResult;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import eu.coherentpaas.mongodb.exceptions.MongoMVCCException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentMap;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public class MongoMVCCUpdator {
    private static final Logger Log = LoggerFactory.getLogger(MongoMVCCUpdator.class);
    private final int NUM_THREADS = 10;
    protected final ConcurrentMap<Object, Bson> writeset;
    protected final BasicDBObject _query;
    protected final BasicDBObject _updateQuery;
    protected final MongoTransactionContext mongoTransactionalContext;
    protected final boolean _upsert;
    protected boolean _multi;
    protected final DeprecatedDBCollection _depracedDBCollection;
    protected final WriteConcern concern;
    protected final DBEncoder encoder;
    
    public MongoMVCCUpdator(DeprecatedDBCollection collection, ConcurrentMap<Object, Bson> writeset, MongoTransactionContext mongoTransactionalContext, BasicDBObject query, BasicDBObject updateQuery, boolean upsert, boolean multi, WriteConcern writeConcern, DBEncoder encoder) {
        this._query = query;
        this._updateQuery = updateQuery;
        this._upsert = upsert;
        this._depracedDBCollection = collection;
        this.writeset = writeset;
        this.mongoTransactionalContext = mongoTransactionalContext;
        this._multi = multi;
        this.encoder = encoder;
        if(writeConcern!=null)
            this.concern = writeConcern;
        else
            this.concern = this._depracedDBCollection.getWriteConcern();
    }
    
    public WriteResult update() throws MongoMVCCException, TransactionManagerException {
        
        String key = _updateQuery.keySet().iterator().next(); //key to check if it is a "set" or "replace" operation
        
        if(key.equals("$set"))
            return updateSet();
        else 
            return updateReplace();
    }
    
    private WriteResult updateSet() throws MongoMVCCException, TransactionManagerException {
        
        WriteResult writeResult = null;
        int countTotal = 0;
        DeprecatedDBCursor cursor;
        //first iterate through the private writeset and update
        if(!this.writeset.isEmpty()) {
            BasicDBObject queryVersioned = VersionQueryUtils.getQueryPrivateVersion(_query, mongoTransactionalContext.getTid());
            cursor = this._depracedDBCollection.find(queryVersioned);

            while(cursor.hasNext()) { //keep the ids of the private versions
                countTotal++;
                ObjectId objectID = ((BasicDBObject)cursor.next()).getObjectId(MongoMVCCFields.DATA_ID);

                if(this._multi==false) {
                    //only update one document and return
                    queryVersioned = queryVersioned.append(MongoMVCCFields.DATA_ID, objectID);
                    if(encoder==null)
                        writeResult = this._depracedDBCollection.update(queryVersioned, _updateQuery, _upsert, _multi, concern);
                    else
                        writeResult = this._depracedDBCollection.update(queryVersioned, _updateQuery, _upsert, _multi, concern, encoder);
                    return writeResult;
                }
            }
            cursor.close();


            if(countTotal>0) {
                //if countTotal=0 there's nothing to update
                if(encoder==null)
                    writeResult = this._depracedDBCollection.update(queryVersioned, _updateQuery, _upsert, _multi, concern);
                else
                    writeResult = this._depracedDBCollection.update(queryVersioned, _updateQuery, _upsert, _multi, concern, encoder);
            }
        }
        
        //now update the old ones that are not in the private writeset
        List<DBObject> versionsToInsert = new ArrayList<>();
        cursor = this._depracedDBCollection.find(VersionQueryUtils.getQueryVersion(_query, mongoTransactionalContext.getStartTimestamp()));
        while(cursor.hasNext()) {
            DBObject dbObject = cursor.next();
            Object currentObjectId =  dbObject.get(MongoMVCCFields.DATA_ID);
            
            //if there's not in the private writeset (if it was, then the new one has already been updated
            if(!writeset.containsKey(currentObjectId)) {
                //get the key in byte [] to be checked for conflicts
                byte [] key = ByteOperators.getKeyBytes(MongoMVCCFields.STRING_SEPARATOR, currentObjectId);
                
                //if transaction fails, then a TransactionManagerException will be thrown which will lead to a rollback
                this.mongoTransactionalContext.checkForConflicts(key);
                
                countTotal++;
                versionsToInsert.add(updateDocument((BasicDBObject) dbObject));
                this.writeset.putIfAbsent(currentObjectId, (BasicDBObject) dbObject);
                if(_multi==false)
                    break; //only update one
            }
        }
        
        cursor.close();
        
        if(writeResult!=null)
            countTotal = writeResult.getN();
        
        if(!versionsToInsert.isEmpty()) {
            if(encoder==null)
                writeResult = this._depracedDBCollection.insert(versionsToInsert, concern);
            else
                writeResult = this._depracedDBCollection.insert(versionsToInsert, concern, encoder);
            writeResult = (new WriteResultBuilder(writeResult)).copy(countTotal);
        }
        
        
        return writeResult;
    }
    
    
    private BasicDBObject updateDocument(BasicDBObject dbObject) throws MongoMVCCException {
        String key = _updateQuery.keySet().iterator().next();
        if(!key.equals("$set"))
            throw new MongoMVCCException("not a valid set parameter");
        
        BasicDBObject values = (BasicDBObject) _updateQuery.values().iterator().next();
        for (Entry entry : values.entrySet()) {
            String documentField = (String) entry.getKey();
            Object documentFieldValue = entry.getValue();
            if(_upsert) {
                //put the value, even if the specific field does not exist
                dbObject.put(documentField, documentFieldValue);
            } else {
                //check if the field exists and update only if exists
                if(dbObject.containsField(documentField))
                    dbObject.put(documentField, documentFieldValue);
            }
        }
        
        //remove its _id and timestamps and add the new ones
        dbObject.remove(MongoMVCCFields._ID);
        dbObject.remove(MongoMVCCFields.TID);
        dbObject.remove(MongoMVCCFields.COMMIT_TIMESTAMP);
        dbObject.remove(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP);
        
        dbObject.append(MongoMVCCFields.TID, this.mongoTransactionalContext.getTid())
                .append(MongoMVCCFields.COMMIT_TIMESTAMP, "")
                .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, "")
                .append(MongoMVCCFields._ID, ByteOperators.getBytes(dbObject.get(MongoMVCCFields.DATA_ID), this.mongoTransactionalContext.getTid()));
        
        return dbObject;
    }
    
    
    private WriteResult updateReplace() throws TransactionManagerException, MongoMVCCException {
        
        if(_multi==true) {
            //Cannot update multi documents without a $ operator
            _multi=false;
        }
        
        WriteResult writeResult = null;
        int countTotal = 0;
        DeprecatedDBCursor cursor;
        
        //first iterate through the private writeset and update
        if(!this.writeset.isEmpty()) {
            BasicDBObject queryVersioned = VersionQueryUtils.getQueryPrivateVersion(_query, mongoTransactionalContext.getTid());
            cursor = this._depracedDBCollection.find(queryVersioned);

            while(cursor.hasNext()) { //keep the ids of the private versions
                //each document should have its own query --to handle different _dataIDs
                BasicDBObject objectUpdateQuery = new BasicDBObject(_updateQuery.toMap());
                countTotal++;
                ObjectId objectId = ((BasicDBObject)cursor.next()).getObjectId(MongoMVCCFields.DATA_ID);

                //if already has a DATA_ID different than its previous one, then change the _id correspondingly
                if(!objectUpdateQuery.containsField(MongoMVCCFields.DATA_ID)) {
                    objectUpdateQuery.append(MongoMVCCFields.DATA_ID, objectId);
                }

                //if already has a DATA_ID different than its previous one, then change the _id correspondingly
                if(!objectUpdateQuery.getObjectId(MongoMVCCFields.DATA_ID).equals(objectId)) {
                    objectUpdateQuery.append(MongoMVCCFields._ID, ByteOperators.getBytes(objectUpdateQuery.getObjectId(MongoMVCCFields.DATA_ID), this.mongoTransactionalContext.getTid()));               
                }


                //add the correspoding values (if there are missing)
                objectUpdateQuery.append(MongoMVCCFields.TID, this.mongoTransactionalContext.getTid())
                            .append(MongoMVCCFields.COMMIT_TIMESTAMP, "")
                            .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, "");

                //and update one by one
                queryVersioned = queryVersioned.append(MongoMVCCFields.DATA_ID, objectId);
                if(encoder==null)
                    writeResult = this._depracedDBCollection.update(queryVersioned, objectUpdateQuery, _upsert, _multi, concern);
                else
                    writeResult = this._depracedDBCollection.update(queryVersioned, objectUpdateQuery, _upsert, _multi, concern, encoder);

                if(this._multi==false) {
                    //only update one document and return
                    return writeResult;
                }
            }
            cursor.close();
        
        }
        
         //now update the old ones that are not in the private writeset
        List<DBObject> versionsToInsert = new ArrayList<>();
        cursor = this._depracedDBCollection.find(VersionQueryUtils.getQueryVersion(_query, mongoTransactionalContext.getStartTimestamp()));
        while(cursor.hasNext()) {
            DBObject dbObject = cursor.next();
            Object currentObjectId = dbObject.get(MongoMVCCFields.DATA_ID);
            
            //if there's not in the private writeset (if it was, then the new one has already been updated
            if(!writeset.containsKey(currentObjectId)) {
                //get the key in byte [] to be checked for conflicts
                byte [] key = ByteOperators.getKeyBytes(MongoMVCCFields.STRING_SEPARATOR, currentObjectId);
                
                //if transaction fails, then a TransactionManagerException will be thrown which will lead to a rollback
                this.mongoTransactionalContext.checkForConflicts(key);
                
                //each document should have its own query --to handle different _dataIDs
                BasicDBObject objectUpdateQuery = new BasicDBObject(_updateQuery.toMap());
                countTotal++;
                
                //if already has a DATA_ID different than its previous one, then change the _id correspondingly
                if(!objectUpdateQuery.containsField(MongoMVCCFields.DATA_ID)) {
                    objectUpdateQuery.append(MongoMVCCFields.DATA_ID, currentObjectId);
                }

                objectUpdateQuery.append(MongoMVCCFields._ID, ByteOperators.getBytes(currentObjectId, this.mongoTransactionalContext.getTid()))
                                 .append(MongoMVCCFields.TID, this.mongoTransactionalContext.getTid())
                                 .append(MongoMVCCFields.COMMIT_TIMESTAMP, "")
                                 .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, "");
                
                versionsToInsert.add(objectUpdateQuery);
                this.writeset.putIfAbsent(currentObjectId, (BasicDBObject) objectUpdateQuery);
                
                if(_multi==false)
                    break; //only update one
            }
        }
        
        cursor.close();
        
        if(!versionsToInsert.isEmpty()) {
            if(encoder==null)
                writeResult = this._depracedDBCollection.insert(versionsToInsert, concern);
            else
                writeResult = this._depracedDBCollection.insert(versionsToInsert, concern, encoder);
            
            writeResult = (new WriteResultBuilder(writeResult)).copy(countTotal);
        }
        
        
        
        return writeResult;
    }
}
