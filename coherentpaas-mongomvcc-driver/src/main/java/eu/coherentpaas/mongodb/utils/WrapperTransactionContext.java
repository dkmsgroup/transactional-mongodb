package eu.coherentpaas.mongodb.utils;

import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.datastore.DataStoreId;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;

/**
 *
 * @author idezol
 */
public class WrapperTransactionContext extends MongoTransactionContext {
    
    private TxnCtx txnCtx;

    public WrapperTransactionContext(TxnCtx txnCtx) {
        super(
                txnCtx.getCtxId(), 
                txnCtx.getStartTimestamp(), 
                txnCtx.getCommitTimestamp());
        
        this.txnCtx = txnCtx;
    }

    public TxnCtx getTxnCtx() {
        return txnCtx;
    }

    public void setTxnCtx(TxnCtx txnCtx) {
        this.txnCtx = txnCtx;
    } 
    
    
    @Override
    public void checkForConflicts(byte [] key) throws TransactionManagerException {
        this.txnCtx.hasConflict(key, DataStoreId.MONGODB);
    }
    
}
