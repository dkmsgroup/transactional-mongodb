package eu.coherentpaas.mongodb.utils;

import com.mongodb.BasicDBObject;
import com.mongodb.DBEncoder;
import com.mongodb.DBObject;
import com.mongodb.DeprecatedDBCollection;
import com.mongodb.DeprecatedDBCursor;
import com.mongodb.WriteConcern;
import com.mongodb.WriteResult;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public class MongoMVCCRemover {
    private static final Logger Log = LoggerFactory.getLogger(MongoMVCCRemover.class);
    private final int NUM_THREADS = 10;
    protected final ConcurrentMap<Object, Bson> writeset;
    protected final BasicDBObject _query;
    protected final MongoTransactionContext mongoTransactionContext;
    protected final DeprecatedDBCollection _depracedDBCollection;
    protected final WriteConcern concern;
    protected final DBEncoder encoder;
    
    public MongoMVCCRemover(DeprecatedDBCollection collection, ConcurrentMap<Object, Bson> writeset, MongoTransactionContext mongoTransactionContext, BasicDBObject query, WriteConcern writeConcern, DBEncoder encoder) {
        this._query = query;
        this._depracedDBCollection = collection;
        this.writeset = writeset;
        this.mongoTransactionContext = mongoTransactionContext;
        this.encoder = encoder;
        if(writeConcern!=null)
            this.concern = writeConcern;
        else
            this.concern = this._depracedDBCollection.getWriteConcern();
    }
    
    public WriteResult remove() throws TransactionManagerException { 
        
        WriteResult writeResult = null;
        int countTotal = 0;
        
        //first update the private writeset and set to be deleted
        if(!this.writeset.isEmpty()) {
            BasicDBObject queryVersioned = VersionQueryUtils.getQueryPrivateVersion(_query, this.mongoTransactionContext.getTid());
            BasicDBObject setDeleteFlag = new BasicDBObject("$set", new BasicDBObject(MongoMVCCFields.DELETED_DOCUMENT, true));
             if(encoder==null)
                writeResult = this._depracedDBCollection.update(queryVersioned, setDeleteFlag, false, true, concern);
            else
                writeResult = this._depracedDBCollection.update(queryVersioned, setDeleteFlag, false, true, concern, encoder);
        }
        
         //now add a new 'deleted' version to documents that are public and ARE NOT in the transaction's private writeset
         List<DBObject> versionsToInsert = new ArrayList<>();
        try (DeprecatedDBCursor cursor = this._depracedDBCollection.find(VersionQueryUtils.getQueryVersion(_query, this.mongoTransactionContext.getStartTimestamp()))) {
            while(cursor.hasNext()) {
                DBObject dbObject = cursor.next();
                Object currentObjectId = dbObject.get(MongoMVCCFields.DATA_ID);
                
                //if there's not in the private writeset (if it was, then the new one has already been checked previosly)
                if(!writeset.containsKey(currentObjectId)) {
                    //get the key in byte [] to be checked for conflicts
                    byte [] key = ByteOperators.getKeyBytes(MongoMVCCFields.STRING_SEPARATOR, currentObjectId);
                    
                    //if transaction fails, then a TransactionManagerException will be thrown which will lead to a rollback
                    this.mongoTransactionContext.checkForConflicts(key);
                    
                    countTotal++;
                    versionsToInsert.add(removeDocument((BasicDBObject) dbObject));
                    this.writeset.putIfAbsent(currentObjectId, (BasicDBObject) dbObject);
                }
            }
        }
        
        if(writeResult!=null)
            countTotal = writeResult.getN();
        
        if(!versionsToInsert.isEmpty()) {
            if(encoder==null)
                writeResult = this._depracedDBCollection.insert(versionsToInsert, concern);
            else
                writeResult = this._depracedDBCollection.insert(versionsToInsert, concern, encoder);
            countTotal = countTotal + writeResult.getN();
            writeResult = (new WriteResultBuilder(writeResult)).copy(countTotal);
        }
        
         
        return writeResult;
    }
    
    private BasicDBObject removeDocument(BasicDBObject dbObject) {
        //remove its _id and timestamps and add the new ones
        dbObject.remove(MongoMVCCFields._ID);
        dbObject.remove(MongoMVCCFields.TID);
        dbObject.remove(MongoMVCCFields.COMMIT_TIMESTAMP);
        dbObject.remove(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP);
        
        //append values
        dbObject.append(MongoMVCCFields.TID, this.mongoTransactionContext.getTid())
                .append(MongoMVCCFields.COMMIT_TIMESTAMP, "")
                .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, "")
                .append(MongoMVCCFields.DELETED_DOCUMENT, true)
                .append(MongoMVCCFields._ID, ByteOperators.getBytes(dbObject.get(MongoMVCCFields.DATA_ID), this.mongoTransactionContext.getTid()));
        
        return dbObject;
    }
}
